-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2015 a las 06:45:37
-- Versión del servidor: 5.6.14
-- Versión de PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sis2015`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_fix`
--

CREATE TABLE IF NOT EXISTS `curso_fix` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `curso_sede` int(11) NOT NULL,
  `turno` int(11) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `modalidad` int(11) DEFAULT NULL COMMENT '2 presencial 1 linea',
  `usuario_id_usuario` int(11) NOT NULL,
  `robotoit_user_id_user` int(9) unsigned NOT NULL,
  `fecha_inscripcion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10000 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
