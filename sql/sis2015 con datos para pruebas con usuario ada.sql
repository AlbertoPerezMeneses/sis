-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-03-2015 a las 18:35:43
-- Versión del servidor: 5.5.39
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sis2015`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_cursos`
--

CREATE TABLE IF NOT EXISTS `catalogo_cursos` (
`id_catalogo_cursos` int(11) NOT NULL,
  `nombre_catalogo` varchar(90) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `catalogo_cursos`
--

INSERT INTO `catalogo_cursos` (`id_catalogo_cursos`, `nombre_catalogo`) VALUES
(1, 'Excel Básico'),
(2, 'word basico'),
(3, 'power point basico'),
(4, 'PNL'),
(5, 'Lectura rapida y de comprensión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--

CREATE TABLE IF NOT EXISTS `examen` (
`id_examen` int(11) NOT NULL,
  `nombre_examen` varchar(25) DEFAULT NULL,
  `calificacion` float DEFAULT '0',
  `usuario_id_usuario` int(11) NOT NULL,
  `catalogo_cursos_id_catalogo_cursos` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `examen`
--

INSERT INTO `examen` (`id_examen`, `nombre_examen`, `calificacion`, `usuario_id_usuario`, `catalogo_cursos_id_catalogo_cursos`) VALUES
(1, 'excel basico', 7, 1, 1),
(2, 'word basico', 4.5, 1, 2),
(3, 'power point', 9, 2, 3),
(4, 'PNL', 2.3, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_envia`
--

CREATE TABLE IF NOT EXISTS `examen_envia` (
`id_examen_envia` int(11) NOT NULL,
  `aciertos` float DEFAULT '0',
  `intentos` int(11) DEFAULT '0',
  `usuario` varchar(45) DEFAULT NULL COMMENT 'Este usuario tiene como va a ser el folio de capacitacion para nosotros.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculacion`
--

CREATE TABLE IF NOT EXISTS `matriculacion` (
`id_matriculacion` int(11) NOT NULL,
  `status_matriculacionl` int(10) DEFAULT '0' COMMENT 'En caso de no poder inscribir al alumno al sistema del sidepaae el estatus es 0',
  `catalogo_cursos_id_catalogo_cursos` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `matriculacion`
--

INSERT INTO `matriculacion` (`id_matriculacion`, `status_matriculacionl`, `catalogo_cursos_id_catalogo_cursos`) VALUES
(1, 0, 2),
(2, 1, 3),
(3, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `robotoit_user`
--

CREATE TABLE IF NOT EXISTS `robotoit_user` (
`id_user` int(9) unsigned NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_password` varchar(256) NOT NULL,
  `imagen` varchar(512) DEFAULT 'default.png',
  `status` int(11) DEFAULT '2' COMMENT '1 admin 2 profesor 3 alumno',
  `existencia` int(11) DEFAULT '1' COMMENT '1 dado de alta'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `robotoit_user`
--

INSERT INTO `robotoit_user` (`id_user`, `user_name`, `user_password`, `imagen`, `status`, `existencia`) VALUES
(4, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'default.png', 1, 1),
(5, 'ada', '8c8d357b5e872bbacd45197626bd5759', 'default.png', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`id_usuario` int(11) NOT NULL,
  `folio_capacitacion` int(11) DEFAULT NULL,
  `nombre_del_trabajador` varchar(90) DEFAULT NULL,
  `rfc` varchar(18) DEFAULT NULL,
  `correo` varchar(128) DEFAULT NULL,
  `matriculacion_id_matriculacion` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `folio_capacitacion`, `nombre_del_trabajador`, `rfc`, `correo`, `matriculacion_id_matriculacion`) VALUES
(1, 4550, 'alberto perez meneses', '4550', 'dr.kidb@gmail.com', 0),
(2, 4551, 'ada', 'adabla220390', 'ada@gmail.com', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_catalogo_cursos`
--

CREATE TABLE IF NOT EXISTS `usuario_has_catalogo_cursos` (
`id_usuario_has_catalogo_cursos` int(11) NOT NULL,
  `usuario_id_usuario` int(11) NOT NULL,
  `catalogo_cursos_id_catalogo_cursos` int(11) NOT NULL,
  `intentos` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `usuario_has_catalogo_cursos`
--

INSERT INTO `usuario_has_catalogo_cursos` (`id_usuario_has_catalogo_cursos`, `usuario_id_usuario`, `catalogo_cursos_id_catalogo_cursos`, `intentos`) VALUES
(1, 1, 1, NULL),
(2, 1, 2, NULL),
(3, 2, 3, 0),
(4, 2, 4, NULL),
(5, 2, 5, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catalogo_cursos`
--
ALTER TABLE `catalogo_cursos`
 ADD PRIMARY KEY (`id_catalogo_cursos`);

--
-- Indices de la tabla `examen`
--
ALTER TABLE `examen`
 ADD PRIMARY KEY (`id_examen`), ADD KEY `fk_examen_usuario1_idx` (`usuario_id_usuario`), ADD KEY `fk_examen_catalogo_cursos1_idx` (`catalogo_cursos_id_catalogo_cursos`);

--
-- Indices de la tabla `examen_envia`
--
ALTER TABLE `examen_envia`
 ADD PRIMARY KEY (`id_examen_envia`);

--
-- Indices de la tabla `matriculacion`
--
ALTER TABLE `matriculacion`
 ADD PRIMARY KEY (`id_matriculacion`), ADD KEY `fk_matriculacion_catalogo_cursos_idx` (`catalogo_cursos_id_catalogo_cursos`);

--
-- Indices de la tabla `robotoit_user`
--
ALTER TABLE `robotoit_user`
 ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuario_has_catalogo_cursos`
--
ALTER TABLE `usuario_has_catalogo_cursos`
 ADD PRIMARY KEY (`id_usuario_has_catalogo_cursos`,`usuario_id_usuario`,`catalogo_cursos_id_catalogo_cursos`), ADD KEY `fk_usuario_has_catalogo_cursos_catalogo_cursos1_idx` (`catalogo_cursos_id_catalogo_cursos`), ADD KEY `fk_usuario_has_catalogo_cursos_usuario1_idx` (`usuario_id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catalogo_cursos`
--
ALTER TABLE `catalogo_cursos`
MODIFY `id_catalogo_cursos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `examen`
--
ALTER TABLE `examen`
MODIFY `id_examen` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `examen_envia`
--
ALTER TABLE `examen_envia`
MODIFY `id_examen_envia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `matriculacion`
--
ALTER TABLE `matriculacion`
MODIFY `id_matriculacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `robotoit_user`
--
ALTER TABLE `robotoit_user`
MODIFY `id_user` int(9) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuario_has_catalogo_cursos`
--
ALTER TABLE `usuario_has_catalogo_cursos`
MODIFY `id_usuario_has_catalogo_cursos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `examen`
--
ALTER TABLE `examen`
ADD CONSTRAINT `fk_examen_catalogo_cursos1` FOREIGN KEY (`catalogo_cursos_id_catalogo_cursos`) REFERENCES `catalogo_cursos` (`id_catalogo_cursos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_examen_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `matriculacion`
--
ALTER TABLE `matriculacion`
ADD CONSTRAINT `fk_matriculacion_catalogo_cursos` FOREIGN KEY (`catalogo_cursos_id_catalogo_cursos`) REFERENCES `catalogo_cursos` (`id_catalogo_cursos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_has_catalogo_cursos`
--
ALTER TABLE `usuario_has_catalogo_cursos`
ADD CONSTRAINT `fk_usuario_has_catalogo_cursos_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_usuario_has_catalogo_cursos_catalogo_cursos1` FOREIGN KEY (`catalogo_cursos_id_catalogo_cursos`) REFERENCES `catalogo_cursos` (`id_catalogo_cursos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
