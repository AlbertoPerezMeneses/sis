-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-03-2015 a las 18:50:54
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sis2015`
--

--
-- Volcado de datos para la tabla `catalogo_cursos`
--

INSERT INTO `catalogo_cursos` (`id_catalogo_cursos`, `nombre_catalogo`) VALUES
(1, 'Excel Básico'),
(2, 'word basico'),
(3, 'power point basico'),
(4, 'PNL'),
(5, 'Lectura rapida y de comprensión');

--
-- Volcado de datos para la tabla `examen`
--

INSERT INTO `examen` (`id_examen`, `nombre_examen`, `calificacion`, `usuario_id_usuario`, `catalogo_cursos_id_catalogo_cursos`) VALUES
(1, 'excel basico', 7, 1, 1),
(2, 'word basico', 4.5, 1, 2),
(3, 'power point', 9, 2, 3),
(4, 'PNL', 2.3, 2, 4);

--
-- Volcado de datos para la tabla `matriculacion`
--

INSERT INTO `matriculacion` (`id_matriculacion`, `status_matriculacionl`, `catalogo_cursos_id_catalogo_cursos`) VALUES
(1, 0, 2);

--
-- Volcado de datos para la tabla `robotoit_user`
--

INSERT INTO `robotoit_user` (`id_user`, `user_name`, `user_password`, `imagen`, `status`, `existencia`) VALUES
(4, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'default.png', 1, 1),
(5, 'ada', '8c8d357b5e872bbacd45197626bd5759', 'default.png', 2, 1);

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `folio_capacitacion`, `nombre_del_trabajador`, `rfc`, `correo`, `robotoit_user_id_user`, `matriculacion_id_matriculacion`) VALUES
(1, 4550, 'alberto perez meneses', '4550', 'dr.kidb@gmail.com', 4, 0),
(2, 4551, 'ada', 'adabla220390', 'ada@gmail.com', 5, 0);

--
-- Volcado de datos para la tabla `usuario_has_catalogo_cursos`
--

INSERT INTO `usuario_has_catalogo_cursos` (`id_usuario_has_catalogo_cursos`, `usuario_id_usuario`, `catalogo_cursos_id_catalogo_cursos`, `intentos`) VALUES
(1, 1, 1, NULL),
(2, 1, 2, NULL),
(3, 2, 3, NULL),
(4, 2, 4, NULL),
(5, 2, 5, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
