SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sis2015
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sis2015` ;
CREATE SCHEMA IF NOT EXISTS `sis2015` DEFAULT CHARACTER SET utf8 ;
USE `sis2015` ;

-- -----------------------------------------------------
-- Table `sis2015`.`catalogo_cursos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`catalogo_cursos` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`catalogo_cursos` (
  `id_catalogo_cursos` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_catalogo` VARCHAR(90) NULL DEFAULT NULL,
  PRIMARY KEY (`id_catalogo_cursos`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sis2015`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`usuario` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`usuario` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `folio_capacitacion` INT(11) NULL DEFAULT NULL,
  `nombre_del_trabajador` VARCHAR(90) NULL DEFAULT NULL,
  `rfc` VARCHAR(18) NULL DEFAULT NULL,
  `correo` VARCHAR(128) NULL DEFAULT NULL,
  `matriculacion_id_matriculacion` INT NOT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sis2015`.`examen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`examen` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`examen` (
  `id_examen` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_examen` VARCHAR(25) NULL DEFAULT NULL,
  `calificacion` FLOAT NULL DEFAULT '0',
  `usuario_id_usuario` INT(11) NOT NULL,
  `catalogo_cursos_id_catalogo_cursos` INT(11) NOT NULL,
  PRIMARY KEY (`id_examen`),
  INDEX `fk_examen_usuario1_idx` (`usuario_id_usuario` ASC),
  INDEX `fk_examen_catalogo_cursos1_idx` (`catalogo_cursos_id_catalogo_cursos` ASC),
  CONSTRAINT `fk_examen_catalogo_cursos1`
    FOREIGN KEY (`catalogo_cursos_id_catalogo_cursos`)
    REFERENCES `sis2015`.`catalogo_cursos` (`id_catalogo_cursos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_examen_usuario1`
    FOREIGN KEY (`usuario_id_usuario`)
    REFERENCES `sis2015`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sis2015`.`robotoit_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`robotoit_user` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`robotoit_user` (
  `id_user` INT(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(45) NOT NULL,
  `user_password` VARCHAR(256) NOT NULL,
  `imagen` VARCHAR(512) NULL DEFAULT 'default.png',
  `status` INT(11) NULL DEFAULT '2' COMMENT '1 admin 2 profesor 3 alumno',
  `existencia` INT(11) NULL DEFAULT '1' COMMENT '1 dado de alta',
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sis2015`.`usuario_has_catalogo_cursos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`usuario_has_catalogo_cursos` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`usuario_has_catalogo_cursos` (
  `id_usuario_has_catalogo_cursos` INT NOT NULL AUTO_INCREMENT,
  `usuario_id_usuario` INT(11) NOT NULL,
  `catalogo_cursos_id_catalogo_cursos` INT(11) NOT NULL,
  `intentos` INT NULL,
  PRIMARY KEY (`id_usuario_has_catalogo_cursos`, `usuario_id_usuario`, `catalogo_cursos_id_catalogo_cursos`),
  INDEX `fk_usuario_has_catalogo_cursos_catalogo_cursos1_idx` (`catalogo_cursos_id_catalogo_cursos` ASC),
  INDEX `fk_usuario_has_catalogo_cursos_usuario1_idx` (`usuario_id_usuario` ASC),
  CONSTRAINT `fk_usuario_has_catalogo_cursos_usuario1`
    FOREIGN KEY (`usuario_id_usuario`)
    REFERENCES `sis2015`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_catalogo_cursos_catalogo_cursos1`
    FOREIGN KEY (`catalogo_cursos_id_catalogo_cursos`)
    REFERENCES `sis2015`.`catalogo_cursos` (`id_catalogo_cursos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sis2015`.`matriculacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`matriculacion` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`matriculacion` (
  `id_matriculacion` INT NOT NULL AUTO_INCREMENT,
  `status_matriculacionl` INT(10) NULL DEFAULT 0 COMMENT 'En caso de no poder inscribir al alumno al sistema del sidepaae el estatus es 0',
  `catalogo_cursos_id_catalogo_cursos` INT(11) NOT NULL,
  PRIMARY KEY (`id_matriculacion`),
  INDEX `fk_matriculacion_catalogo_cursos_idx` (`catalogo_cursos_id_catalogo_cursos` ASC),
  CONSTRAINT `fk_matriculacion_catalogo_cursos`
    FOREIGN KEY (`catalogo_cursos_id_catalogo_cursos`)
    REFERENCES `sis2015`.`catalogo_cursos` (`id_catalogo_cursos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sis2015`.`examen_envia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sis2015`.`examen_envia` ;

CREATE TABLE IF NOT EXISTS `sis2015`.`examen_envia` (
  `id_examen_envia` INT NOT NULL AUTO_INCREMENT,
  `aciertos` FLOAT NULL DEFAULT 0.0,
  `intentos` INT NULL DEFAULT 0,
  `usuario` VARCHAR(45) NULL COMMENT 'Este usuario tiene como va a ser el folio de capacitacion para nosotros.',
  PRIMARY KEY (`id_examen_envia`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
