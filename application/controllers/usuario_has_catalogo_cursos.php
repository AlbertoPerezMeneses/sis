<?php

// created 17-03-15 02:48:10
if (!defined("BASEPATH"))
    die();

class Usuario_has_catalogo_cursos extends CI_Controller {

    public function index() {
        $this->load->view("templates/header");
        $this->load->view("templates/aside");
        $this->load->view("templates/footer");
    }

    public function view() {

        $this->load->model("usuario_has_catalogo_cursos_model");
        $this->config->load("cms");
        /**/$data["item"] = $this->config->item("user_nav");
        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get();
        //$data["keys"]=array_keys($data["usuario_has_catalogo_cursos"][0]);
        $this->load->view("templates/header");
        $this->load->view("templates/menubar", $data);
        $this->load->view("users/usuario_has_catalogo_cursos/show_usuario_has_catalogo_cursos", $data);
        $this->load->view("templates/footer");
    }

    public function admin() {

        $this->load->model("usuario_has_catalogo_cursos_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get();
        //$data["keys"]=array_keys($data["usuario_has_catalogo_cursos"][0]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario_has_catalogo_cursos/show_usuario_has_catalogo_cursos", $data);
        $this->load->view("templates/footer");
    }

    public function find() {

        $this->load->model("usuario_has_catalogo_cursos_model");
        $this->config->load("cms");
        /**/$data["item"] = $this->config->item("user_nav");
        $data["id"] = $this->uri->segment(3);
        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->getOne($data["id"]);
        $this->load->view("templates/header");
        $this->load->view("templates/menubar", $data);
        $this->load->view("users/usuario_has_catalogo_cursos/find_usuario_has_catalogo_cursos", $data);
        $this->load->view("templates/footer");
    }

    public function create() {
        $this->load->model("usuario_has_catalogo_cursos_model");

        $data["usuario"] = $this->usuario_has_catalogo_cursos_model->get_usuario_nombre_del_trabajador();

        $data["catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get_catalogo_cursos_nombre_catalogo();

        $data["client"] = $this->client();
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");

        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {
            $this->usuario_has_catalogo_cursos_model->add();
            redirect("/usuario_has_catalogo_cursos/admin/", "refresh");
        }
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario_has_catalogo_cursos/add_usuario_has_catalogo_cursos", $data);
        $this->load->view("templates/footer");
    }

    public function update() {
        $this->load->model("usuario_has_catalogo_cursos_model");

        $data["usuario"] = $this->usuario_has_catalogo_cursos_model->get_usuario_nombre_del_trabajador();

        $data["catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get_catalogo_cursos_nombre_catalogo();

        $data["client"] = $this->client();
        $data["id"] = $this->uri->segment(3);
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        if ($this->server() AND ( isset($data["id"]) and $data["id"] != "") and ( isset($_POST) and ! empty($_POST))) {
            $this->usuario_has_catalogo_cursos_model->update($this->uri->rsegment(3));
            redirect("/usuario_has_catalogo_cursos/admin/", "refresh");
        }
        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->getOne($data["id"]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario_has_catalogo_cursos/edit_usuario_has_catalogo_cursos", $data);
        $this->load->view("templates/footer");
    }

    public function delete() {
        $data["id"] = $this->uri->segment(3);
        $this->load->model("usuario_has_catalogo_cursos_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        if ((isset($data["id"]) and $data["id"] != "") and ! empty($_POST)) {
            if ($_POST["confirmar"] === "TRUE") {
                $this->usuario_has_catalogo_cursos_model->delete($data["id"]);
                redirect("/usuario_has_catalogo_cursos/admin/TRUE/", "refresh");
            } else {
                redirect("/usuario_has_catalogo_cursos/admin/FALSE/", "refresh");
            }
        }

        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario_has_catalogo_cursos/delete_usuario_has_catalogo_cursos", $data);
        $this->load->view("templates/footer");
    }
    public function generar_oferta() {
         /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario_has_catalogo_cursos/crea_oferta");
        $this->load->view("templates/footer");
    }
    
   
    public function server() {
        if (isset($_POST) and ! empty($_POST)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("intentos", "intentos", "trim|xss_clean");
            $this->form_validation->set_rules("usuario_id_usuario", "usuario_id_usuario", "trim|required|xss_clean|integer");
            //$data["usuario"]=$this->usuario_has_catalogo_cursos_model->get_usuario_nombre_del_trabajador();
            $this->form_validation->set_rules("catalogo_cursos_id_catalogo_cursos", "catalogo_cursos_id_catalogo_cursos", "trim|required|xss_clean|integer");
            //$data["catalogo_cursos"]=$this->usuario_has_catalogo_cursos_model->get_catalogo_cursos_nombre_catalogo();

            return $this->form_validation->run();
        } else {
            return FALSE;
        }
    }

    public function client() {
        $this->load->library("validate");
        $this->validate->setId("usuario_has_catalogo_cursos");

        return $this->validate->getJscript();
    }

}
