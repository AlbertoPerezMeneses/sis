<?php

// created 19-03-15 01:53:18

if (!defined("BASEPATH"))
    die();

include_once APPPATH . "libraries/REST_Controller.php";

class Examen_envia_API extends REST_Controller {

    public function show_all_get() {



        $this->load->model("examen_envia_model");

        $data["examen_envia"] = $this->examen_envia_model->get();

        $this->response($data, 200);
    }

    public function correo_post() {

        $config['charset'] = 'iso-8859-1';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';


        $this->load->library('email');
        $this->email->initialize($config);




        $address = $this->post("to");
        $message = $this->post("message");
        $subject = $this->post("subject");

        $this->email->to($address);
        $this->email->from('noreply@robotoit.com', "soporte");
        $this->email->subject($subject);
        $this->email->message($message);
        $result["response"] = $this->email->send();
        $this->response($result, 200);
    }

    public function find_by_id_get() {



        $this->load->model("examen_envia_model");



        $data["id"] = $this->get("id", TRUE);

        $data["examen_envia"] = $this->examen_envia_model->getOne($data["id"]);

        $this->response($data, 200);
    }

    public function find_by_folio_capacitacion_get() {

        $this->load->model("examen_envia_model");

        $data["id"] = $this->get("id", TRUE);
        $data["examen_envia"] = $this->examen_envia_model->find_by_folio($data["id"]);
        $data["queries"] = $this->db->queries;
        $this->response($data, 200);
    }

    public function create_post() {
        $this->load->model("examen_envia_model");
        $this->load->model("examen_model");
        $this->load->model("usuario_model");

        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {
            $this->examen_envia_model->add();
            $usuario = $this->usuario_model->get_by_folio_capacitacion($this->input->post("usuario"));

            $examen = $this->parseEnviaToExamen($_POST, $usuario[0]["id_usuario"]);
            /*
              $data["sobrecalificado"]=$this->filtro_sobrecalificado($_POST["aciertos"],$usuario[0]["id_usuario"],$_POST["id_catalogo_cursos"]);
              $data["insuficiente"]=$this->filtro_insuficiente($_POST["aciertos"],$usuario[0]["id_usuario"],$_POST["id_catalogo_cursos"]);
              $data["calificado"]=$this->filtro_calificado($_POST["aciertos"],$usuario[0]["id_usuario"],$_POST["id_catalogo_cursos"]);
             */
            //este es el nuevo filtro necesario para la uam
            $data["calificado"] = $this->califica_examen($_POST["aciertos"], $usuario[0]["id_usuario"], $_POST["id_catalogo_cursos"]);

            $this->examen_model->add_by_data($examen);


            $data["response"] = 1;
            $data["evaluation"] = $_POST;
        } else {
            $data["response"] = 0;
        }
        $this->response($data, 200);
    }

    /* metodo de prueba para los examenes de envia */

    public function create_debug_post() {
        $this->load->model("examen_envia_model");
        $this->load->model("examen_model");
        $this->load->model("usuario_model");

        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {
            //  $this->examen_envia_model->add();
            $usuario = $this->usuario_model->get_by_folio_capacitacion($_POST["usuario"]);

            $examen = $this->parseEnviaToExamen($_POST, $usuario[0]["id_usuario"]);

            $data["evaluacion"] = $this->evalua_examen($_POST["aciertos"], $_POST["id_catalogo_cursos"]);
            $data["usuario"] = $usuario;
            $data["examen"] = $examen;
            $data["response"] = 1;
            $data["evaluation"] = $_POST;
        } else {
            $data["response"] = 0;
        }
        $this->response($data, 200);
    }

    /* fin  metodo de prueba para los examenes de envia */

    private function parseEnviaToExamen($data = NULL, $usuario_id_usuario = NULL) {

        if ($data !== NULL and is_array($data)) {

            $array["nombre_examen"] = "";

            $array["calificacion"] = $data["aciertos"];

            $array["catalogo_cursos_id_catalogo_cursos"] = $data["id_catalogo_cursos"];

            if ($usuario_id_usuario != NULL) {

                $array["usuario_id_usuario"] = $usuario_id_usuario;
            } else {

                $array["usuario_id_usuario"] = $data["usuario"];
            }
        }

        return $array;
    }

    /*

     * este filtro matricula al usuario en caso de tener una calificacion entre 9.59 y 6.0 puntos
     *      */

    private function filtro_calificado($calificacion = NULL, $id_usuario = NULL, $curso = NULL) {
        if ($calificacion != NULL and $id_usuario != NULL and $curso != NULL and ( $calificacion < 9.6 and $calificacion > 6.0)) {
            $data["intentos"] = 1;
            $this->load->model("usuario_has_catalogo_cursos_model");
            $this->usuario_has_catalogo_cursos_model->updateByIdUsuarioCurso($id_usuario, $curso, $data);
            $this->usuario_has_catalogo_cursos_model->matricula_alumno($id_usuario, $curso);
            return true;
        } else {
            return false;
        }
    }

    /*

     * en caso de tener  mas de 9.6 puntos el usuario no podra ser matriculado y se registrara como un intento

     *   */

    private function filtro_sobrecalificado($calificacion = NULL, $id_usuario = NULL, $curso = NULL) {

        if ($calificacion != NULL and $id_usuario != NULL and $curso != NULL and $calificacion >= 9.6) {

            $data["intentos"] = 1;

            $this->load->model("usuario_has_catalogo_cursos_model");

            $this->usuario_has_catalogo_cursos_model->updateByIdUsuarioCurso($id_usuario, $curso, $data);

            return $this->db->last_query();
        } else {

            return false;
        }
    }

    /*
     * este filtro se usara en caso de que el participante tenga menor puntaje que el necesario para poder inscribirse
     *   
     */

    private function filtro_insuficiente($calificacion = NULL, $id_usuario = NULL, $curso = NULL) {
        if ($calificacion != NULL and $id_usuario != NULL and $curso != NULL and $calificacion <= 6.0) {
            $data["intentos"] = 1;
            $this->load->model("usuario_has_catalogo_cursos_model");
            $this->usuario_has_catalogo_cursos_model->updateByIdUsuarioCurso($id_usuario, $curso, $data);
            return false;
        } else {
            return true;
        }
    }

    /* filtro activo para los nuevos requisitos de la plataforma de evaluacion */

    private function califica_examen($calificacion = null, $id_usuario = null, $curso = null) {
        if ($calificacion != NULL and $id_usuario != NULL and $curso != NULL and $this->evalua_examen($calificacion, $curso)) {
            $data["intentos"] = 1;
            $this->load->model("usuario_has_catalogo_cursos_model");
            $this->usuario_has_catalogo_cursos_model->updateByIdUsuarioCurso($id_usuario, $curso, $data);
            $this->usuario_has_catalogo_cursos_model->matricula_alumno($id_usuario, $curso);

            return 1;
        } else {
            $data["intentos"] = 1;
            $this->load->model("usuario_has_catalogo_cursos_model");
            $this->usuario_has_catalogo_cursos_model->updateByIdUsuarioCurso($id_usuario, $curso, $data);
            return 0;
        }
    }

    private function evalua_examen($calificacion = null, $curso = null) {

        //$calificacion = $this->get("dato", true);
        $calificacion += 10000000000;
        $items["enteros"] = substr($calificacion, -2);
        $items["centenas"] = substr($calificacion, -4, -2);
        $items["millares"] = substr($calificacion, -6, -4);
        $items["millones"] = substr($calificacion, -7, -6);
        //$items["tamanio"] = strlen($calificacion);

        $resultado = $this->califica_normal($items["millones"], $items["millares"], $items["centenas"], $items["enteros"]);



        /* aqui 1 2 3 6 7 8 11 12 13 14  18 19 acredita en caso de ser cualquiera de estos examenes */
        if ($curso == 1 or $curso == 2 or $curso == 3 or $curso == 6 or $curso == 7 or $curso == 8 or $curso == 11 or $curso == 12 or $curso == 13 or $curso == 14 or $curso == 18 or $curso == 19) {
            $resultado = true;
        }

        /* fix */

        // $this->response($items, 200);
        return $resultado;
    }

    private function is_comprometido_interesado($numero = null) {
        if ($numero != null) {
            //return $numero>=1;				
            return true;
        }
    }

    /*
      depuracion
     */

    public function debug_evalua_examen_get($calificacion = null, $curso = null) {

        $calificacion = $this->get("dato", true);
        $curso = $this->get("curso", true);

        $calificacion += 10000000000;
        $items["enteros"] = substr($calificacion, -2);
        $items["centenas"] = substr($calificacion, -4, -2);
        $items["millares"] = substr($calificacion, -6, -4);
        $items["millones"] = substr($calificacion, -7, -6);
        $items["tamanio"] = strlen($calificacion);

        $resultado = $this->califica_normal($items["millones"], $items["millares"], $items["centenas"], $items["enteros"]);
        /*

         */
        $items["eval_unidades"] = $this->is_comprometido_interesado($items["enteros"]);
        $items["eval_centenas"] = $this->is_apto($items["centenas"]);
        $items["eval_millares"] = $this->is_conocedor($items["millares"]);
        $items["eval_millones"] = $this->is_comprometido_interesado($items["millones"]);

        /* aqui 1 2 3 6 7 8 11 12 13 14  18 19 acredita en caso de ser cualquiera de estos examenes */
        if ($curso == 1 or $curso == 2 or $curso == 3 or $curso == 6 or $curso == 7 or $curso == 8 or $curso == 11 or $curso == 12 or $curso == 13 or $curso == 14 or $curso == 18 or $curso == 19) {
            $resultado = true;
        }
        $items["resultado"] = $resultado;
        $this->response($items, 200);
        // return $resultado;
    }

    /*
      depuracion
     */

    private function is_conocedor($numero = null) {
        if ($numero != null) {
            //return $numero>=0 and $numero <15;				
            return $numero < 14;
        }
    }

    private function is_apto($numero = null) {
        if ($numero != null) {
            //return $numero>=5 and $numero <=8;
            return $numero >= 5 and $numero <= 10;
        }
    }

    private function califica_normal($comprometido = null, $conocedor = null, $contenido_curso = null, $interesado = null) {
        if ($conocedor != null and $contenido_curso != null and $comprometido != null and $interesado != null)
            return $this->is_comprometido_interesado($comprometido) and $this->is_comprometido_interesado($interesado) and $this->is_apto($contenido_curso) and $this->is_conocedor($conocedor);
    }

    /* fin filtro por */
    /* metodo para probar examen desde el servicio
      public   function evalua_examen_get() {

      $calificacion = $this->get("dato", true);
      $calificacion += 10000000000;
      $items["enteros"] = substr($calificacion, -2);
      $items["centenas"] = substr($calificacion, -4, -2);
      $items["millares"] = substr($calificacion, -6, -4);
      $items["millones"] = substr($calificacion, -7, -6);
      $items["tamanio"] = strlen($calificacion);
      $items["numero_completo"]=$calificacion;
      $items["resultado"]= $this->califica_normal($items["millones"], $items["millares"], $items["centenas"], $items["enteros"]);
      $items["isconocedor"]=	$this->is_conocedor($items["millares"]);
      $items["iscomprometido"]=$this->is_comprometido_interesado($items["millones"]);
      $items["isapto"]=	$this->is_apto($items["centenas"]);
      $items["isinteresado"]=	$this->is_comprometido_interesado($items["enteros"]);


      $this->response($items, 200);

      }
      /* */

    public function edit_post() {

        $this->load->model("examen_envia_model");



        $data["id"] = $this->post("id", TRUE);



        if ($this->server() AND ( isset($_data["id"]) and $_data["id"] > 0) and ( isset($_POST) and ! empty($_POST))) {

            $this->examen_envia_model->update($data["id"]);
        }

        $data["examen_envia"] = $this->examen_envia_model->getOne($data["id"]);

        $this->response($data, 200);
    }

    public function delete_post() {

        $data["id"] = $this->post("id");

        $this->load->model("examen_envia_model");

        $data["validation"] = (isset($data["id"]) and $data["id"] > 0) and ! empty($_POST);

        if ($data["validation"]) {

            if ($_POST["confirmar"] === "TRUE") {

                $this->examen_envia_model->delete($data["id"]);
            }

            $this->response($data, 200);
        }
    }

    public function server() {

        if (isset($_POST) and ! empty($_POST)) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("aciertos", "aciertos", "trim|required|xss_clean");

            $this->form_validation->set_rules("intentos", "intentos", "trim|required|xss_clean");

            $this->form_validation->set_rules("usuario", "usuario", "trim|required|xss_clean");



            return $this->form_validation->run();
        } else {

            return FALSE;
        }
    }

}
