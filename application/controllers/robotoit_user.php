<?php

// created 23-09-14 08:40:13
if (!defined("BASEPATH"))
    die();

class Robotoit_user extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view("templates/header");
        //$this->load->view("templates/aside");
        $this->load->view("templates/footer");
    }

    public function view() {
        $this->load->model("robotoit_user_model");
        $this->config->load("cms");
        /**/$data["item"] = $this->config->item("user_nav");
        $data["robotoit_user"] = $this->robotoit_user_model->get();
        //$data["keys"]=array_keys($data["robotoit_user"][0]);
        $this->load->view("templates/header");
        $this->load->view("templates/menubar", $data);
        $this->load->view("users/robotoit_user/show_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function admin() {

        $this->load->model("robotoit_user_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        $data["robotoit_user"] = $this->robotoit_user_model->get();
        //$data["keys"]=array_keys($data["robotoit_user"][0]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/robotoit_user/show_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function find() {

        $this->load->model("robotoit_user_model");
        $this->config->load("cms");
        /**/$data["item"] = $this->config->item("user_nav");
        $data["id"] = $this->uri->segment(3);
        $data["robotoit_user"] = $this->robotoit_user_model->getOne($data["id"]);
        $this->load->view("templates/header");
        $this->load->view("templates/menubar", $data);
        $this->load->view("users/robotoit_user/find_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function create() {
        $this->load->model("robotoit_user_model");

        $data["client"] = $this->client("create");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");



        //   $data["area"] = $this->robotoit_user_model->get_areas();

        if ($this->server("create") AND ( isset($_POST) AND ! empty($_POST))) {

            if ($this->do_upload()) {
                $data["algo"] = $this->upload->data();
                $upload_data = $this->upload->data();
                $_POST["imagen"] = $upload_data["file_name"];
                $this->resize_image($upload_data["full_path"]);
            }

            $this->robotoit_user_model->add();
            redirect("/robotoit_user/admin/", "refresh");
        }
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/robotoit_user/add_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function update() {
        $this->load->model("robotoit_user_model");

        $data["client"] = $this->client("update");
        $data["id"] = $this->uri->segment(3);
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        //and (trim($_POST["user_password"])!="")
        if (isset($data["id"]) and $data["id"] !== "" and isset($_POST) and ! empty($_POST)) {


            if ($this->do_upload()) {
                $data["algo"] = $this->upload->data();
                $upload_data = $this->upload->data();
                $_POST["imagen"] = $upload_data["file_name"];
                $this->resize_image($upload_data["full_path"]);
            }

            $this->robotoit_user_model->update($data["id"]);
            redirect("/robotoit_user/update/" . $data["id"], "refresh");
        }
        $data["robotoit_user"] = $this->robotoit_user_model->getOne($data["id"]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/robotoit_user/edit_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function update_pass() {
        $this->load->model("robotoit_user_model");

        $data["client"] = $this->client("change");
        $data["id"] = $this->uri->segment(3);
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        //
        if ($this->server("change") AND ( isset($data["id"]) and $data["id"] !== "") and ( isset($_POST) and ! empty($_POST)) and ( trim($_POST["user_password"]) != "")) {


            $this->robotoit_user_model->update($data["id"]);
            redirect("/robotoit_user/update/" . $data["id"], "refresh");
        }
        $data["robotoit_user"] = $this->robotoit_user_model->getOne($data["id"]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/robotoit_user/edit_password_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function delete() {
        $data["id"] = $this->uri->segment(3);
        $this->load->model("robotoit_user_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        if ((isset($data["id"]) and $data["id"] != "") and ! empty($_POST)) {
            if ($_POST["confirmar"] === "TRUE") {
                $this->robotoit_user_model->delete($data["id"]);
                redirect("/robotoit_user/admin/TRUE/", "refresh");
            } else {
                redirect("/robotoit_user/admin/FALSE/", "refresh");
            }
        }

        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/robotoit_user/delete_robotoit_user", $data);
        $this->load->view("templates/footer");
    }

    public function server($method = null) {
        if (isset($_POST) and ! empty($_POST)) {
            $this->load->library("form_validation");

            if (($method == "create") or ( $method == "update")) {
                $this->form_validation->set_rules("user_name", "user_name", "is_unique[robotoit_user.user_name]|trim|required|xss_clean");
              
            }
            if (($method == "create") or ( $method == "change")) {
                $this->form_validation->set_rules("user_password", "user_password", "trim|required|xss_clean");
                $this->form_validation->set_rules('user_password', 'user_password1', 'required|matches[user_password]|xss_clean');
            }


            return $this->form_validation->run();
        } else {
            return FALSE;
        }
    }

    public function client($method = null) {
        $this->load->library("validate");
        $this->validate->setId("robotoit_user");

        if (($method == "create") or ( $method == "update")) {
            $this->validate->clientInputEmpty("user_name");
        }
        if (($method == "create") or ( $method == "change")) {
            $this->validate->clientInputEmpty("user_password");
            $this->validate->clientMatch("user_password", "user_password1");
        }

        return $this->validate->getJscript();
    }

    private function do_upload() {
        $config['upload_path'] = './assets/galeria/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '4000';
        $config['max_width'] = '4000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        return $this->upload->do_upload("imagen");
    }

    private function resize_image($path = null) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = 60;
        $config['width'] = 600;
        $config['height'] = 800;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

}
