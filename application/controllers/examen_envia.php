<?php



// created 19-03-15 01:53:18

if (!defined("BASEPATH"))

    die();



class Examen_envia extends CI_Controller {



    public function index() {

        $this->load->view("templates/header");

        $this->load->view("templates/aside");

        $this->load->view("templates/footer");

    }



    public function view() {



        $this->load->model("examen_envia_model");

        $this->config->load("cms");

        /**/$data["item"] = $this->config->item("user_nav");

        $data["examen_envia"] = $this->examen_envia_model->get();

        //$data["keys"]=array_keys($data["examen_envia"][0]);

        $this->load->view("templates/header");

        $this->load->view("templates/menubar", $data);

        $this->load->view("users/examen_envia/show_examen_envia", $data);

        $this->load->view("templates/footer");

    }



    public function admin() {



        $this->load->model("examen_envia_model");

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");

        $data["examen_envia"] = $this->examen_envia_model->get();

        //$data["keys"]=array_keys($data["examen_envia"][0]);

        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        //$this->load->view("admin/examen_envia/show_examen_envia", $data);
	  $this->load->view("admin/examen_envia/buscar_examen", $data);

        $this->load->view("templates/footer");

    }



    public function find() {



        $this->load->model("examen_envia_model");

        $this->config->load("cms");

        /**/$data["item"] = $this->config->item("user_nav");

        $data["id"] = $this->uri->segment(3);

        $data["examen_envia"] = $this->examen_envia_model->getOne($data["id"]);

        $this->load->view("templates/header");

        $this->load->view("templates/menubar", $data);

        $this->load->view("users/examen_envia/find_examen_envia", $data);

        $this->load->view("templates/footer");

    }



    public function create() {

        $this->load->model("examen_envia_model");



        $data["client"] = $this->client();

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");



        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {

            $this->examen_envia_model->add();

            redirect("/examen_envia/admin/", "refresh");

        }

        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/examen_envia/add_examen_envia", $data);

        $this->load->view("templates/footer");

    }



    public function update() {

        $this->load->model("examen_envia_model");



        $data["client"] = $this->client();

        $data["id"] = $this->uri->segment(3);

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");

        if ($this->server() AND ( isset($data["id"]) and $data["id"] != "") and ( isset($_POST) and ! empty($_POST))) {

            $this->examen_envia_model->update($this->uri->rsegment(3));

            redirect("/examen_envia/admin/", "refresh");

        }

        $data["examen_envia"] = $this->examen_envia_model->getOne($data["id"]);

        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/examen_envia/edit_examen_envia", $data);

        $this->load->view("templates/footer");

    }



    public function delete() {

        $data["id"] = $this->uri->segment(3);

        $this->load->model("examen_envia_model");

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");

        if ((isset($data["id"]) and $data["id"] != "") and ! empty($_POST)) {

            if ($_POST["confirmar"] === "TRUE") {

                $this->examen_envia_model->delete($data["id"]);

                redirect("/examen_envia/admin/TRUE/", "refresh");

            } else {

                redirect("/examen_envia/admin/FALSE/", "refresh");

            }

        }



        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/examen_envia/delete_examen_envia", $data);

        $this->load->view("templates/footer");

    }



    public function server() {

        if (isset($_POST) and ! empty($_POST)) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("aciertos", "aciertos", "trim|required|xss_clean");

            $this->form_validation->set_rules("intentos", "intentos", "trim|required|xss_clean");

            $this->form_validation->set_rules("usuario", "usuario", "trim|required|xss_clean");



            return $this->form_validation->run();

        } else {

            return FALSE;

        }

    }



    public function client() {

        $this->load->library("validate");

        $this->validate->setId("examen_envia");

        $this->validate->clientInputEmpty("aciertos");

        $this->validate->clientInputEmpty("intentos");

        $this->validate->clientInputEmpty("usuario");



        return $this->validate->getJscript();

    }



}

