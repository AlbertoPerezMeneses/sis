<?php

// created 19-03-15 01:53:18

if (!defined("BASEPATH"))

    die();

include_once APPPATH . "libraries/REST_Controller.php";



class mailService extends REST_Controller {

    //put your code here

    public function index_get(){

        $this->response("hello",200);

    }

    public function send_oferta_post() {



        $this->load->library('parser');

        

        $data["user"]= $this->post("user");

        $data["pass"]=$this->post("pass");

        $data["to"]=$this->post("to");

        $data["message"] = $this->parser->parse('mail/formato_inicio', $data);

        

        $data["debug"]=$this->correo($data["to"], "Bienvenido al sistema de inscripciones UAM Xochimilco", $data["message"]);

        

        $this->response($data,200);

    }

    


     public function send_inscrito_post() {
        $this->load->library('parser');
        $this->load->model("usuario_model");
        $data["user"]= $this->post("id");
        
        $data["usuario"] =$this->usuario_model->getOne(md5($data["user"]));
        
        $data["message"] = $this->parser->parse('mail/formato_inscrito', $data["usuario"][0]);
        
        $data["info_mail"]=$this->correo($data["usuario"][0]["correo"], "Universidad Autonoma Metropolitana, Unidad Xochimilco", $data["message"]);
        
	$data["nose"]=$data["usuario"][0]["correo"];
	$data["response"]=$this->email->print_debugger();
        $this->response($data,200);
    }
    
    

    

    private  function correo($address=null,$subject=null,$message=null){

	$config['charset'] = 'utf-8';
	$config['mailtype'] = 'html';

	$config['wrapchars'] = 512;

	$this->load->library('email');
	$this->email->initialize($config);

               
	$result["address"]=trim ($address);        

        $this->email->to(trim ($address));

        $this->email->from('noreply@robotoit.com',"comunidad uam");

	 $this->email->reply_to('educacion_virtual@correo.xoc.uam.mx', 'CECAD');

        $this->email->subject(trim ($subject));

        $this->email->message($message);

        $result["result"]=$this->email->send();

        //echo $this->email->print_debugger();

        return $result;

    }



}

