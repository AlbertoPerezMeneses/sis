<?php

if (!defined('BASEPATH'))
    die();

class Login extends CI_Controller {

    public function index() {
        $this->load->model("login_model");
        $data = $this->login_model->login();


        /**/if ($this->server()) {
            $data["server"] = "";
        } else
        /**/ {
            $data["server"] = "";
        }

        $data["server"] = $this->session_is_started();
        $data["client"] = $this->client();
        $this->load->view("templates/header");
        $this->load->view("admin/login", $data);
        $this->load->view("templates/footer");
    }

    public function close() {
        session_start();
        session_unset();
        session_destroy();
        redirect("/", "refresh");
    }

    private function session_is_started() {
        session_start();

        if (isset($_SESSION["status"])) {
            /* Aqui se define la ruta que va a tomar dependiendo del valor de sua */
            redirect("panel/");
        }
    }

    private function server() {
        $this->load->library("validate");
        if (isset($_POST) and !empty($_POST)) {
            $this->validate->inputEmpty("username");
            $this->validate->inputEmpty("password");
            return $this->validate->validateAll();
        } else {
            return false;
        }
    }

    private function client() {
        $this->load->library("validate");
        $this->validate->setId("login");
        $this->validate->clientInputEmpty("username");
        $this->validate->clientInputEmpty("password");
        return $this->validate->getJScript();
    }

}
