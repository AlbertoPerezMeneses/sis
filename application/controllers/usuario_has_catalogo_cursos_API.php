<?php



// created 17-03-15 02:48:10

if (!defined("BASEPATH"))

    die();

include_once APPPATH . "libraries/REST_Controller.php";



class Usuario_has_catalogo_cursos_API extends REST_Controller {



    public function show_all_get() {



        $this->load->model("usuario_has_catalogo_cursos_model");

        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get();

        $this->response($data, 200);

    }



    public function find_by_id_get() {



        $this->load->model("usuario_has_catalogo_cursos_model");



        $data["id"] = $this->get("id", TRUE);

        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->getOne($data["id"]);

        $this->response($data, 200);

    }


 public function find_by_folio_capacitacion_get() {



        $this->load->model("usuario_has_catalogo_cursos_model");



        $data["id"] = $this->get("id", TRUE);

        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get_intentosByFolio($data["id"]);

        $this->response($data, 200);

    }




    public function create_post() {

        $this->load->model("usuario_has_catalogo_cursos_model");



        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {

            $this->usuario_has_catalogo_cursos_model->add();

        } else {



            $data["usuario"] = $this->usuario_has_catalogo_cursos_model->get_usuario_nombre_del_trabajador();



            $data["catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get_catalogo_cursos_nombre_catalogo();

        }

        $this->response($data, 200);

    }



    public function edit_post() {

        $this->load->model("usuario_has_catalogo_cursos_model");



        $data["usuario"] = $this->usuario_has_catalogo_cursos_model->get_usuario_nombre_del_trabajador();



        $data["catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->get_catalogo_cursos_nombre_catalogo();



        $data["id"] = $this->post("id", TRUE);



        if ($this->server() AND ( isset($_data["id"]) and $_data["id"] > 0) and ( isset($_POST) and ! empty($_POST))) {

            $this->usuario_has_catalogo_cursos_model->update($data["id"]);

        }

        $data["usuario_has_catalogo_cursos"] = $this->usuario_has_catalogo_cursos_model->getOne($data["id"]);

        $this->response($data, 200);

    }



    public function delete_post() {

        $data["id"] = $this->post("id");

        $this->load->model("usuario_has_catalogo_cursos_model");

        $data["validation"] = (isset($data["id"]) and $data["id"] > 0) and ! empty($_POST);

        if ($data["validation"]) {

            if ($_POST["confirmar"] === "TRUE") {

                $this->usuario_has_catalogo_cursos_model->delete($data["id"]);

            }

            $this->response($data, 200);

        }

    }



    public function es_matriculable_get() {

        $data["id"] = $this->get("id");

        $this->load->model("usuario_has_catalogo_cursos_model");

        $data["count"] = $this->usuario_has_catalogo_cursos_model->getCountByFolio($data["id"]);

        $this->response($data);

    }



    public function guarda_oferta_post() {

        $data["id"] = $this->post("searchUser", true);

        $data["items"] = $this->post("cursos", true);



        $this->load->model("usuario_has_catalogo_cursos_model");

        $this->usuario_has_catalogo_cursos_model->multiple_add($data["id"], $data["items"]);



        $this->response($data, 200);

    }



    public function server() {

        if (isset($_POST) and ! empty($_POST)) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("intentos", "intentos", "trim|xss_clean");

            $this->form_validation->set_rules("usuario_id_usuario", "usuario_id_usuario", "trim|required|xss_clean|integer");

            $this->form_validation->set_rules("catalogo_cursos_id_catalogo_cursos", "catalogo_cursos_id_catalogo_cursos", "trim|required|xss_clean|integer");

            return $this->form_validation->run();

        } else {

            return FALSE;

        }

    }



}

