<?php // created 13-03-15 01:20:36
if (!defined("BASEPATH")) die();
class Catalogo_cursos extends CI_Controller {

   public function index()
	{
      $this->load->view("templates/header");
	  $this->load->view("templates/aside");
      $this->load->view("templates/footer");
	}
   public function view()
   {
   
	$this->load->model("catalogo_cursos_model");
	$this->config->load("cms");
    /**/$data["item"]=$this->config->item("user_nav");
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->get();
	//$data["keys"]=array_keys($data["catalogo_cursos"][0]);
	$this->load->view("templates/header");
	$this->load->view("templates/menubar",$data);
	$this->load->view("users/catalogo_cursos/show_catalogo_cursos",$data);
    $this->load->view("templates/footer");
   }
    public function admin()
   {
   
	$this->load->model("catalogo_cursos_model");
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->get();
	//$data["keys"]=array_keys($data["catalogo_cursos"][0]);
	$this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/catalogo_cursos/show_catalogo_cursos",$data);
    $this->load->view("templates/footer");
   }
   public function find()
   {
   
	$this->load->model("catalogo_cursos_model");
	$this->config->load("cms");
    /**/$data["item"]=$this->config->item("user_nav");
	$data["id"]=$this->uri->segment(3);
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->getOne($data["id"]);
    $this->load->view("templates/header");
	$this->load->view("templates/menubar",$data);
	$this->load->view("users/catalogo_cursos/find_catalogo_cursos",$data);
    $this->load->view("templates/footer");
   }
   public function create()
   {
   $this->load->model("catalogo_cursos_model");
   
    $data["client"]=$this->client();
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
    
	if($this->server() AND (isset($_POST) AND !empty($_POST)))
	{
	$this->catalogo_cursos_model->add();
        redirect("/catalogo_cursos/admin/", "refresh");
	}
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/catalogo_cursos/add_catalogo_cursos",$data);
    $this->load->view("templates/footer");
   }
      public function update()
   {
   $this->load->model("catalogo_cursos_model");
   
    $data["client"]=$this->client();
    $data["id"]=$this->uri->segment(3);
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
	if($this->server() AND (isset($data["id"]) and $data["id"]!="") and (isset($_POST) and !empty($_POST))){
	$this->catalogo_cursos_model->update($this->uri->rsegment(3));
            redirect("/catalogo_cursos/admin/", "refresh");
	}
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->getOne($data["id"]);
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/catalogo_cursos/edit_catalogo_cursos",$data);
    $this->load->view("templates/footer");
   }
   public function delete()
   {
	$data["id"]=$this->uri->segment(3);
	$this->load->model("catalogo_cursos_model");
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
    if((isset($data["id"]) and $data["id"] != "") and !empty($_POST)){
        if($_POST["confirmar"]==="TRUE"){
	$this->catalogo_cursos_model->delete($data["id"]);
        redirect("/catalogo_cursos/admin/TRUE/", "refresh");
	}
	else
	{
	redirect("/catalogo_cursos/admin/FALSE/", "refresh");
	}
      }
	
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/catalogo_cursos/delete_catalogo_cursos",$data);
    $this->load->view("templates/footer");
   }
	public function server()
	{
	if(isset($_POST) and !empty($_POST))
	{
	$this->load->library("form_validation");
	$this->form_validation->set_rules("nombre_catalogo","nombre_catalogo","trim|required|xss_clean");
					
	return $this->form_validation->run();
	}
        else
        {
        return FALSE;
        }
	}
	public function client()
	{
	$this->load->library("validate");
	$this->validate->setId("catalogo_cursos");
	$this->validate->clientInputEmpty("nombre_catalogo");
				
	return $this->validate->getJscript();
	}
}
