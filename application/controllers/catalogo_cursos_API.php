<?php // created 13-03-15 01:20:36
            if (!defined("BASEPATH")) die();
include_once APPPATH . "libraries/REST_Controller.php";
class Catalogo_cursos_API extends REST_Controller {

   public function show_all_get()
   {
   
	$this->load->model("catalogo_cursos_model");	
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->get();
        $this->response($data, 200);
   }
    
   public function find_by_id_get()
   {
   
	$this->load->model("catalogo_cursos_model");
	
	$data["id"]=$this->get("id", TRUE);
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->getOne($data["id"]);
        $this->response($data, 200);
   
   }
   public function create_post()
   {
   $this->load->model("catalogo_cursos_model");
       
	if($this->server() AND (isset($_POST) AND !empty($_POST)))
	{
	$this->catalogo_cursos_model->add();
    	}else{
   
    	}
    $this->response($data, 200);
   }
      public function edit_post()
   {
   $this->load->model("catalogo_cursos_model");
       
    $data["id"]=$this->post("id",TRUE);
	
	if($this->server() AND (isset($_data["id"]) and $_data["id"]>0) and (isset($_POST) and !empty($_POST))){
	$this->catalogo_cursos_model->update($data["id"]);
        
	}
	$data["catalogo_cursos"]=$this->catalogo_cursos_model->getOne($data["id"]);
    $this->response($data, 200);
   }
   public function delete_post()
   {
	$data["id"]=$this->post("id");
	$this->load->model("catalogo_cursos_model");
	$data["validation"]=(isset($data["id"]) and $data["id"]>0) and !empty($_POST);
    if($data["validation"]){
        if($_POST["confirmar"]==="TRUE"){
	$this->catalogo_cursos_model->delete($data["id"]);        
	}
        $this->response($data,200);
      }	
   }
	public function server()
	{
	if(isset($_POST) and !empty($_POST))
	{
	$this->load->library("form_validation");
	$this->form_validation->set_rules("nombre_catalogo","nombre_catalogo","trim|required|xss_clean");
					
	return $this->form_validation->run();
	}else
        {
        return FALSE;
        }
	}
	
}
