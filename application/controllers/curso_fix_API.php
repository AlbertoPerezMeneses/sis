<?php

// created 11-06-15 06:28:10
if (!defined("BASEPATH"))
    die();
include_once APPPATH . "libraries/REST_Controller.php";

class Curso_fix_API extends REST_Controller {

    public function show_all_get() {

        $this->load->model("curso_fix_model");
        $data["curso_fix"] = $this->curso_fix_model->get();
        $this->response($data, 200);
    }

    public function find_by_id_get() {

        $this->load->model("curso_fix_model");

        $data["id"] = $this->get("id", TRUE);
        $data["curso_fix"] = $this->curso_fix_model->getOne($data["id"]);
        $this->response($data, 200);
    }

    public function create_post() {
        $this->load->model("curso_fix_model");
        $this->load->model("robotoit_user_model");

        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {
            $user = $this->robotoit_user_model->getOne($this->post("robotoit_user_id_user"));
            $data=$this->post();
            $data["robotoit_user_id_user"]=$user[0]["id_user"];
            $data["query"]= $this->curso_fix_model->add_by_data($data);            
        } else {
            
        }
        $this->response($data, 200);
    }
    public function count_curso_get() {
         $this->load->model("curso_fix_model");
         $data=$this->get();
         $data= $this->curso_fix_model->cuentaCurso($this->get("curso_asignado"),$this->get("turno"),$this->get("modalidad"),$this->get("sede"),$this->get("fecha_inicio"));  
         $data["query"]=$this->db->queries;
         $this->response($data, 200);
    }
    public function edit_post() {
        $this->load->model("curso_fix_model");

        $data["id"] = $this->post("id", TRUE);

        if ($this->server() AND ( isset($_data["id"]) and $_data["id"] > 0) and ( isset($_POST) and ! empty($_POST))) {
            $this->curso_fix_model->update($data["id"]);
        }
        $data["curso_fix"] = $this->curso_fix_model->getOne($data["id"]);
        $this->response($data, 200);
    }

    public function delete_post() {
        $data["id"] = $this->post("id");
        $this->load->model("curso_fix_model");
        $data["validation"] = (isset($data["id"]) and $data["id"] > 0) and ! empty($_POST);
        if ($data["validation"]) {
            if ($_POST["confirmar"] === "TRUE") {
                $this->curso_fix_model->delete($data["id"]);
            }
            $this->response($data, 200);
        }
    }

    public function server() {
        if (isset($_POST) and ! empty($_POST)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("curso_sede", "curso_sede", "trim|required|xss_clean");
            $this->form_validation->set_rules("turno", "turno", "trim|required|xss_clean");
            $this->form_validation->set_rules("fecha_inicio", "fecha_inicio", "trim|xss_clean");
            $this->form_validation->set_rules("modalidad", "modalidad", "trim|xss_clean");
            $this->form_validation->set_rules("usuario_id_usuario", "usuario_id_usuario", "trim|required|xss_clean");
            $this->form_validation->set_rules("robotoit_user_id_user", "robotoit_user_id_user", "trim|xss_clean");
            $this->form_validation->set_rules("fecha_inscripcion", "fecha_inscripcion", "trim|xss_clean");

            return $this->form_validation->run();
        } else {
            return FALSE;
        }
    }

}
