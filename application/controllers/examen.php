<?php

// created 13-03-15 01:20:36
if (!defined("BASEPATH"))
    die();

class Examen extends CI_Controller {

    public function index() {
        $this->load->view("templates/header");
        $this->load->view("templates/aside");
        $this->load->view("templates/footer");
    }

    public function view() {

        $this->load->model("examen_model");
        $this->config->load("cms");
        /**/$data["item"] = $this->config->item("user_nav");
        $data["examen"] = $this->examen_model->get();
        //$data["keys"]=array_keys($data["examen"][0]);
        $this->load->view("templates/header");
        $this->load->view("templates/menubar", $data);
        $this->load->view("users/examen/show_examen", $data);
        $this->load->view("templates/footer");
    }

    public function admin() {

        $this->load->model("examen_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        $data["examen"] = $this->examen_model->get();
        //$data["keys"]=array_keys($data["examen"][0]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/examen/show_examen", $data);
        $this->load->view("templates/footer");
    }

    public function find() {

        $this->load->model("examen_model");
        $this->config->load("cms");
        /**/$data["item"] = $this->config->item("user_nav");
        $data["id"] = $this->uri->segment(3);
        $data["examen"] = $this->examen_model->getOne($data["id"]);
        $this->load->view("templates/header");
        $this->load->view("templates/menubar", $data);
        $this->load->view("users/examen/find_examen", $data);
        $this->load->view("templates/footer");
    }

    public function create() {
        $this->load->model("examen_model");

        $data["usuario"] = $this->examen_model->get_usuario_nombre_del_trabajador();

        $data["catalogo_cursos"] = $this->examen_model->get_catalogo_cursos_nombre_catalogo();

        $data["client"] = $this->client();
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");

        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {
            $this->examen_model->add();
            redirect("/examen/admin/", "refresh");
        }
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/examen/add_examen", $data);
        $this->load->view("templates/footer");
    }

    public function update() {
        $this->load->model("examen_model");

        $data["usuario"] = $this->examen_model->get_usuario_nombre_del_trabajador();

        $data["catalogo_cursos"] = $this->examen_model->get_catalogo_cursos_nombre_catalogo();

        $data["client"] = $this->client();
        $data["id"] = $this->uri->segment(3);
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        if ($this->server() AND ( isset($data["id"]) and $data["id"] != "") and ( isset($_POST) and ! empty($_POST))) {
            $this->examen_model->update($this->uri->rsegment(3));
            redirect("/examen/admin/", "refresh");
        }
        $data["examen"] = $this->examen_model->getOne($data["id"]);
        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/examen/edit_examen", $data);
        $this->load->view("templates/footer");
    }

    public function delete() {
        $data["id"] = $this->uri->segment(3);
        $this->load->model("examen_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        if ((isset($data["id"]) and $data["id"] != "") and ! empty($_POST)) {
            if ($_POST["confirmar"] === "TRUE") {
                $this->examen_model->delete($data["id"]);
                redirect("/examen/admin/TRUE/", "refresh");
            } else {
                redirect("/examen/admin/FALSE/", "refresh");
            }
        }

        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/examen/delete_examen", $data);
        $this->load->view("templates/footer");
    }

    public function server() {
        if (isset($_POST) and ! empty($_POST)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("nombre_examen", "nombre_examen", "trim|required|xss_clean");
            $this->form_validation->set_rules("calificacion", "calificacion", "trim|required|xss_clean");
            $this->form_validation->set_rules("usuario_id_usuario", "usuario_id_usuario", "trim|required|xss_clean|integer");
            //$data["usuario"]=$this->examen_model->get_usuario_nombre_del_trabajador();
            $this->form_validation->set_rules("catalogo_cursos_id_catalogo_cursos", "catalogo_cursos_id_catalogo_cursos", "trim|required|xss_clean|integer");
            //$data["catalogo_cursos"]=$this->examen_model->get_catalogo_cursos_nombre_catalogo();

            return $this->form_validation->run();
        } else {
            return FALSE;
        }
    }

    public function client() {
        $this->load->library("validate");
        $this->validate->setId("examen");
        $this->validate->clientInputEmpty("nombre_examen");
        $this->validate->clientInputEmpty("calificacion");

        return $this->validate->getJscript();
    }

}
