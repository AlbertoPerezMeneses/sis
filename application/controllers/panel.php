<?php

if (!defined('BASEPATH'))
    die();

class Panel extends CI_Controller {

    public function index() {
      
        $this->config->load("cms");
        $data["item"] = $this->config->item("admin_nav");
        $this->load->view("templates/header");
        $this->load->view("templates/adminmenu", $data);


        if (isset($_SESSION) and ! empty($_SESSION)) {
            if ($_SESSION["status"] == "2") {
                $this->load->view("admin/panel/editor");
            } elseif ($_SESSION ["status"]== "1") {
                $this->load->view("admin/panel/admin");
            }
            $this->load->view("templates/footer");
        } else {
            redirect("/login/", "refresh");
        }
    }

    
}
