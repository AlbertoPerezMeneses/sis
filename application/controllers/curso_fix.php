<?php // created 11-06-15 06:28:10
if (!defined("BASEPATH")) die();
class Curso_fix extends CI_Controller {

   public function index()
	{
      $this->load->view("templates/header");
	  $this->load->view("templates/aside");
      $this->load->view("templates/footer");
	}
   public function view()
   {
   
	$this->load->model("curso_fix_model");
	$this->config->load("cms");
    /**/$data["item"]=$this->config->item("user_nav");
	$data["curso_fix"]=$this->curso_fix_model->get();
	//$data["keys"]=array_keys($data["curso_fix"][0]);
	$this->load->view("templates/header");
	$this->load->view("templates/menubar",$data);
	$this->load->view("users/curso_fix/show_curso_fix",$data);
    $this->load->view("templates/footer");
   }
    public function admin()
   {
   
	$this->load->model("curso_fix_model");
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
	$data["curso_fix"]=$this->curso_fix_model->get();
	//$data["keys"]=array_keys($data["curso_fix"][0]);
	$this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso_fix/show_curso_fix",$data);
    $this->load->view("templates/footer");
   }
   public function find()
   {
   
	$this->load->model("curso_fix_model");
	$this->config->load("cms");
    /**/$data["item"]=$this->config->item("user_nav");
	$data["id"]=$this->uri->segment(3);
	$data["curso_fix"]=$this->curso_fix_model->getOne($data["id"]);
    $this->load->view("templates/header");
	$this->load->view("templates/menubar",$data);
	$this->load->view("users/curso_fix/find_curso_fix",$data);
    $this->load->view("templates/footer");
   }
   public function create()
   {
   $this->load->model("curso_fix_model");
   
    $data["client"]=$this->client();
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
    
	if($this->server() AND (isset($_POST) AND !empty($_POST)))
	{
	$this->curso_fix_model->add();
        redirect("/curso_fix/admin/", "refresh");
	}
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso_fix/add_curso_fix",$data);
    $this->load->view("templates/footer");
   }
      public function update()
   {
   $this->load->model("curso_fix_model");
   
    $data["client"]=$this->client();
    $data["id"]=$this->uri->segment(3);
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
	if($this->server() AND (isset($data["id"]) and $data["id"]!="") and (isset($_POST) and !empty($_POST))){
	$this->curso_fix_model->update($this->uri->rsegment(3));
            redirect("/curso_fix/admin/", "refresh");
	}
	$data["curso_fix"]=$this->curso_fix_model->getOne($data["id"]);
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso_fix/edit_curso_fix",$data);
    $this->load->view("templates/footer");
   }
   public function delete()
   {
	$data["id"]=$this->uri->segment(3);
	$this->load->model("curso_fix_model");
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
    if((isset($data["id"]) and $data["id"] != "") and !empty($_POST)){
        if($_POST["confirmar"]==="TRUE"){
	$this->curso_fix_model->delete($data["id"]);
        redirect("/curso_fix/admin/TRUE/", "refresh");
	}
	else
	{
	redirect("/curso_fix/admin/FALSE/", "refresh");
	}
      }
	
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso_fix/delete_curso_fix",$data);
    $this->load->view("templates/footer");
   }
	public function server()
	{
	if(isset($_POST) and !empty($_POST))
	{
	$this->load->library("form_validation");
	$this->form_validation->set_rules("curso_sede","curso_sede","trim|required|xss_clean");
					$this->form_validation->set_rules("turno","turno","trim|required|xss_clean");
					$this->form_validation->set_rules("fecha_inicio","fecha_inicio","trim|xss_clean");
                                $this->form_validation->set_rules("modalidad","modalidad","trim|xss_clean");
                                $this->form_validation->set_rules("usuario_id_usuario","usuario_id_usuario","trim|required|xss_clean");
					$this->form_validation->set_rules("robotoit_user_id_user","robotoit_user_id_user","trim|xss_clean");
                                $this->form_validation->set_rules("fecha_inscripcion","fecha_inscripcion","trim|xss_clean");
                                
	return $this->form_validation->run();
	}
        else
        {
        return FALSE;
        }
	}
	public function client()
	{
	$this->load->library("validate");
	$this->validate->setId("curso_fix");
	$this->validate->clientInputEmpty("curso_sede");
				$this->validate->clientInputEmpty("turno");
				$this->validate->clientInputEmpty("usuario_id_usuario");
				
	return $this->validate->getJscript();
	}
}
