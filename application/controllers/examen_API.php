<?php

// created 13-03-15 01:20:36
if (!defined("BASEPATH"))
    die();
include_once APPPATH . "libraries/REST_Controller.php";

class Examen_API extends REST_Controller {

    public function show_all_get() {

        $this->load->model("examen_model");
        $data["examen"] = $this->examen_model->get();
        $this->response($data, 200);
    }

    public function find_by_id_get() {

        $this->load->model("examen_model");

        $data["id"] = $this->get("id", TRUE);
        $data["examen"] = $this->examen_model->getOne($data["id"]);
        $this->response($data, 200);
    }

    public function create_post() {
        $this->load->model("examen_model");

        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {
            $this->examen_model->add();
        } else {

            $data["usuario"] = $this->examen_model->get_usuario_nombre_del_trabajador();

            $data["catalogo_cursos"] = $this->examen_model->get_catalogo_cursos_nombre_catalogo();
        }
        $this->response($data, 200);
    }

    public function edit_post() {
        $this->load->model("examen_model");

        $data["usuario"] = $this->examen_model->get_usuario_nombre_del_trabajador();

        $data["catalogo_cursos"] = $this->examen_model->get_catalogo_cursos_nombre_catalogo();

        $data["id"] = $this->post("id", TRUE);

        if ($this->server() AND ( isset($_data["id"]) and $_data["id"] > 0) and ( isset($_POST) and ! empty($_POST))) {
            $this->examen_model->update($data["id"]);
        }
        $data["examen"] = $this->examen_model->getOne($data["id"]);
        $this->response($data, 200);
    }

    public function delete_post() {
        $data["id"] = $this->post("id");
        $this->load->model("examen_model");
        $data["validation"] = (isset($data["id"]) and $data["id"] > 0) and ! empty($_POST);
        if ($data["validation"]) {
            if ($_POST["confirmar"] === "TRUE") {
                $this->examen_model->delete($data["id"]);
            }
            $this->response($data, 200);
        }
    }

    public function server() {
        if (isset($_POST) and ! empty($_POST)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("nombre_examen", "nombre_examen", "trim|required|xss_clean");
            $this->form_validation->set_rules("calificacion", "calificacion", "trim|required|xss_clean");
          //  $this->form_validation->set_rules("usuario_id_usuario", "usuario_id_usuario", "trim|required|xss_clean|integer");
          //  $this->form_validation->set_rules("catalogo_cursos_id_catalogo_cursos", "catalogo_cursos_id_catalogo_cursos", "trim|required|xss_clean|integer");
            return $this->form_validation->run();
        } else {
            return FALSE;
        }
    }

}
