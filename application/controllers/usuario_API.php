<?php



// created 19-03-15 12:37:07

if (!defined("BASEPATH"))

    die();

include_once APPPATH . "libraries/REST_Controller.php";



class Usuario_API extends REST_Controller {



    public function show_all_get() {



        $this->load->model("usuario_model");

        $data["usuario"] = $this->usuario_model->get();

        $this->response($data, 200);

    }



    public function find_by_id_get() {



        $this->load->model("usuario_model");



        $data["id"] = $this->get("id", TRUE);

        $data["usuario"] = $this->usuario_model->getOne($data["id"]);

        $this->response($data, 200);

    }

    public function find_result_capacitacion_get() {

        $this->load->model("usuario_model");

        $data["id"] = $this->get("id", TRUE);
        $data["usuario"] = $this->usuario_model->get_course_result($data["id"]);
        $this->response($data, 200);
    }
    

    public function find_status_matriculacion_get() {

        $this->load->model("usuario_model");

        $data["id"] = $this->get("id", TRUE);
        $data["usuario"] = $this->usuario_model->get_status_matriculacion_by_folio($data["id"]);
        $this->response($data, 200);
    }


    

      public function find_by_folio_capacitacion_get() {
        $this->load->model("usuario_model");
        $data["id"] = $this->get("id", TRUE);
        $data["usuario"] = $this->usuario_model->get_by_folio_capacitacion($data["id"]);
        $this->response($data, 200);
    }


    public function busca_by_folio_capacitacion_get() {

        $this->load->model("usuario_model");

        $data["id"] = $this->get("id", TRUE);
        $data["usuario"] = $this->usuario_model->find_by_folio_capacitacion($data["id"]);
        $this->response($data, 200);
    }


    public function create_post() {

        $this->load->model("usuario_model");



        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {

            $this->usuario_model->add();

        } else {

            

        }

        $this->response($data, 200);

    }


 public function terminar_proceso_post(){
        $this->load->model("usuario_model");
        $data["id"] = $this->post("id", TRUE);
        $this->usuario_model->termina_proceso($data["id"]);
        $this->response($data, 200);
    }

public function rescindir_proceso_post(){
        $this->load->model("usuario_model");
        $data["id"] = $this->post("id", TRUE);
        $this->usuario_model->rescinde_proceso($data["id"]);
        $this->response($data, 200);
    }


     public function reiniciar_proceso_post(){
        $this->load->model("usuario_model");
        $data["id"] = $this->post("id", TRUE);
        $data["result"]=$this->usuario_model->reiniciar_usuario($data["id"]);        
        //$data["queries"]=$this->db->queries;
        $this->response($data, 200);
    }


    public function edit_post() {

        $this->load->model("usuario_model");



        $data["id"] = $this->post("id", TRUE);



        if ($this->server() AND ( isset($_data["id"]) and $_data["id"] > 0) and ( isset($_POST) and ! empty($_POST))) {

            $this->usuario_model->update($data["id"]);

        }

        $data["usuario"] = $this->usuario_model->getOne($data["id"]);

        $this->response($data, 200);

    }

         function registro_get() {        
            $user_name=$this->get("user_name");
            $this->load->model("usuario_has_catalogo_cursos_model");
            $this->load->model("usuario_model");
            $this->load->model("catalogo_cursos_model");
            $usuario = $this->usuario_model->find_by_folio_capacitacion($user_name);
            $id_usuario = $usuario[0]["id_usuario"];

 	    if (isset($usuario[0]["curso_matriculado"]) and $usuario[0]["curso_matriculado"] != null and ! empty($usuario[0]["curso_matriculado"])) {
                $curso_inscrito = $this->catalogo_cursos_model->get_One($usuario[0]["curso_matriculado"]);
                $data["curso_inscrito"] = $curso_inscrito[0]["nombre_catalogo"];
            }
            $cursos = $this->usuario_has_catalogo_cursos_model->getByFolio($id_usuario);


	if($usuario[0]["status_matriculacion"]==2 OR $usuario[0]["status_matriculacion"]==3){
	$usuario = $this->usuario_model->get_resultado($user_name);	
	}

            $data["cursos"] = $cursos;
            $data["usuario"] = $usuario;
	 $data["queries"]=$this->db->queries;
	$this->response($data,200);
        }


    public function delete_post() {

        $data["id"] = $this->post("id");

        $this->load->model("usuario_model");

        $data["validation"] = (isset($data["id"]) and $data["id"] > 0) and ! empty($_POST);

        if ($data["validation"]) {

            if ($_POST["confirmar"] === "TRUE") {

                $this->usuario_model->delete($data["id"]);

            }

            $this->response($data, 200);

        }

    }



    public function server() {

        if (isset($_POST) and ! empty($_POST)) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("folio_capacitacion", "folio_capacitacion", "trim|required|xss_clean");

            $this->form_validation->set_rules("nombre_del_trabajador", "nombre_del_trabajador", "trim|required|xss_clean");

            $this->form_validation->set_rules("rfc", "rfc", "trim|required|xss_clean");

            $this->form_validation->set_rules("correo", "correo", "trim|required|xss_clean|valid_email");

            $this->form_validation->set_rules("matriculacion_id_matriculacion", "matriculacion_id_matriculacion", "trim|xss_clean");



            return $this->form_validation->run();

        } else {

            return FALSE;

        }

    }



}

