<?php // created 05-06-15 09:07:45
if (!defined("BASEPATH")) die();
class Curso extends CI_Controller {

   public function index()
	{
      $this->load->view("templates/header");
	  $this->load->view("templates/aside");
      $this->load->view("templates/footer");
	}
   public function view()
   {
   
	$this->load->model("curso_model");
	$this->config->load("cms");
    /**/$data["item"]=$this->config->item("user_nav");
	$data["curso"]=$this->curso_model->get();
	//$data["keys"]=array_keys($data["curso"][0]);
	$this->load->view("templates/header");
	$this->load->view("templates/menubar",$data);
	$this->load->view("users/curso/show_curso",$data);
    $this->load->view("templates/footer");
   }
    public function admin()
   {
   
	$this->load->model("curso_model");
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
	$data["curso"]=$this->curso_model->get();
	//$data["keys"]=array_keys($data["curso"][0]);
	$this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso/show_curso",$data);
    $this->load->view("templates/footer");
   }
   public function find()
   {
   
	$this->load->model("curso_model");
	$this->config->load("cms");
    /**/$data["item"]=$this->config->item("user_nav");
	$data["id"]=$this->uri->segment(3);
	$data["curso"]=$this->curso_model->getOne($data["id"]);
    $this->load->view("templates/header");
	$this->load->view("templates/menubar",$data);
	$this->load->view("users/curso/find_curso",$data);
    $this->load->view("templates/footer");
   }
   public function create()
   {
   $this->load->model("curso_model");
   
    $data["usuario"]=$this->curso_model->get_usuario_folio_capacitacion();
    
    $data["robotoit_user"]=$this->curso_model->get_robotoit_user_user_name();
    
    $data["client"]=$this->client();
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
    
	if($this->server() AND (isset($_POST) AND !empty($_POST)))
	{
	$this->curso_model->add();
        redirect("/curso/admin/", "refresh");
	}
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso/add_curso",$data);
    $this->load->view("templates/footer");
   }
      public function update()
   {
   $this->load->model("curso_model");
   
    $data["usuario"]=$this->curso_model->get_usuario_folio_capacitacion();
    
    $data["robotoit_user"]=$this->curso_model->get_robotoit_user_user_name();
    
    $data["client"]=$this->client();
    $data["id"]=$this->uri->segment(3);
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
	if($this->server() AND (isset($data["id"]) and $data["id"]!="") and (isset($_POST) and !empty($_POST))){
	$this->curso_model->update($this->uri->rsegment(3));
            redirect("/curso/admin/", "refresh");
	}
	$data["curso"]=$this->curso_model->getOne($data["id"]);
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso/edit_curso",$data);
    $this->load->view("templates/footer");
   }
   public function delete()
   {
	$data["id"]=$this->uri->segment(3);
	$this->load->model("curso_model");
	/**/$this->config->load("cms");
    /**/$data["item"]=$this->config->item("admin_nav");
    if((isset($data["id"]) and $data["id"] != "") and !empty($_POST)){
        if($_POST["confirmar"]==="TRUE"){
	$this->curso_model->delete($data["id"]);
        redirect("/curso/admin/TRUE/", "refresh");
	}
	else
	{
	redirect("/curso/admin/FALSE/", "refresh");
	}
      }
	
    $this->load->view("templates/header");
	/**/$this->load->view("templates/adminmenu",$data);
	$this->load->view("admin/curso/delete_curso",$data);
    $this->load->view("templates/footer");
   }
	public function server()
	{
	if(isset($_POST) and !empty($_POST))
	{
	$this->load->library("form_validation");
	$this->form_validation->set_rules("curso_sede","curso_sede","trim|required|xss_clean");
					$this->form_validation->set_rules("turno","turno","trim|required|xss_clean");
					$this->form_validation->set_rules("fecha_inicio","fecha_inicio","trim|required|xss_clean|exact_length[10]");
					$this->form_validation->set_rules("modalidad","modalidad","trim|required|xss_clean");
	//				$this->form_validation->set_rules("fecha_inscripcion","fecha_inscripcion","trim|required|xss_clean|exact_length[10]");
	//				$this->form_validation->set_rules("usuario_id_usuario","usuario_id_usuario","trim|required|xss_clean|integer");
    //$data["usuario"]=$this->curso_model->get_usuario_folio_capacitacion();
    //$this->form_validation->set_rules("robotoit_user_id_user","robotoit_user_id_user","trim|required|xss_clean|integer");
    //$data["robotoit_user"]=$this->curso_model->get_robotoit_user_user_name();
    
	return $this->form_validation->run();
	}
        else
        {
        return FALSE;
        }
	}
	public function client()
	{
	$this->load->library("validate");
	$this->validate->setId("curso");
	$this->validate->clientInputEmpty("curso_sede");
				$this->validate->clientInputEmpty("turno");
	//			$this->validate->clientDateFormat("fecha_inicio");
				$this->validate->clientInputEmpty("modalidad");
	//			$this->validate->clientDateFormat("fecha_inscripcion");
				
	return $this->validate->getJscript();
	}
}
