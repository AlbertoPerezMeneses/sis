    <?php

// created 12-03-15 02:21:45

if (!defined("BASEPATH"))
    die();

class Usuario extends CI_Controller {
    /*

      public function index() {

      $this->load->view("templates/header");

      $this->load->view("templates/aside");

      $this->load->view("templates/footer");

      }

     */

    public function view() {



        $this->load->model("usuario_model");

        $this->config->load("cms");

        /**/$data["item"] = $this->config->item("user_nav");

        $data["usuario"] = $this->usuario_model->get();

        //$data["keys"]=array_keys($data["usuario"][0]);

        $this->load->view("templates/header");

        $this->load->view("templates/menubar", $data);

        $this->load->view("users/usuario/show_usuario", $data);

        $this->load->view("templates/footer");
    }

    public function finalizar_proceso() {
        $posicion = $this->uri->segment(3);
        $multiplo = $this->uri->segment(4);
        $this->load->model("usuario_model");
        /**/$this->config->load("cms");
        /**/$data["item"] = $this->config->item("admin_nav");
        $usuarios = $this->usuario_model->get_matriculados();
        $data["usuario"] = $usuarios;
        //$data["dump"]=$posicion*$multiplo;
        $this->load->view("templates/header");
        //$this->load->view("templates/vardump",$data);
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario/finalizar_proceso", $data);
        $this->load->view("templates/footer");
    }

    public function inscritos() {
        /**/$this->config->load("cms");
        $this->load->model("usuario_model");
        /**/$data["item"] = $this->config->item("admin_nav");
        $data["usuario"] = $this->usuario_model->get_inscritos();


        $this->load->view("templates/header");
        /**/$this->load->view("templates/adminmenu", $data);
        $this->load->view("admin/usuario/usuarios_matriculados", $data);
        $this->load->view("templates/footer");
    }

    public function admin() {

        $posicion = $this->uri->segment(3);

        $multiplo = $this->uri->segment(4);

        $this->load->model("usuario_model");

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");

        $usuarios = $this->usuario_model->get();

        $data["usuario"] = $usuarios;

        //$data["dump"]=$posicion*$multiplo;

        $this->load->view("templates/header");

        //$this->load->view("templates/vardump",$data);

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/usuario/buscar_usuario", $data);

        $this->load->view("templates/footer");
    }

    public function find() {



        $this->load->model("usuario_model");

        $this->config->load("cms");

        /**/$data["item"] = $this->config->item("user_nav");

        $data["id"] = $this->uri->segment(3);

        $data["usuario"] = $this->usuario_model->getOne($data["id"]);

        $this->load->view("templates/header");

        $this->load->view("templates/menubar", $data);

        $this->load->view("users/usuario/find_usuario", $data);

        $this->load->view("templates/footer");
    }

    public function create() {

        $this->load->model("usuario_model");



        $data["robotoit_user"] = $this->usuario_model->get_robotoit_user_user_name();



        $data["client"] = $this->client();

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");



        if ($this->server() AND ( isset($_POST) AND ! empty($_POST))) {

            $this->usuario_model->add();

            redirect("/usuario/admin/", "refresh");
        }

        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/usuario/add_usuario", $data);

        $this->load->view("templates/footer");
    }

    public function update() {

        $this->load->model("usuario_model");



        //$data["robotoit_user"] = $this->usuario_model->get_robotoit_user_user_name();
        $data["catalogo_cursos"] = $this->usuario_model->get_catalogo_cursos();


        $data["client"] = $this->client();

        $data["id"] = $this->uri->segment(3);

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");

        if ($this->server() AND ( isset($data["id"]) and $data["id"] != "") and ( isset($_POST) and ! empty($_POST))) {

            $this->usuario_model->update($this->uri->rsegment(3));

            redirect("/usuario/admin/", "refresh");
        }

        $data["usuario"] = $this->usuario_model->getOne($data["id"]);
        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/usuario/edit_usuario", $data);

        $this->load->view("templates/footer");
    }

    public function delete() {

        $data["id"] = $this->uri->segment(3);

        $this->load->model("usuario_model");

        /**/$this->config->load("cms");

        /**/$data["item"] = $this->config->item("admin_nav");

        if ((isset($data["id"]) and $data["id"] != "") and ! empty($_POST)) {

            if ($_POST["confirmar"] === "TRUE") {

                $this->usuario_model->delete($data["id"]);

                redirect("/usuario/admin/TRUE/", "refresh");
            } else {

                redirect("/usuario/admin/FALSE/", "refresh");
            }
        }



        $this->load->view("templates/header");

        /**/$this->load->view("templates/adminmenu", $data);

        $this->load->view("admin/usuario/delete_usuario", $data);

        $this->load->view("templates/footer");
    }

    public function server() {

        if (isset($_POST) and ! empty($_POST)) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("folio_capacitacion", "folio_capacitacion", "trim|required|xss_clean");

            $this->form_validation->set_rules("nombre_del_trabajador", "nombre_del_trabajador", "trim|required|xss_clean");

            $this->form_validation->set_rules("rfc", "rfc", "trim|required|xss_clean");

            $this->form_validation->set_rules("correo", "correo", "trim|required|xss_clean|valid_email");

            //$this->form_validation->set_rules("robotoit_user_id_user", "robotoit_user_id_user", "trim|required|xss_clean|integer");
            //$data["robotoit_user"]=$this->usuario_model->get_robotoit_user_user_name();



            return $this->form_validation->run();
        } else {

            return FALSE;
        }
    }

    public function client() {

        $this->load->library("validate");

        $this->validate->setId("usuario");

        $this->validate->clientInputEmpty("folio_capacitacion");

        $this->validate->clientInputEmpty("nombre_del_trabajador");

        $this->validate->clientInputEmpty("rfc");

        // $this->validate->clientMail("correo");



        return $this->validate->getJscript();
    }

    /*

     *

     *   */

    public function index() {



        $data["server"] = $this->session_is_started();

        $data["client"] = $this->client();



        $this->load->model("usuario_model");



        if (isset($_POST["username"]) and isset($_POST["password"]) and is_array($_POST)) {

            if ($this->usuario_model->auth()) {

                $data["success"] = TRUE;



                $_SESSION["username"] = $_POST["username"];

                $_SESSION["password"] = $_POST["password"];

                //$_SESSION["status"]="algo";



                redirect("usuario/registro");
            } else {

                $data["error"] = TRUE;
            }
        }

        $this->load->view("templates/header");



        $this->load->view("admin/usuario/login", $data);

        $this->load->view("templates/footer");
    }

    public function close() {

        session_start();

        session_unset();

        session_destroy();

        redirect("/", "refresh");
    }

    private function session_is_started() {

        session_start();



        if (isset($_SESSION["username"])) {

            /* Aqui se define la ruta que va a tomar dependiendo del valor de sua */

            redirect("usuario/registro");
        }
    }

//    public function panel(){
//         //session_start();
//        $this->load->view("templates/header");
//        //$this->load->view("templates/vardump", $data);
//

//        $this->load->view("admin/panel/user");
//

//        $this->load->view("templates/footer");
//    }

    public function registro() {

        /*

          busca por el folio de capacitacion y posteriormente entrega el id del usuario para hacer una busqueda de los examenes asignados.

         *  */

        session_start();

        $data = array();

        if (isset($_SESSION["username"]) and $_SESSION["username"] != "") {



            $this->load->model("usuario_has_catalogo_cursos_model");

            $this->load->model("usuario_model");

            $this->load->model("catalogo_cursos_model");

            $usuario = $this->usuario_model->get_by_folio_capacitacion($_SESSION["username"]);

            $id_usuario = $usuario[0]["id_usuario"];

            if ($usuario[0]["curso_matriculado"] != null and ! empty($usuario[0]["curso_matriculado"])) {

                $curso_inscrito = $this->catalogo_cursos_model->get_One($usuario[0]["curso_matriculado"]);

                $data["curso_inscrito"] = $curso_inscrito[0]["nombre_catalogo"];

                //$data["dump"]=$curso_inscrito;
            }

            $cursos = $this->usuario_has_catalogo_cursos_model->getByFolio($id_usuario);

            //$data["dump"] = $id_usuario;

            $data["usuario"] = $usuario;

            $data["cursos"] = $cursos;

            //$data["dump"] = $this->usuario_model->get_by_folio_capacitacion()

            /*



             *  ojo no estamos guardando el id de usuario en la sesion 

             *              */
        }



        $this->load->view("templates/header");

        //$this->load->view("templates/vardump", $data);



        $this->load->view("admin/usuario/muestra_catalogo_de_examenes", $data);



        $this->load->view("templates/footer");
    }

    /*     * */
    
    function app(){
        
          session_start();
        $data = array();
        if (isset($_SESSION["username"]) and $_SESSION["username"] != "") {            
            $this->load->model("usuario_model");
            $data["usuario"] = $this->usuario_model->find_by_folio_capacitacion($_SESSION["username"]);
            $data["debug"]=$this->usuario_model->debugfind_by_folio_capacitacion($_SESSION["username"]);
            //$id_usuario = $usuario[0]["id_usuario"];
        
        $this->load->view("templates/header");
        //$this->load->view("templates/vardump", $data);

        $this->load->view("admin/usuario/suport_roboto", $data);

        $this->load->view("templates/footer");
        }
    }
}
