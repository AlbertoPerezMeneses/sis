<?php

class Extraecsv1 {

    private $archivo;
    private $archivoArray;
    private $filaArray;

    public function __construct() {
        $this->archivoArray = array();
        $this->filaArray = array();
    }

    public function set_route($ruta = "") {
        $this->archivo = fopen($ruta, "r");
    }

    public function extrae() {
        return "algodon";
    }

    public function get_CSV() {
        if ($this->archivo !== FALSE) {
            $respaldo = array();
            $fila = 0;
            while (($this->archivoArray = fgetcsv($this->archivo, 1000, ",")) !== FALSE) {
                foreach ($this->archivoArray as $key => $dato) {
                    switch ($key) {
                        case 1: $respaldo[$fila]["hostname"] = $dato;
                            break;
                        case 2: $respaldo[$fila]["ip"] = $dato;
                            break;
                        case 3: $respaldo[$fila]["dia_inicio"] = $dato;
                            break;
                        case 4: $respaldo[$fila]["hora_inicio"] = $dato;
                            break;
                        case 5: $respaldo[$fila]["nombre_log"] = $dato;
                            break;
                        case 6: $respaldo[$fila]["tipo_respaldo1"] = $dato;
                            break;
                        case 7: $respaldo[$fila]["tipo_respaldo2"] = $dato;
                            break;
                        case 8: $respaldo[$fila]["tipo_respaldo3"] = $dato;
                            break;
                        case 9: $respaldo[$fila]["drives"] = $dato;
                            break;
                        case 12: $respaldo[$fila]["tamano_respaldo"] = $dato;
                            break;
                        case 13: $respaldo[$fila]["dia_fin"] = $dato;
                            break;
                        case 14: $respaldo[$fila]["hora_fin"] = $dato;
                            break;
                        case 15: $respaldo[$fila]["status"] = $dato;


                            $fila++;

                            break;
                        default:
                            break;
                    }
                    //echo $key." ".$dato."<br />" ;
                }
            }
            return $respaldo;
            fclose($this->archivo);
        } else {
            //return "falso";
        }
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

