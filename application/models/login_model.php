<?php

if (!defined('BASEPATH'))
    die();

class Login_model extends CI_Model {

    private $d;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->d = null;
    }

    public function login() {
        if (isset($_POST) and !empty($_POST)) {
            if ($this->auth()) {
                $data["error"] = FALSE;
                $data["success"] = TRUE;
                session_start();
                $_SESSION["id_user"]=md5($this->d[0]["id_user"]);
                $_SESSION["password"] = $this->d[0]["user_password"];
                $_SESSION["name"] = $this->d[0]["user_name"];
                $_SESSION["status"] = $this->d[0]["status"];
               redirect("/panel/", "refresh");
            } else {
                $data["error"] = TRUE;
                $data["success"] = FALSE;
            }
        } else {
            $data["error"] = FALSE;
            $data["success"] = FALSE;
        }
        return $data;
    }

    public function auth() {
        $SQL = 'SELECT robotoit_user.id_user,robotoit_user.user_password,robotoit_user.user_name,robotoit_user.status FROM robotoit_user 
		WHERE  robotoit_user.user_name="' . $_POST["username"] . '" AND robotoit_user.user_password ="' . md5($_POST["password"]) . '" LIMIT 1';

        $query = $this->db->query($SQL);
        $this->d = $query->result_array();

        if (!empty($this->d) and ($this->d[0]["user_password"] == md5($_POST["password"]) and $this->d[0]["user_name"] == $_POST["username"])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}