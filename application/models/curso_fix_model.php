<?php

if (!defined("BASEPATH"))
    die();

class Curso_fix_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function get() {
        $this->db->select("curso_fix.id_curso,curso_fix.curso_sede,curso_fix.turno,curso_fix.fecha_inicio,curso_fix.modalidad,curso_fix.usuario_id_usuario,curso_fix.robotoit_user_id_user,curso_fix.fecha_inscripcion");
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }
    public function getOne($id) {
        $this->db->select("curso_fix.id_curso,curso_fix.curso_sede,curso_fix.turno,curso_fix.fecha_inicio,curso_fix.modalidad,curso_fix.usuario_id_usuario,curso_fix.robotoit_user_id_user,curso_fix.fecha_inscripcion");
        $this->db->where("md5(curso_fix.id_curso)", $id);
        $this->db->limit(1);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }
    
    function cuentaCurso($curso,$turno,$modalidad,$sede,$inicio) {
         $this->db->select("count(*) as cuenta");
         $this->db->join("curso_fix","usuario.id_usuario= curso_fix.usuario_id_usuario","INNER");
        $this->db->where("curso_fix.modalidad", $modalidad);
        $this->db->where("curso_fix.turno", $turno);
        $this->db->where("curso_fix.curso_sede", $sede);
        $this->db->where("usuario.curso_matriculado", $curso);
        $this->db->where("curso_fix.fecha_inicio", $inicio);
        $query = $this->db->get("usuario");
        return $query->row_array();
    }
    
    function add() {        
        $data = $_POST;
        $query = $this->db->insert("curso_fix", $data);
    }
    
    function add_by_data($data=null) {        
        $query = $this->db->insert("curso_fix", $data);
    }

    function update($id) {
        $data = $_POST;
        $this->db->where("md5(id_curso)", $id);
        $query = $this->db->update("curso_fix", $data);
    }

    public function delete($id) {
        $this->db->where("md5(id_curso)", $id);
        $query = $this->db->delete("curso_fix");
    }

    public function get_by_curso_sede($curso_sede = NULL) {
        $this->db->like("curso_sede", $curso_sede);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }
    public function get_by_turno($turno = NULL) {
        $this->db->like("turno", $turno);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

    public function get_by_fecha_inicio($fecha_inicio = NULL) {
        $this->db->like("fecha_inicio", $fecha_inicio);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

    public function get_by_modalidad($modalidad = NULL) {
        $this->db->like("modalidad", $modalidad);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

    public function get_by_usuario_id_usuario($usuario_id_usuario = NULL) {
        $this->db->like("usuario_id_usuario", $usuario_id_usuario);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

    public function get_by_robotoit_user_id_user($robotoit_user_id_user = NULL) {
        $this->db->like("robotoit_user_id_user", $robotoit_user_id_user);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

    public function get_by_fecha_inscripcion($fecha_inscripcion = NULL) {
        $this->db->like("fecha_inscripcion", $fecha_inscripcion);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

    public function get_by_curso_fix_fields($curso_fix = NULL) {
        $this->db->or_like("curso_sede", $curso_fix);
        $this->db->or_like("turno", $curso_fix);
        $this->db->or_like("fecha_inicio", $curso_fix);
        $this->db->or_like("modalidad", $curso_fix);
        $this->db->or_like("usuario_id_usuario", $curso_fix);
        $this->db->or_like("robotoit_user_id_user", $curso_fix);
        $this->db->or_like("fecha_inscripcion", $curso_fix);
        $query = $this->db->get("curso_fix");
        return $query->result_array();
    }

}
