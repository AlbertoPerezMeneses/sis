<?php

if (!defined("BASEPATH"))

    die();



class Usuario_model extends CI_Model {



    public function __construct() {

        parent::__construct();

        $this->load->database();

    }

public function get_matriculados() {
        $this->db->select("catalogo_cursos.nombre_catalogo,usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,usuario.turno,usuario.curso_matriculado");
        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario.curso_matriculado");
        $this->db->where("usuario.status_matriculacion",2);
        $query = $this->db->get("usuario");
        return $query->result_array();
    }

   public function get_inscritos() {
        $this->db->select("catalogo_cursos.nombre_catalogo,usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,usuario.turno,curso_fix.curso_sede,curso_fix.modalidad,curso_fix.turno as contraturno");
        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario.curso_matriculado");
        $this->db->join("curso_fix", "curso_fix.usuario_id_usuario=usuario.id_usuario");
        $this->db->where("usuario.status_matriculacion",3);
      $this->db->group_by("usuario.folio_capacitacion","DESC");
        $query = $this->db->get("usuario");
        return $query->result_array();
    }

    public function get() {

        $this->db->select("usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,usuario.turno");
  	$this->db->limit(1000);
        $query = $this->db->get("usuario");

        return $query->result_array();

    }


    function get_course_result($id=null) {
        $query=$this->db->query('SELECT * FROM examen  JOIN usuario on usuario.id_usuario=examen.usuario_id_usuario LEFT JOIN usuario_has_catalogo_cursos ON  usuario_has_catalogo_cursos.usuario_id_usuario=examen.usuario_id_usuario where examen.usuario_id_usuario='.$id.' group by calificacion order by id_examen;');
         return $query->result_array();
    }

    public function get_range($multiplo, $inicio) {

        $this->db->select("usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,usuario.turno");

        $query = $this->db->get("usuario",$multiplo,$inicio);

        return $query->result_array();

    }

    public function getOne($id) {

        $this->db->select("usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,usuario.turno,usuario.curso_matriculado");

        $this->db->where("md5(usuario.id_usuario)", $id);

        $this->db->limit(1);

        $query = $this->db->get("usuario");

        return $query->result_array();

    }


    public function get_status_matriculacion_by_folio($folio=NULL) {
        $this->db->select("usuario.status_matriculacion");
        $this->db->where("usuario.folio_capacitacion", $folio);
        $query = $this->db->get("usuario");
        return $query->row_array();
    }



    function add() {

        /* we change this if you don´t need work with webservice please uncoment this lines */

        //if(isset($_POST) and !empty($_POST))

        //	{

        $data = $_POST;

        $query = $this->db->insert("usuario", $data);

        //redirect("/usuario/admin/", "refresh");

        //	}

    }



    function update($id) {

        //if((isset($id) and $id>0) and (isset($_POST) and !empty($_POST)))

        //{

        $data = $_POST;

        $this->db->where("md5(id_usuario)", $id);

        $query = $this->db->update("usuario", $data);

        //redirect("/usuario/admin/", "refresh");

        //}

    }

    function termina_proceso($id){
        $data["status_matriculacion"]=3;
        $this->db->where("id_usuario", $id);
        $query = $this->db->update("usuario", $data);
    }
    
    function rescinde_proceso($id){
        $data["status_matriculacion"]=4;
        $this->db->where("id_usuario", $id);
        $query = $this->db->update("usuario", $data);
    }
    

    function reiniciar_usuario($id = null){
        
        $this->db->trans_start();
        $this->db->where("usuario_id_usuario",$id);
        $this->db->delete("usuario_has_catalogo_cursos");
        
        $this->db->flush_cache();
        
        $data = array("status_matriculacion"=>"0","curso_matriculado"=>"0");
        $this->db->where("id_usuario",$id);        
        $this->db->update("usuario",$data);
        
        $this->db->trans_start();
        return $this->db->trans_status();
    }

    public function delete($id) {

        $this->db->where("md5(id_usuario)", $id);
        $query = $this->db->delete("usuario");

    }



    public function get_robotoit_user_user_name() {

        $this->db->select("user_name,id_user");

        $query = $this->db->get("robotoit_user");

        return $query->result_array();

    }



      public function get_catalogo_cursos() {
        $this->db->select("catalogo_cursos.id_catalogo_cursos,catalogo_cursos.nombre_catalogo");
        $query = $this->db->get("catalogo_cursos");
        return $query->result_array();
    }
  

    public function get_by_folio_capacitacion($folio_capacitacion = NULL) {
        $this->db->select("md5(usuario.id_usuario) as id_encript,usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,usuario.curso_matriculado,usuario.turno");
        $this->db->like("folio_capacitacion", $folio_capacitacion);
        $this->db->limit(100);
        $query = $this->db->get("usuario");
        return $query->result_array();
    }




    public function find_by_folio_capacitacion($folio_capacitacion = NULL) {
	  $this->db->select("md5(usuario.id_usuario) as id_encript,usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion");
	// $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario.curso_matriculado");
       // $this->db->select("md5(usuario.id_usuario) as id_encript,usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,catalogo_cursos.nombre_catalogo as curso_inscrito");
	// $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario.curso_matriculado");

        $this->db->where("folio_capacitacion", $folio_capacitacion);
        $query = $this->db->get("usuario");
        return $query->result_array();
    }


 public function get_resultado($folio_capacitacion = NULL) {

	   $this->db->select("md5(usuario.id_usuario) as id_encript,usuario.id_usuario,usuario.folio_capacitacion,usuario.nombre_del_trabajador,usuario.rfc,usuario.correo,usuario.status_matriculacion,catalogo_cursos.nombre_catalogo as curso_inscrito");
	 $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario.curso_matriculado");

        $this->db->where("folio_capacitacion", $folio_capacitacion);
        $query = $this->db->get("usuario");
        return $query->result_array();
    }

   

    public function get_by_rfc($rfc = NULL) {

        $this->db->like("rfc", $rfc);

        $query = $this->db->get("usuario");

        return $query->result_array();

    }



    public function get_by_correo($correo = NULL) {

        $this->db->like("correo", $correo);

        $query = $this->db->get("usuario");

        return $query->result_array();

    }



    public function get_by_usuario_fields($usuario = NULL) {

        $this->db->or_like("folio_capacitacion", $usuario);



        $this->db->or_like("nombre_del_trabajador", $usuario);



        $this->db->or_like("rfc", $usuario);



        $this->db->or_like("correo", $usuario);

        $query = $this->db->get("usuario");

        return $query->result_array();

    }



    public function auth() {

     

        $this->d = $this->get_autentication($_POST["password"], $_POST["username"]);



        if (!empty($this->d) and ($this->d[0]["rfc"] == $_POST["password"] and $this->d[0]["folio_capacitacion"] == $_POST["username"])) {

            return TRUE;

        } else {

            return FALSE;

        }

       

    }

    

     

    public function get_autentication($rfc = NULL,$folio_capacitacion = NULL) {

        $this->db->where("folio_capacitacion", $folio_capacitacion);

        $this->db->where("rfc", $rfc);

        $query = $this->db->get("usuario");

        return $query->result_array();

    }


    

}

