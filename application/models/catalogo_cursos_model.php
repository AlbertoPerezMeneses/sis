<?php if (!defined("BASEPATH")) die();

class Catalogo_cursos_model extends CI_Model {



   public function __construct()

	{

	  parent::__construct();

      $this->load->database();

	}

   public function get()

   {$this->db->select("catalogo_cursos.id_catalogo_cursos,catalogo_cursos.nombre_catalogo");

	 $query = $this->db->get("catalogo_cursos");
	
	 return $query->result_array();

   }

   public function getOne($id)

   {

   $this->db->select("catalogo_cursos.id_catalogo_cursos,catalogo_cursos.nombre_catalogo");

	 $this->db->where("md5(catalogo_cursos.id_catalogo_cursos)",$id);

	 $this->db->limit(1);

	 $query = $this->db->get("catalogo_cursos");

	 return $query->result_array();

   }

   //sin md5

   public function get_One($id)

   {

   $this->db->select("catalogo_cursos.id_catalogo_cursos,catalogo_cursos.nombre_catalogo");

	 $this->db->where("catalogo_cursos.id_catalogo_cursos",$id);

	 $this->db->limit(1);

	 $query = $this->db->get("catalogo_cursos");

	 return $query->result_array();

   }

   function add()

   {

   /*we change this if you don´t need work with webservice please uncoment this lines*/

	//if(isset($_POST) and !empty($_POST))

	//	{

			$data = $_POST;

			$query =$this->db->insert("catalogo_cursos",$data);

			//redirect("/catalogo_cursos/admin/", "refresh");

	//	}

   }

   function update($id)

   {

   //if((isset($id) and $id>0) and (isset($_POST) and !empty($_POST)))

    //{

	$data =$_POST;

	$this->db->where( "md5(id_catalogo_cursos)",$id);

	$query =$this->db->update("catalogo_cursos",$data);

	//redirect("/catalogo_cursos/admin/", "refresh");

	//}

   }

   

   public function delete($id)

   {

   //if(isset($id) and $id>0 and !empty($_POST))

    //{

	//if($_POST["confirmar"]==="TRUE"){

	$this->db->where("md5(id_catalogo_cursos)",$id);

	$query =$this->db->delete("catalogo_cursos");

	//redirect("/catalogo_cursos/admin/TRUE/", "refresh");

	//}

	//else

	//{

	//redirect("/catalogo_cursos/admin/FALSE/", "refresh");

	//}

	//}

   }

   

     public function get_by_nombre_catalogo($nombre_catalogo = NULL)

     {

         $this->db->like("nombre_catalogo",$nombre_catalogo);

         $query = $this->db->get("catalogo_cursos");

         return $query->result_array();

     }

     public function get_by_catalogo_cursos_fields($catalogo_cursos = NULL)

     {     

         $this->db->or_like("nombre_catalogo",$catalogo_cursos);

         $query = $this->db->get("catalogo_cursos");

         return $query->result_array();

     }}

