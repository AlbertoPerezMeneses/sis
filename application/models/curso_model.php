<?php if (!defined("BASEPATH")) die();
class Curso_model extends CI_Model {

   public function __construct()
	{
	  parent::__construct();
      $this->load->database();
	}
   public function get()
   {$this->db->select("curso.id_curso,curso.curso_sede,curso.turno,curso.fecha_inicio,curso.modalidad,curso.fecha_inscripcion,usuario.folio_capacitacion,robotoit_user.user_name");
                $this->db->join("usuario","usuario.id_usuario=curso.usuario_id_usuario","INNER");
                $this->db->join("robotoit_user","robotoit_user.id_user=curso.robotoit_user_id_user","INNER");
	 $query = $this->db->get("curso");
	 return $query->result_array();
   }
   public function getOne($id)
   {
   $this->db->select("curso.id_curso,curso.curso_sede,curso.turno,curso.fecha_inicio,curso.modalidad,curso.fecha_inscripcion,usuario.folio_capacitacion,robotoit_user.user_name");
                $this->db->join("usuario","usuario.id_usuario=curso.usuario_id_usuario","INNER");
                $this->db->join("robotoit_user","robotoit_user.id_user=curso.robotoit_user_id_user","INNER");
	 $this->db->where("md5(curso.id_curso)",$id);
	 $this->db->limit(1);
	 $query = $this->db->get("curso");
	 return $query->result_array();
   }
   function add()
   {
   /*we change this if you don´t need work with webservice please uncoment this lines*/
	//if(isset($_POST) and !empty($_POST))
	//	{
			$data = $_POST;
			$query =$this->db->insert("curso",$data);
			//redirect("/curso/admin/", "refresh");
	//	}
   }
   function update($id)
   {
   //if((isset($id) and $id>0) and (isset($_POST) and !empty($_POST)))
    //{
	$data =$_POST;
	$this->db->where( "md5(id_curso)",$id);
	$query =$this->db->update("curso",$data);
	//redirect("/curso/admin/", "refresh");
	//}
   }
   
   public function delete($id)
   {
   //if(isset($id) and $id>0 and !empty($_POST))
    //{
	//if($_POST["confirmar"]==="TRUE"){
	$this->db->where("md5(id_curso)",$id);
	$query =$this->db->delete("curso");
	//redirect("/curso/admin/TRUE/", "refresh");
	//}
	//else
	//{
	//redirect("/curso/admin/FALSE/", "refresh");
	//}
	//}
   }
   
        public function get_usuario_folio_capacitacion()
       {
             $this->db->select("folio_capacitacion,id_usuario");
             $query = $this->db->get("usuario");
                  return $query->result_array();
       }
        public function get_robotoit_user_user_name()
       {
             $this->db->select("user_name,id_user");
             $query = $this->db->get("robotoit_user");
                  return $query->result_array();
       }
     public function get_by_curso_sede($curso_sede = NULL)
     {
         $this->db->like("curso_sede",$curso_sede);
         $query = $this->db->get("curso");
         return $query->result_array();
     }
     public function get_by_turno($turno = NULL)
     {
         $this->db->like("turno",$turno);
         $query = $this->db->get("curso");
         return $query->result_array();
     }
     public function get_by_fecha_inicio($fecha_inicio = NULL)
     {
         $this->db->like("fecha_inicio",$fecha_inicio);
         $query = $this->db->get("curso");
         return $query->result_array();
     }
     public function get_by_modalidad($modalidad = NULL)
     {
         $this->db->like("modalidad",$modalidad);
         $query = $this->db->get("curso");
         return $query->result_array();
     }
     public function get_by_fecha_inscripcion($fecha_inscripcion = NULL)
     {
         $this->db->like("fecha_inscripcion",$fecha_inscripcion);
         $query = $this->db->get("curso");
         return $query->result_array();
     }
     public function get_by_curso_fields($curso = NULL)
     {     
         $this->db->or_like("curso_sede",$curso);
              
         $this->db->or_like("turno",$curso);
              
         $this->db->or_like("fecha_inicio",$curso);
              
         $this->db->or_like("modalidad",$curso);
              
         $this->db->or_like("fecha_inscripcion",$curso);
         $query = $this->db->get("curso");
         return $query->result_array();
     }}
