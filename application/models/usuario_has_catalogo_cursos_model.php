<?php



if (!defined("BASEPATH"))

    die();



class Usuario_has_catalogo_cursos_model extends CI_Model {



    public function __construct() {

        parent::__construct();

        $this->load->database();

    }



    public function get() {

        $this->db->select("usuario_has_catalogo_cursos.id_usuario_has_catalogo_cursos,usuario_has_catalogo_cursos.intentos,usuario.nombre_del_trabajador,catalogo_cursos.nombre_catalogo");

        $this->db->join("usuario", "usuario.id_usuario=usuario_has_catalogo_cursos.usuario_id_usuario", "INNER");

        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario_has_catalogo_cursos.catalogo_cursos_id_catalogo_cursos", "INNER");

        $this->db->order_by("catalogo_cursos.nombre_catalogo","ASC");
	
        $query = $this->db->get("usuario_has_catalogo_cursos");

	
        return $query->result_array();

    }

    

    public function getByFolio($folio) {

//        SELECT * FROM `usuario_has_catalogo_cursos` 

//	inner join usuario on usuario.id_usuario = usuario_has_catalogo_cursos.usuario_id_usuario

//    	where usuario_id_usuario = 1

        $this->db->select("catalogo_cursos.id_catalogo_cursos,usuario_has_catalogo_cursos.id_usuario_has_catalogo_cursos,usuario_has_catalogo_cursos.intentos,usuario.nombre_del_trabajador,catalogo_cursos.nombre_catalogo");

        $this->db->join("usuario", "usuario.id_usuario=usuario_has_catalogo_cursos.usuario_id_usuario");

        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario_has_catalogo_cursos.catalogo_cursos_id_catalogo_cursos");

        $this->db->where("usuario_has_catalogo_cursos.usuario_id_usuario", $folio);

        $this->db->where('usuario_has_catalogo_cursos.intentos', "0");

        $query = $this->db->get("usuario_has_catalogo_cursos");

        return $query->result_array();

    }

     public function get_intentosByFolio($folio) {

        $this->db->select("catalogo_cursos.id_catalogo_cursos,usuario_has_catalogo_cursos.id_usuario_has_catalogo_cursos,usuario_has_catalogo_cursos.intentos,usuario.nombre_del_trabajador,catalogo_cursos.nombre_catalogo");

        $this->db->join("usuario", "usuario.id_usuario=usuario_has_catalogo_cursos.usuario_id_usuario");

        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario_has_catalogo_cursos.catalogo_cursos_id_catalogo_cursos");

        $this->db->where("usuario_has_catalogo_cursos.usuario_id_usuario", $folio);

        $this->db->where('usuario_has_catalogo_cursos.intentos', "1");

        $query = $this->db->get("usuario_has_catalogo_cursos");

        return $query->result_array();

    }


    

    

    public function getCountByFolio($folio=NULL) {

        //$this->db->select("count(usuario_id_usuario) as num_count");

        $this->db->join("usuario", "usuario.id_usuario=usuario_has_catalogo_cursos.usuario_id_usuario", "INNER");

        $this->db->where("usuario.folio_capacitacion", $folio);

        

        $query = $this->db->get("usuario_has_catalogo_cursos");

       // return $query->row_array();
	 return $query->num_rows();

    }

    

    private function getIdUserByFolio($folio=NULL) {

        $this->db->select("usuario.id_usuario");

        $this->db->where("usuario.folio_capacitacion", $folio);

        $query = $this->db->get("usuario");

        return $query->row_array();

    }

    

    public function getOne($id) {

        $this->db->select("usuario_has_catalogo_cursos.id_usuario_has_catalogo_cursos,usuario_has_catalogo_cursos.intentos,usuario.nombre_del_trabajador,catalogo_cursos.nombre_catalogo");

        $this->db->join("usuario", "usuario.id_usuario=usuario_has_catalogo_cursos.usuario_id_usuario", "INNER");

        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=usuario_has_catalogo_cursos.catalogo_cursos_id_catalogo_cursos", "INNER");

        $this->db->where("md5(usuario_has_catalogo_cursos.id_usuario_has_catalogo_cursos)", $id);

        $this->db->limit(1);

        $query = $this->db->get("usuario_has_catalogo_cursos");

        return $query->result_array();

    }



    function add() {

        /* we change this if you don´t need work with webservice please uncoment this lines */

        //if(isset($_POST) and !empty($_POST))

        //	{

        $data = $_POST;

        $query = $this->db->insert("usuario_has_catalogo_cursos", $data);

        //redirect("/usuario_has_catalogo_cursos/admin/", "refresh");

        //	}

    }

    /*

añade varios registros en la bd

     *      */

     function multiple_add($folio=null, $oferta=null) {

        $info=array();

         $id=$this->getIdUserByFolio($folio);

        if($oferta!=null and $id!=null){

            foreach ($oferta as $item) {

                $data["intentos"]=0;

                 $data["catalogo_cursos_id_catalogo_cursos"] = $item;

                 

                 $data["usuario_id_usuario"]=$id["id_usuario"];

        $query = $this->db->insert("usuario_has_catalogo_cursos", $data);

             //  $info[]=$data;

            }

            $this->autoriza_alumno($id["id_usuario"]);

        }

      // return $info;

    }



    function autoriza_alumno($id =NULL){

        $this->db->where("id_usuario", $id);

        $query = $this->db->update("usuario", array("status_matriculacion"=>1));

    }

  function matricula_alumno($id =NULL,$curso=NULL){
        $this->db->where("id_usuario", $id);
        $query = $this->db->update("usuario", array("status_matriculacion"=>2,"curso_matriculado"=>$curso));
    }

    function update($id) {

        //if((isset($id) and $id>0) and (isset($_POST) and !empty($_POST)))

        //{

        $data = $_POST;

        $this->db->where("md5(id_usuario_has_catalogo_cursos)", $id);

        $query = $this->db->update("usuario_has_catalogo_cursos", $data);

        //redirect("/usuario_has_catalogo_cursos/admin/", "refresh");

        //}

    }

    

     function updateByIdUsuarioCurso($id=NULL,$curso=NULL,$data=NULL) {

        if($id!=NULL and $data !=NULL and $curso!=NULL)

        {

        $this->db->where("usuario_id_usuario", $id);

        $this->db->where("catalogo_cursos_id_catalogo_cursos", $curso);

        $query = $this->db->update("usuario_has_catalogo_cursos", $data);

        

        }

    }



    public function delete($id) {

        //if(isset($id) and $id>0 and !empty($_POST))

        //{

        //if($_POST["confirmar"]==="TRUE"){

        $this->db->where("md5(id_usuario_has_catalogo_cursos)", $id);

        $query = $this->db->delete("usuario_has_catalogo_cursos");

        //redirect("/usuario_has_catalogo_cursos/admin/TRUE/", "refresh");

        //}

        //else

        //{

        //redirect("/usuario_has_catalogo_cursos/admin/FALSE/", "refresh");

        //}

        //}

    }



    public function get_usuario_nombre_del_trabajador() {

        $this->db->select("nombre_del_trabajador,id_usuario");

        $query = $this->db->get("usuario");

        return $query->result_array();

    }



    public function get_catalogo_cursos_nombre_catalogo() {

        $this->db->select("nombre_catalogo,id_catalogo_cursos");

        $query = $this->db->get("catalogo_cursos");

        return $query->result_array();

    }



    public function get_by_intentos($intentos = NULL) {

        $this->db->like("intentos", $intentos);

        $query = $this->db->get("usuario_has_catalogo_cursos");

        return $query->result_array();

    }



    public function get_by_usuario_has_catalogo_cursos_fields($usuario_has_catalogo_cursos = NULL) {

        $this->db->or_like("intentos", $usuario_has_catalogo_cursos);

        $query = $this->db->get("usuario_has_catalogo_cursos");

        return $query->result_array();

    }



}

