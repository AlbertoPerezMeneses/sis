<?php



if (!defined("BASEPATH"))
    die();

class Examen_envia_model extends CI_Model {

    public function __construct() {

        parent::__construct();

        $this->load->database();
    }

    public function get() {

        $this->db->select("examen_envia.id_examen_envia,examen_envia.aciertos,examen_envia.intentos,examen_envia.usuario,examen_envia.id_catalogo_cursos");

        $query = $this->db->get("examen_envia");

        return $query->result_array();
    }

    public function getOne($id) {

        $this->db->select("examen_envia.id_examen_envia,examen_envia.aciertos,examen_envia.intentos,examen_envia.usuario,examen_envia.id_catalogo_cursos");

        $this->db->where("md5(examen_envia.id_examen_envia)", $id);

        $this->db->limit(1);

        $query = $this->db->get("examen_envia");

        return $query->result_array();
    }

    public function get_by_folio($id = NULL) {
        $this->db->select("examen_envia.id_examen_envia,examen_envia.aciertos,examen_envia.intentos,examen_envia.usuario,examen_envia.id_catalogo_cursos");
        $this->db->like("examen_envia.usuario", $id);
        $this->db->limit(100);
        $query = $this->db->get("examen_envia");
        return $query->result_array();
    }

    public function find_by_folio($id = NULL) {
        $this->db->select("examen_envia.id_examen_envia,examen_envia.aciertos,examen_envia.intentos,examen_envia.usuario,examen_envia.id_catalogo_cursos,catalogo_cursos.nombre_catalogo");
        $this->db->join("catalogo_cursos", "examen_envia.id_catalogo_cursos = catalogo_cursos.id_catalogo_cursos", "INNER");
        $this->db->where("examen_envia.usuario", $id);
        $this->db->limit(100);
        $query = $this->db->get("examen_envia");
        return $query->result_array();
    }

    function add() {

        /* we change this if you don´t need work with webservice please uncoment this lines */

        //if(isset($_POST) and !empty($_POST))
        //	{

        $data = $_POST;

        $query = $this->db->insert("examen_envia", $data);

        //redirect("/examen_envia/admin/", "refresh");
        //	}
        return $this->db->last_query();
    }

    function update($id) {

        //if((isset($id) and $id>0) and (isset($_POST) and !empty($_POST)))
        //{

        $data = $_POST;

        $this->db->where("md5(id_examen_envia)", $id);

        $query = $this->db->update("examen_envia", $data);

        //redirect("/examen_envia/admin/", "refresh");
        //}
    }

    public function delete($id) {

        //if(isset($id) and $id>0 and !empty($_POST))
        //{
        //if($_POST["confirmar"]==="TRUE"){

        $this->db->where("md5(id_examen_envia)", $id);

        $query = $this->db->delete("examen_envia");

        //redirect("/examen_envia/admin/TRUE/", "refresh");
        //}
        //else
        //{
        //redirect("/examen_envia/admin/FALSE/", "refresh");
        //}
        //}
    }

    public function get_by_aciertos($aciertos = NULL) {

        $this->db->like("aciertos", $aciertos);

        $query = $this->db->get("examen_envia");

        return $query->result_array();
    }

    public function get_by_intentos($intentos = NULL) {

        $this->db->like("intentos", $intentos);

        $query = $this->db->get("examen_envia");

        return $query->result_array();
    }

    public function get_by_usuario($usuario = NULL) {

        $this->db->like("usuario", $usuario);

        $query = $this->db->get("examen_envia");

        return $query->result_array();
    }

    public function get_by_examen_envia_fields($examen_envia = NULL) {

        $this->db->or_like("aciertos", $examen_envia);



        $this->db->or_like("intentos", $examen_envia);



        $this->db->or_like("usuario", $examen_envia);

        $query = $this->db->get("examen_envia");

        return $query->result_array();
    }

}
