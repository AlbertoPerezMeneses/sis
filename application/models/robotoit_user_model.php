<?php



if (!defined("BASEPATH"))

    die();



class Robotoit_user_model extends CI_Model {



    public function __construct() {

        parent::__construct();

        $this->load->database();

    }



    public function get() {

        $this->db->select("robotoit_user.id_user,robotoit_user.user_name,robotoit_user.user_password,robotoit_user.imagen");

    

        $query = $this->db->get("robotoit_user");

        $data=$query->result_array();       

        

        return $query->result_array();

    }



    public function getOne($id) {

       $this->db->select("robotoit_user.id_user,robotoit_user.user_name,robotoit_user.user_password,robotoit_user.imagen");

        $this->db->where("md5(robotoit_user.id_user)", $id);

        $this->db->limit(1);

        $query = $this->db->get("robotoit_user");

        return $query->result_array();

    }



    function add() {

     

        $data = $_POST;

        $data["user_password"]=md5($data["user_password"]);

        $query = $this->db->insert("robotoit_user", $data);

     

    }



    function update($id) {

        $data = $_POST;

        if(isset($data["user_password"])){

        $data["user_password"]=md5($data["user_password"]);

        }

        $this->db->where("md5(id_user)", $id);

        $query = $this->db->update("robotoit_user", $data);

        return $this->db->last_query();

    }



    public function delete($id) {

        $this->db->where("md5(id_user)", $id);

        $query = $this->db->delete("robotoit_user");

     

    }



    public function get_by_user_name($user_name = NULL) {

        $this->db->like("user_name", $user_name);

        $query = $this->db->get("robotoit_user");

        return $query->result_array();

    }



    public function get_by_user_email($user_email = NULL) {

        $this->db->like("user_email", $user_email);

        $query = $this->db->get("robotoit_user");

        return $query->result_array();

    }



    public function get_by_user_password($user_password = NULL) {

        $this->db->like("user_password", $user_password);

        $query = $this->db->get("robotoit_user");

        return $query->result_array();

    }



    public function get_by_robotoit_user_fields($robotoit_user = NULL) {

        $this->db->or_like("user_name", $robotoit_user);



        $this->db->or_like("user_email", $robotoit_user);



        $this->db->or_like("user_password", $robotoit_user);

        $query = $this->db->get("robotoit_user");

        return $query->result_array();

    }

    

    

 /*

  * 

  *   

  */

}

