<?php

if (!defined("BASEPATH"))
    die();

class Examen_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get() {
        $this->db->select("examen.id_examen,examen.nombre_examen,examen.calificacion,usuario.nombre_del_trabajador,catalogo_cursos.nombre_catalogo");
        $this->db->join("usuario", "usuario.id_usuario=examen.usuario_id_usuario", "INNER");
        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=examen.catalogo_cursos_id_catalogo_cursos", "INNER");
        $query = $this->db->get("examen");
        return $query->result_array();
    }

    public function getOne($id) {
        $this->db->select("examen.id_examen,examen.nombre_examen,examen.calificacion,usuario.nombre_del_trabajador,catalogo_cursos.nombre_catalogo");
        $this->db->join("usuario", "usuario.id_usuario=examen.usuario_id_usuario", "INNER");
        $this->db->join("catalogo_cursos", "catalogo_cursos.id_catalogo_cursos=examen.catalogo_cursos_id_catalogo_cursos", "INNER");
        $this->db->where("md5(examen.id_examen)", $id);
        $this->db->limit(1);
        $query = $this->db->get("examen");
        return $query->result_array();
    }

    function add() {
        /* we change this if you don´t need work with webservice please uncoment this lines */
        //if(isset($_POST) and !empty($_POST))
        //	{
        $data = $_POST;
        $query = $this->db->insert("examen", $data);
        //redirect("/examen/admin/", "refresh");
        //	}
    }

    function add_by_data($data=null) {
        /* we change this if you don´t need work with webservice please uncoment this lines */
        //if(isset($_POST) and !empty($_POST))
        //	{
        if ($data !==NULL and is_array($data)) {
            $query = $this->db->insert("examen", $data);
        }
        
        //redirect("/examen/admin/", "refresh");
        //	}
    }
    
    function update($id) {
        //if((isset($id) and $id>0) and (isset($_POST) and !empty($_POST)))
        //{
        $data = $_POST;
        $this->db->where("md5(id_examen)", $id);
        $query = $this->db->update("examen", $data);
        //redirect("/examen/admin/", "refresh");
        //}
    }

    public function delete($id) {
        //if(isset($id) and $id>0 and !empty($_POST))
        //{
        //if($_POST["confirmar"]==="TRUE"){
        $this->db->where("md5(id_examen)", $id);
        $query = $this->db->delete("examen");
        //redirect("/examen/admin/TRUE/", "refresh");
        //}
        //else
        //{
        //redirect("/examen/admin/FALSE/", "refresh");
        //}
        //}
    }

    public function get_usuario_nombre_del_trabajador() {
        $this->db->select("nombre_del_trabajador,id_usuario");
        $query = $this->db->get("usuario");
        return $query->result_array();
    }

    public function get_catalogo_cursos_nombre_catalogo() {
        $this->db->select("nombre_catalogo,id_catalogo_cursos");
        $query = $this->db->get("catalogo_cursos");
        return $query->result_array();
    }

    public function get_by_nombre_examen($nombre_examen = NULL) {
        $this->db->like("nombre_examen", $nombre_examen);
        $query = $this->db->get("examen");
        return $query->result_array();
    }

    public function get_by_calificacion($calificacion = NULL) {
        $this->db->like("calificacion", $calificacion);
        $query = $this->db->get("examen");
        return $query->result_array();
    }

    public function get_by_examen_fields($examen = NULL) {
        $this->db->or_like("nombre_examen", $examen);

        $this->db->or_like("calificacion", $examen);
        $query = $this->db->get("examen");
        return $query->result_array();
    }

}
