<nav class="navbar navbar-inverse josefin" role="navigation">

            <div class="container-fluid">              

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-drop">

                        <span class="sr-only">Toggle navigation</span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                    </button>                

                </div>

                <!-- menu colapsado -->

                <div class="collapse navbar-collapse" id="menu-drop">

                        <ul class="nav navbar-nav hidden-xs">      

                            <li  ><a href="<?php  $segments= array("panel"); echo site_url($segments); ?>"><i class="fa fa-home"></i> Inicio</a></li>

                             <li><a href="<?php  $segments = array("usuario_has_catalogo_cursos","generar_oferta"); echo site_url($segments);?>"><i class="fa fa-plus-square"></i> Asignar oferta</a></li>

                                   

                            <li><a href="<?php  $segments = array("usuario","finalizar_proceso"); echo site_url($segments);?>"><i class="fa fa-check-circle-o"></i> Terminar proceso</a></li>

                            <li><a href="<?php  $segments = array("usuario","inscritos"); echo site_url($segments);?>"><i class="fa fa-certificate"></i> Inscritos</a></li>

                             <li class="dropdown">

                                 <a class="dropdown-toggle" data-toggle="dropdown">Vistas detalladas<b class="caret"></b></a>

                                 <ul class="dropdown-menu">

				 <?php if(isset($item) and !empty($item))

				 {

					foreach ($item as $keys=>$items){

		  		 ?>

                                <li><a href="<?php $segments= array($items); echo site_url($segments);?>/admin"><?php echo $keys ?></a></li>

                                <?php }

                                }

                                ?> 

                                </ul>

                            </li>                        

                        </ul>

                    <!-- drop down en dispositivos moviles provablemente falle en iphones -->

                    <ul class="nav navbar-nav  hidden-sm hidden-md  hidden-lg ">      

                             <li  ><a href="<?php  $segments= array("panel"); echo site_url($segments); ?>">Inicio</a></li>

                            

                                 

				 <?php if(isset($item) and !empty($item))

				 {

					foreach ($item as $keys=>$items){

		  		 ?>

                                     <li><a class="" href="<?php $segments= array($items); echo site_url($segments);?>"><?php echo $keys ?></a></li>

                                <?php }

                                }

                                ?> 

                          

                            <li><a href="<?php  $segments = array("usuario_has_catalogo_cursos","generar_oferta"); echo site_url($segments);?>"><i class="fa fa-bar-chart-o"></i> Asignar oferta</a></li>

                        </ul>

                    

                    <ul class="nav navbar-nav navbar-right">

                            <li><a href="<?php   $segments = array("robotoit_user","update",$_SESSION["id_user"]); echo site_url($segments);?>"><i class="fa fa-user"></i> Mi perfil</a></li>

                            <?php if($_SESSION["status"]=="1"){?>

                            <li><a href="<?php  $segments = array("robotoit_user","admin"); echo site_url($segments);?>"><i class="fa fa-users"></i> Administrar usuarios</a></li>

                           

                                <?php }?>

                            <li><a href="<?php $segments = array("login","close"); echo site_url($segments);?>"><i class="fa fa-sign-out"></i> Salir</a></li> 

                        </ul>

                </div>                            

            </div>

        </nav>