<nav class="subnav lp-subnav" style="margin-bottom: 10px;">
   <ul class="nav nav-pills">
      <li  ><a href="<?php  $segments= array("panel"); echo site_url($segments); ?>">Inicio</a></li>
      <li class="dropdown">
         <a class="dropdown-toggle" data-toggle="dropdown">Vistas detalladas<b class="caret"></b></a>
         <ul class="dropdown-menu">
				<?php if(isset($item) and !empty($item))
				{
					foreach ($item as $keys=>$items){
				?>
               <li><a href="<?php $segments= array($items,$session_navigator); echo site_url($segments);?>"><?php echo $keys ?></a></li>
               
			   
			   <?php }
			   }
                           
                          
			   ?>
               
         </ul>
      </li>
      <li><a href="<?php  $segments = array("Modulo_Servidor","server_list"); echo site_url($segments);?>"><i class="fa fa-bar-chart-o"></i> Estado de respaldos</a></li>
      <ul class="nav nav-pills pull-right">
          
          <li><a href="<?php   $segments = array("robotoit_user","update",$_SESSION["id_user"]); echo site_url($segments);?>"><i class="fa fa-user"></i> Mi perfil</a></li>
          <?php if($_SESSION["status"]=="1"){?>
          <li><a href="<?php  $segments = array("robotoit_user","admin"); echo site_url($segments);?>"><i class="fa fa-users"></i> Administrar usuarios</a></li>
          <li><a href="<?php  $segments = array("area","admin"); echo site_url($segments);?>"><i class="fa fa-sitemap"></i> Administrar Areas</a></li>
          <?php }?>
          <li><a href="<?php $segments = array("login","close"); echo site_url($segments);?>"><i class="fa fa-sign-out"></i> Salir</a></li>
      </ul>
   </ul>
</nav>