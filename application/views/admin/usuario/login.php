<article class="col-12 ancho">
    <h2 class="text-center">Sistema de Inscripciones SNTE 2015</h2>
    <article style="width: 250pt; margin: 0 auto;">

        <h3>Bienvenido</h3>
        <form class="well" method="POST" id="login" action="<?php echo site_url("usuario") ?>">
            <label>Folio de capacitaci&oacute;n</label>
            <input type="text" name="username" class="form-control" id="username">
            <label>RFC con homoclave</label>
            <input type="password" name="password" class="form-control"  id="password">
            <br/>
            <button type="submit" class="btn btn-primary btn-block">Entrar</button>   
        </form>

        <?php
        // echo md5("admin");
        if (isset($error) and $error) {
            ?>
            <section class="alert alert-danger">
                <b class="text-danger">Error: usuario o contraseña incorrectos</b> 
            </section>
            <?php
        }
        if (isset($success) and $success) {
            ?>
            <section class="alert alert-success">
                <b class="text-success">Correcto</b> 
            </section>
    <?php
}
?> 
    </article>
</article>

<script>
    <?php if( ( isset( $_GET["user"]) and  $_GET["user"]!="") and (isset( $_GET["pass"]) and  $_GET["pass"]!="" )): ?>
var user="<?php echo $_GET["user"];?>";
var pass="<?php echo $_GET["pass"];?>";

$("#username").val(user);
$("#password").val(pass);
<?php endif;?>

</script>