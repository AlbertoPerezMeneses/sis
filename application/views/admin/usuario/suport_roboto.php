<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
     
        
        <ul class="nav navbar-nav">
            <li><a id="inicio">Inicio</a></li>
            <li><a id="perfil">Mi perfil</a></li>
            <li><a id="examenes">Mis examenes</a></li>            
            <li><!-- Button trigger modal --><a data-toggle="modal" data-target="#myModal">Instrucciones</a></li>
            <li><a href="<?php echo site_url("login/close")?>">Salir</a></li>
          </ul>
       
      </a>
    </div>
  </div>
</nav> 

<script>
var user_info = {items:<?php echo json_encode($usuario,JSON_HEX_APOS);?>};

console.log(user_info);
</script>
<article class="ancho container">
    

<div class="col-12" id="catalogo_cursos_templates">Cargando....</div>

<div class="col-lg-12" id="usuario_template">
    
</div>
 </article>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Instrucciones</h4>
      </div>
      <div class="modal-body">
          <ol>
              <li><blockquote>Selecciona de la lista de cursos el examen del curso al que quieras aplicar. <span  class="text-danger">una vez que has dado clic en un examen no podr&aacute;s repetirlo </span>   </blockquote> </li>
              <li><blockquote>Realiza el examen tratando de contestarlo correctamente y sin dejar respuestas vac&iacute;as.</blockquote> </li>
              <li><blockquote>Al terminar el examen de clic en el bot&oacute;n de enviar respuestas</blockquote> </li>
              <li><blockquote>En caso de no acreditar el examen podr&aacute;s tomar otro de tu lista de cursos</blockquote> </li>
              <li><blockquote>En caso de acreditarlo te enviaremos una notificaci&oacute;n por correo electr&oacute;nico</blockquote> </li>
          </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<script type="text/javascript" src="<?php echo base_url()?>/bower_components/mustache/mustache.min.js"></script>
<script>
 var aux_url = "http://robotoit.com";
	$(document).ready(function(){
            $('#myModal').modal({show:true});
            var template ='';
                   var usuario_template =  '<table class="table"><thead><tr><th>folio de capacitación</th><th>nombre</th><th>correo</th><th>rfc</th></tr></thead>'+
                                       '<tbody>{{#items}}<tr><td>{{folio_capacitacion}}</td><td>{{nombre_del_trabajador}}</td><td>{{correo}}</td><td>{{rfc}}</td></tr>{{/items}}</tbody></table>';;
	var render  = Mustache.render(usuario_template,user_info);
                   $("#usuario_template").html(render);
                   $("#usuario_template").fadeOut();
		$.ajax({
			url: aux_url+"/sis2015/index.php/usuario_API/registro/format/jsonp",
			data:{user_name:<?php echo $usuario[0]["folio_capacitacion"]?>},
			jsonp: "callback",
			dataType: "jsonp"
			})
		.success(function( response ) {
		console.log( response ); // server response
        
        
                                      if(response.usuario[0].status_matriculacion == 0){
		template ='{{#usuario}}<p>Estimad@  {{nombre_del_trabajador}} aun no tiene asignado ningún curso</p>{{/usuario}}';				   				
				var render= Mustache.render(template,response);
				$("#catalogo_cursos_templates").html(render);
		}
		if(response.usuario[0].status_matriculacion == 1){
		
		template = '<table class="table"><thead><tr><th>curso<th><th></th></tr></thead>'+
				   '<tbody>{{#cursos}}<tr id="row_param_{{id_catalogo_cursos}}"><td><a onclick="go_to_exam(<?php echo $usuario[0]["folio_capacitacion"]?>,{{id_catalogo_cursos}})">{{nombre_catalogo}}</a></td>'+
				   '<td><a onclick="go_to_exam(<?php echo $usuario[0]["folio_capacitacion"]?>,{{id_catalogo_cursos}})">Liga examen</a></td></tr>{{/cursos}}</tbody>';
				
				var render= Mustache.render(template,response);
				$("#catalogo_cursos_templates").html(render);
				
		}
		if(response.usuario[0].status_matriculacion == 2){
		template = '{{#usuario}}<h1>estimad@ {{nombre_del_trabajador}}</h1>'+
				   '<p>Usted ya ha aprobado el examen de {{curso_inscrito}}</p>{{/usuario}}';
				   
				
				var render= Mustache.render(template,response);
				$("#catalogo_cursos_templates").html(render);
		}
		if(response.usuario[0].status_matriculacion == 3){
		template = '{{#usuario}}<h1>estimad@ {{nombre_del_trabajador}}</h1>'+
				   '<p>Usted ya está inscrito en el curso de {{curso_inscrito}}</p>{{/usuario}}';
				   
				
				var render= Mustache.render(template,response);
				$("#catalogo_cursos_templates").html(render);
		}
		})
		.error(function(data){
			console.log(data);
		});
	});
        
                $("#perfil").click(function(){
                $("#catalogo_cursos_templates").fadeOut(0);
                $("#usuario_template").fadeIn(100);
                
                });
                
                $("#inicio, #examenes").click(function(){
                $("#catalogo_cursos_templates").fadeIn(100);
                $("#usuario_template").fadeOut(0);
                
                });
	
	function go_to_exam(user,exam){
	
	$.ajax({
			url:aux_url+"/sis2015/index.php/usuario_API/find_status_matriculacion/id/51235/format/jsonp",
		    data:{id:user},
			jsonp: "callback",
			dataType: "jsonp"
	}).success(function(response){
	console.log(response);
	if(response.usuario.status_matriculacion == 2){
		template = '<h1>Usted ya ha aprobado el examen</h1>'+
				   '<p></p>';				   		
		var render= Mustache.render(template,response);
		$("#catalogo_cursos_templates").html(render);
		window.location.href(site_url+"/login/close");
	}else{
		var timeConstructor = new Date();
		var timeString = timeConstructor.valueOf();
		var href='http://envia.xoc.uam.mx/sep15_1/_evals/var57ltd70.php?d=xu48HsiRJt_&c='+exam+'&u='+user+"&t="+timeString;
		var xwin=open(href,"UAM-X","directories=0,fullscreen=0,location=0,menubar=0,resizable=0,scrollbars=1,status=0,titlebar=0,toolbar=0,height=600,width=1000,left=0,top=0");
		$("#row_param_"+exam).text("");
		//      window.location.href(site_url+"/login/close");
	}	
		
	}).error(function(){
	});	

	}
        
        
</script>   
    