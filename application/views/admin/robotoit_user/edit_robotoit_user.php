<?php
if (isset($_SESSION) AND !empty($_SESSION)) {
    if (!empty($robotoit_user) and isset($robotoit_user)) {
        ?>
        <div class="row" form-group>
            <div class="col-lg-3">
                <!-bread crumbs->                
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url("panel/index") ?>">Home</a></li>  
                    <!-segunda ruta-> 
                    <?php if($_SESSION["status"]=="1"): ?>
                    <li><a href="<?php  echo site_url("robotoit_user/admin") ?>">Usuarios</a></li>  
                    <?php  endif; ?>
                    <!-segunda ruta->
                    <li class="active">Modificar Perfil</li>
                </ol>
                
            </div>
            <div class="col-lg-6">
                <h2 class="text-right">Modificar Usuario</h2>
                <article>
                    <img src="<?php echo base_url("assets/galeria/" . $robotoit_user[0]["imagen"]); ?>" class="img-thumbnail" height="100px" width="100px"/>
                    <section>
                        <form accept-charset="utf-8" enctype="multipart/form-data"  action="<?php $segments = array('robotoit_user', 'update', $id);
        echo site_url($segments); ?>" method="post" id="robotoit_user" class="form-horizontal">
                            <section class="form-name">
                                <label>Nombre de usuario</label>
                            </section>
                            <section class="form-item">
                                 <?php if($_SESSION["status"]=="1"): ?>
                                <input type="text" value="<?php echo $robotoit_user[0]['user_name']; ?>" name="user_name" id="user_name" class="form-control"/>
                                 <?php  else: ?>
                                <?php echo $robotoit_user[0]['user_name']; ?>
                                <input type="hidden" value="<?php echo $robotoit_user[0]['user_name']; ?>" name="user_name" id="user_name"  readonly/>
                                <?php   endif;?>
                            </section>
                           

 <label>Imagen de usuario</label>
                            <section>
                                <input type="file" name="imagen" class="form-control" accept="image/*"/>
                            </section>
                            <input type="submit" value="enviar" class="form-control" />
                            <br/>

                        </form>
                    </section>
                    <a href="<?php $segments = array('robotoit_user', 'update_pass', $id);
                    echo site_url($segments); ?>"><i class="fa fa-edit"></i> Cambiar contraseña</a>
                </article>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <?php
    }
} else {
    redirect("/login/", "refresh");
}
echo $client;
?>
		