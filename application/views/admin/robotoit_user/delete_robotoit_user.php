<?php
if (isset($_SESSION) AND !empty($_SESSION)) {
    ?>
    <div class="row" form-group>
        <div class="col-lg-3">
            <!-bread crumbs->                
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url("panel/index") ?>">Home</a></li>  
                <!-segunda ruta-> 
                <li><a href="<?php echo site_url("robotoit_user/admin") ?>">Usuarios</a></li>  
                <!-segunda ruta->
                <li class="active">Borrar Usuario</li>
            </ol>
        </div>
        <div class="col-lg-6">
            <h2 class="text-right">Borrar Usuario</h2>

            <article>
                <section>
                    <form accept-charset="utf-8"  action="<?php $segments = array('robotoit_user', 'delete', $id);
    echo site_url($segments); ?>" method="post" id="robotoit_user" class="form-horizontal">
                        <section class="form-name"><label>confirmar</label></section><section class="form-item"><select name="confirmar" id="confirmar " class="form-control">
                                <option value="FALSE">no</option>
                                <option value="TRUE">si</option>
                            </select>
                        </section><input type="submit" value="eliminar" class="form-control" />
                        <br/>
                    </form>
                </section>
            </article>
        </div>
        <div class="col-lg-3"></div>  
    </div>
    <?php
} else {
    redirect("/login/", "refresh");
}
?>