<?php
if (isset($_SESSION) AND !empty($_SESSION)) {
    if (!empty($robotoit_user) and isset($robotoit_user)) {
        ?>
        <div class="row" form-group>
            <div class="col-lg-3">
                <!-bread crumbs->                
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url("panel/index") ?>">Home</a></li>  
                    <!-segunda ruta-> 
                     <?php if($_SESSION["status"]=="1"): ?>
                    <li><a href="<?php echo site_url("robotoit_user/admin") ?>">Usuarios</a></li>  
                    <?php  endif; ?>
                    <!-segunda ruta->
                    <li class="active">Modificar contraseña</li>
                </ol>

            </div>
            <div class="col-lg-6">
                <h2 class="text-right">Modificar Contraseña</h2>
                <article>
                    <img src="<?php echo base_url("assets/galeria/" . $robotoit_user[0]["imagen"]); ?>" class="img-thumbnail" height="100px" width="100px"/>
                    <section>
                    
                    
                    <form accept-charset="utf-8" enctype="multipart/form-data"  action="<?php $segments = array('robotoit_user', 'update_pass',$id); echo site_url($segments); ?>" method="post" id="robotoit_user" class="form-horizontal">
                        <section>
                            <h1 class="text-center"><?php echo  $robotoit_user[0]['user_name']; ?></h1>
                        </section>
                  
                        <section class="form-item">
                            <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Contraseña"/>
                            <input type="password" name="user_password" id="user_password1" class="form-control" placeholder="Confirmar contraseña"/>
                        </section>
                        <input type="submit" value="enviar" class="form-control" />
		<br/>
               
		</form>
			</section>
                </article>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <?php
    }
} else {
    redirect("/login/", "refresh");
}
echo $client;
?>
		