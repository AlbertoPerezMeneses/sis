<?php
?><article class="nuevo">
    <!--bread crumbs-->		
    <ol class="breadcrumb">

        <li><a href="<?php echo site_url("panel/index") ?>">Home</a></li>  
        <!--segunda ruta-->		
        <li class="active">Usuarios</li>
    </ol>

</article>
<h2 class="text-right">Usuarios</h2>
<a href="<?php $segments = array("robotoit_user", "create");
echo site_url($segments); ?>">Agregar</a>
<article class="table-responsive">

    <table class="table table-hover" id="pagination">
        <thead>
            <tr>
                <th>Nombre</th> 
                <th>Correo</th>

                <th>Imagen</th>
                <th> </th>
                <th> </th>


            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($_SESSION) and isset($_SESSION)) {
                if (!empty($robotoit_user) and isset($robotoit_user)) {
                    foreach ($robotoit_user as $dato) {
                        ?><section id="resultado">

                        <tr>
                            <td><?php echo $dato["user_name"]; ?></td>
                          

                            <td>  <img src="<?php echo base_url("assets/galeria/" . $dato["imagen"]); ?>" class="img-thumbnail" height="100px" width="100px"/></td> 
                            <td> <a href="<?php $segments = array("robotoit_user", "update", md5($dato["id_user"]));
            echo site_url($segments); ?>">Actualizar</a> </td>

                            <?php
                            if (strcmp(md5($dato["id_user"]), $_SESSION["id_user"]) != 0) {
                                ?>
                                <td><a href="<?php $segments = array("robotoit_user", "delete", md5($dato["id_user"]));
                echo site_url($segments); ?>">Eliminar</a> </td>              
                                <?php
                            } else {
                                ?>
                                <td></td>
                                <?php
                            }
                            ?>
                            <?php // echo $dato["user_password"];?> 
                        </tr>           
                    </section>
                    <?php
                }
            }
        } else {
            redirect("/login/", "refresh");
        }
        ?>
        </tbody>
    </table>
</article>
