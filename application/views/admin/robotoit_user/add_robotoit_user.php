<?php
if (isset($_SESSION) AND !empty($_SESSION)) {
    ?>
    <div class="row" form-group>

        <div class="col-lg-3">
            <!-bread crumbs->                
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url("panel/index") ?>">Home</a></li>  
                <!-segunda ruta-> 
                <li><a href="<?php echo site_url("robotoit_user/admin") ?>">Usuarios</a></li>  
                <!-segunda ruta->
                <li class="active">Agregar Usuario</li>
            </ol>
        </div>
        <div class="col-lg-6">

            <h2 class="text-right">Agregar Usuario</h2>
            <article>
                <section>
                    <form accept-charset="utf-8" enctype="multipart/form-data" action="<?php $segments = array('robotoit_user', 'create');
    echo site_url($segments); ?>" method="post" id="robotoit_user" class="form-horizontal">
                        <section class="form-name">
                            <label>Nombre de usuario</label>
                        </section>
                        <section class="form-item">
                            <input type="text"  name="user_name" id="user_name" class="form-control"/>
                        </section>
                       
                        <section class="form-name">
                            <label>Contraseña</label>
                        </section>
                        <section class="form-item">
                            <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Contraseña"/>
                            <input type="password" name="user_password" id="user_password1" class="form-control" placeholder="Confirmar contraseña"/>
                        </section>
                         <label>Imagen de usuario</label>
                        <section>
                            <input type="file" name="imagen" class="form-control"/>
                        </section>
                        <br>
                        <input type="submit" value="enviar" class="form-control" />
                    </form>
                </section>
            </article>
        </div>
        <div class="col-lg-3"></div>   
    </div>   

    <?php
} else {
    redirect("/login/", "refresh");
}
echo $client;
?>
