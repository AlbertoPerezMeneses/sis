
<table style="background:#CCC;" width="100%" border="0" cellspacing="0" cellpadding="0"">

<tbody style="font:Verdana, Geneva, sans-serif;">
<tr style="background:#0072CE; color:white; text-align:center;"> 

<td width="195" height="163"><img src="http://robotoit.com/appcecad/assets/galeria/conjunto-baseXoc.png" width="160"  /></td>
<td width="497">
<h2>Bienvenid@ al proceso de inscripciones UAM Xochimilco</h2>
</td>
<td width="197"></td>
</tr>

<tr>
<td height="176"></td>
<td style="background-color:white; color:#333">
La Universidad Aut�noma Metropolitana, Unidad Xochimilco, a trav�s de la Coordinaci�n de 
Educaci�n Continua y a Distancia te da la m�s cordial bienvenida. 
</td>
<td></td>
</tr>

<tr> 
<td height="118"></td>
<td style="background-color:white; color:#333"">
<p>Estimad@: {nombre_del_trabajador}</p>
<p>
Te comentamos que has quedado inscrito en la UAM Xochimilco.
Para revisar tus datos de inscripcion entra a la siguiente pagina:

<a href="http://educasep.xoc.uam.mx/inscripcion/index.php?user={folio_capacitacion}&pass={rfc}">ver pagina</a>
</p>
  <p>&nbsp;</p></td>
<td></td>
<td width="7"></td>
</tr>

<tr> 
<td height="122"></td>
<td style="background-color:white; color:#333"">
  <p>los datos de acceso son los siguientes:</p>

<p>usurio: {folio_capacitacion}</p>

<p>password: {rfc}</p>
</td>
<td></td>
</tr>

<tr> 
<td height="127"></td>
<td style="background-color:white;">&nbsp;</td>
<td></td>
</tr>

<tr>
<td></td> 
<td style="background-color:white;">
<p>
El registro es un tr�mite personal y confidencial. El trabajador no debe compartir sus actividades claves y n�meros personales ya que son su responsabilidad.
</p>
<p>
El examen es un tr�mite personal y confidencial. El trabajador no debe compartir sus claves y n�meros personales, ya que estos son su responsabilidad.
</p>
</td>
<td></td>
</tr>


<tr style="font-size:10px; background-color:#666; color:#FFF;">
<td height="148"><a href="http://www.xoc.uam.mx/">UAM Xochimlco</a></td> 
<td>
Universidad Aut�noma Metropolitana , Unidad Xochimilco Coordinaci�n de Educaci�n Continua y a Distancia Proyecto de Educaci�n Virtual y a Distancia
</td>
<td>Calzada del Hueso 1100, Col. Villa Quietud, Delegaci�n Coyoac�n, C.P. 04960, D.F. M�xico Edificio �A� 2� piso.</td>
</tr>
<tr>
<td height="1"></td>
<td></td>
<td></td>
</tr>
</tbody>

</table>