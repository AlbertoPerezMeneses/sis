<table style="background:#CCC;" width="100%" border="0" cellspacing="0" cellpadding="0"">

<tbody style="font:Verdana, Geneva, sans-serif;">
<tr style="background:#0072CE; color:white; text-align:center;"> 

<td width="195" height="163"><img src="http://robotoit.com/appcecad/assets/galeria/conjunto-baseXoc.png" width="160"  /></td>
<td width="497">
<h2>Bienvenid@ al proceso de inscripciones UAM Xochimilco</h2>
</td>
<td width="197"></td>
</tr>

<tr>
<td height="176"></td>
<td style="background-color:white; color:#333">
La Universidad Autónoma Metropolitana, Unidad Xochimilco, a través de la Coordinación de 
Educación Continua y a Distancia te da la más cordial bienvenida. 
</td>
<td></td>
</tr>

<tr> 
<td height="118"></td>
<td style="background-color:white; color:#333""><p>Para continuar con tu proceso de inscripción entra a la siguiente página: <a href="http://educasep.xoc.uam.mx/inscripcion/index.php?user={user}&amp;pass={pass}" >http://educasep.xoc.uam.mx/inscripcion/index.php?user={user}&amp;amp;pass={pass}</a></p>
  <p> o da clic en el siguiente vinculo: <a href="http://educasep.xoc.uam.mx/inscripcion/index.php?user={user}&amp;pass={pass}" >página de inscripciones</a></p></td>
<td><a href="http://robotoit.com/sis2015/index.php?user={user}&pass={pass}" ></a></td>
<td width="7"></td>
</tr>

<tr> 
<td height="122"></td>
<td style="background-color:white; color:#333""><p>Los datos de acceso son los siguientes:</p>
  <p>usuario: {user}</p>
  <p>password: {pass} </p></td>
<td></td>
</tr>

<tr> 
<td height="122"></td>
<td style="background-color:white; color:#333""><p>Puedes revisar nuestra oferta académica en la siguiente página: <a href="http://educasep.xoc.uam.mx/virtual_uam/sitio/app" >liga</a></p>
  <p></p>
  <p>Nota: La oferta es asignada dependiendo de tu grupo función  </p></td>
<td></td>
</tr>


<tr> 
<td height="127"></td>
<td style="background-color:white;">&nbsp;</td>
<td></td>
</tr> 

<tr>
<td></td> 
<td style="background-color:white;">
<p>
El registro es un trámite personal y confidencial. El trabajador no debe compartir sus actividades claves y números personales ya que son su responsabilidad.
</p>
<p>
El examen es un trámite personal y confidencial. El trabajador no debe compartir sus claves y números personales, ya que estos son su responsabilidad.
</p>
</td>
<td></td>
</tr>


<tr style="font-size:10px; background-color:#666; color:#FFF;">
<td height="148"><a href="http://www.xoc.uam.mx/">UAM Xochimlco</a></td> 
<td>
Universidad Autónoma Metropolitana , Unidad Xochimilco Coordinación de Educación Continua y a Distancia Proyecto de Educación Virtual y a Distancia
</td>
<td>Calzada del Hueso 1100, Col. Villa Quietud, Delegación Coyoacán, C.P. 04960, D.F. México Edificio “A” 2° piso.</td>
</tr>
<tr>
<td height="1"></td>
<td></td>
<td></td>
</tr>
</tbody>

</table>