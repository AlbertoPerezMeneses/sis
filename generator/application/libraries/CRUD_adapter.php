<?php

/**
 * Description of CRUD_adapter
 *
 * @author Roboto
 */
class CRUD_adapter{
    
    private $columns;
    private $primary;
    private $foreigns;
    private $input;
    private $validation;
    private $configuration;
    private $table;
    public function init($form_configuration = array(),$table = NULL) {
        $this->reset();
        $this->configuration=$form_configuration;
        $this->table=$table;
        return $this->process_data($form_configuration);
        
    }
    private function process_data($configuration)
            {
              $data["table"]=$this->table;
              $data["primary"] = $this->primary= $this->primary();
              $data["foreigns"] = $this->foreigns= $this->foraigns();
              $data["columns"] = $this->columns = $this->columns();
              $data["type"]=$this->input;
              $data["validation"]=$this->validation;
              return $data;
            }
    
    private function primary()
            {
                if($this->exists_field($this->configuration[$this->table], "primary"))
                {
                    //return $this->primary;
                    return $this->configuration[$this->table]["primary"];
                }
                else
                {
                    return NULL;
                }
            }
    
    private function foraigns()
    {
        if(isset($this->configuration[$this->table]["relation"]))
        {
            if( is_array( $this->configuration[$this->table]["relation"] ) && !empty( $this->configuration[$this->table]["relation"] ) )
            {
                        return $this->configuration[$this->table]["relation"];
            }
        }
        else
        {
            return NULL;
        }
    }
    
    private function columns()
            {
            if(isset($this->configuration[$this->table]["columns"]))
                {
            
                if(is_array($this->configuration[$this->table]["columns"]) && !empty($this->configuration[$this->table]["columns"]))
                    {
                    foreach (array_keys($this->configuration[$this->table]["columns"]) as $column)
                        {
                        $this->input[$column]=$this->configuration[$this->table]["columns"][$column]["type"];
                        $this->validation[$column]=$this->configuration[$this->table]["columns"][$column]["validation"];
                        }
                    return array_keys($this->configuration[$this->table]["columns"]);
                    }
                    else
                    {
                    return NULL;    
                    }
            }  else {
            return NULL;    
            }    
            }
    private function exists_field($array,$assoc)
            {
                $array_validation=( is_array( $array ) && !empty( $array ) );
                $field_validation= ( isset ( $array[$assoc] ) && ( $assoc!=  trim( $array[$assoc] ) ) );
                if($array_validation  /**/ && /**/ $field_validation)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }
   private function  reset()
           {
            $this->table=NULL;
            $this->configuration=NULL;
            $this->foreigns=NULL;
            $this->input=NULL;
            $this->table=NULL;
            $this->validation=NULL;
           }
}

?>
