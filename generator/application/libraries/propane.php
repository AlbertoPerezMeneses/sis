<?php
include_once("filep.php");
class Propane extends Filep {
    
    private $dumpPath;
    private $dirs;    
    
    
    public function __construct() {
        parent::__construct();
        $this->dumpPath = $this->path() . "generator/application/";
        $this->dirs = array();       
    }

    private function propane_Settings($settings)
            {
        $settings='
<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
/*
 * this option define the driver 
 */
$settings["driver"]="'.$settings["driver"].'";
/*
 * this option define the version of the framework
 */
$settings["version"]="'.$settings["version"].'";
/*
 * this option create all cruds when is true and 
 * var_dump last table when is false
 */
$settings["create"]=TRUE;
/*
 * this option delete all cruds selected in the form settings
 */
$settings["delete"]=FALSE;
/*
 * this option set the backup mode
 */
$settings["backup"]="'.$settings["backup"].'";
/*
 * this load all configurations
 */
$config["propane"]=$settings;
        ';
        
        return $settings;
            }
           public function add_settings($settings)
           {
               $this->add_file("config/","propane",$this->propane_Settings($settings));
               
           }
           
          private function add_file($path = NULL, $fileName = NULL, $data = NULL, $ext=".php") {
        $dir = $this->dumpPath . $path;
        $url = $dir . $fileName . $ext;
        $this->dirs[] = $url;

        if (file_exists($dir)) {
            $this->write($url, $data);
            return "se creo el archivo " . $url;
        } else {
            return "error directorio no encontrado";
        }
    }
            
}