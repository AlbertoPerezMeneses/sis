<?php



    class Filep {

        private $flag;

        function __construct() {
            $this->flag = "w";
        }

        public function delete($url) {
            if (file_exists($url)) {
                unlink($url);
                return true;
            } else {
                return false;
            }
        }

        /*         * funcion que diseñada para comparar las extenciones de los archivos  solo hay que escribir la extencion
          retorna un valor boleano
          Ex: validateMime("roger waters.pdf","pdf");
          regresa "true"
         */

        public function validateMime($fName, $ext) {
            $mime = explode(".", $fName);
            $end = end($mime);
            if ($end == $ext) {
                return(true);
            } else {
                return(false);
            }
        }

        public function write($url, $data) {
            try {
                $fp = fopen($url, $this->flag);
                fwrite($fp, $data);
                fclose($fp);
                return true;
            } catch (Exception $e) {
                return false;
            }
        }

        public function setFileFlag($flag) {
            $this->flag = $flag;
        }

        /*
          funcion que para crear archivos retorna un valor boleano
          nota: el formulario deve contener este template

          <form enctype="multipart/form-data" action="sql/administracion.php" method="POST" >
          <input type="hidden" name="MAX_FILE_SIZE" value="200000" />
          <input name="Filedata" type="file" id="file"  />
          <input type="submit" value="enviar" />
          </form>

         */

        public function insert_file($directorio, $name = "file") {
            $tempFile = $_FILES[$name]['tmp_name'];
            $fileName = $_FILES[$name]['name'];
            $fileSize = $_FILES[$name]['size'];
            $result = move_uploaded_file($tempFile, "./../../" . $directorio . "" . $fileName);
            $file = fopen("log.txt", "w");
            if ($result) {
                fprintf($file, "success %s", $fileName);
                return true;
            } else {
                fprintf($file, "failure %s", $fileName);
                fclose($file);
                return false;
            }
        }

        public function get_dirs($dir = NULL) {
            if (file_exists($dir)) {
                $dirh = opendir($dir);
                if ($dirh) {
                    while (($dirElement = readdir($dirh)) !== false) {
                        $dirs[] = $dirElement;
                    }
                    closedir($dirh);
                }
                return $dirs;
            }
        }

        public function move($from, $to) {
            $data["from"] = $from;
            $data["to"] = $to;
            if (file_exists($from)) {
                $data["move"] = rename($from, $to);
            }
            return $data;
        }

        /*
         * only for codeigniter
         */

        public function path() {
            if (!defined(BASEPATH)) {
                $data = explode("/", BASEPATH);
                array_pop($data);
                array_pop($data);
                array_pop($data);
                return implode("/", $data) . "/";
            } else {
                die("you need define basepath");
            }
        }

        protected function remove_dir($name = NULL) {
            if (file_exists($name)) {
                if (rmdir($name)) {
                    return "delete dir : " . $name;
                } else {
                    return "couldn't delete this dir";
                }
            }
        }

    }
