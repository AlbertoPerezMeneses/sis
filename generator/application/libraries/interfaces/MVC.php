<?php 

interface MVC
{
public function createModel();
public function createView();
public function createController();
public function createDirectory();
}