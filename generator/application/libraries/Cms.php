<?php

include_once("filep.php");

//class Cms extends \Libraries\File\File {
class Cms extends Filep {

    private $dumpPath;
    private $dirs;
    private $folders;
    private $table;

    public function __construct() {
        parent::__construct();
        $this->dumpPath = $this->path() . "application/";
        $this->dirs = array();
        $this->folders = array();
    }

    public function process($data = Array(), $table = NULL, $boolean = FALSE, $backup = "none") {
        $size = count($data);
        /*
         * esta validacion es un poco rigida pero asegura que la aplicacion funcione de manera correcta
         */
        $validation = (is_array($data) && !empty($data) && $size === 9 && isset($data["selectAllAdmin"]));

        if ($validation) {
            if ($boolean) {

                /*
                 * experimental
                 */
                $log["backup"] = $this->backup($table, $backup);
                /*
                 * 
                 */

                /*
                 * directory
                 */
                $this->table = NULL;
                $this->table = $table;
                /*
                  only for create an api
                 */
                if (TRUE) {
                    $log["dir_admin"] = $this->add_dir($this->dumpPath . "views/admin/" . $table);
                    $log["dir_user"] = $this->add_dir($this->dumpPath . "views/users/" . $table);



                    /*
                     * admin views
                     */
                    $log["admin_show_all"] = $this->add_admin("show_" . $table, $data["selectAllAdmin"]);
                    $log["admin_delete"] = $this->add_admin("delete_" . $table, $data["deleteForm"]);
                    $log["admin_insert"] = $this->add_admin("add_" . $table, $data["insertForm"]);
                    $log["admin_update"] = $this->add_admin("edit_" . $table, $data["updateFormAdmin"]);
                    /*
                     * user views
                     */
                    $log["view_show_all"] = $this->add_view("show_" . $table, $data["selectAllView"]);
                    $log["view_show_one"] = $this->add_view("find_" . $table, $data["selectOneView"]);
                    $log["controller"] = $this->add_controller("" . $table, $data["controller"]);
                }
                /*
                 * controller
                 */
                
                $log["controller_API"] = $this->add_controller($table . "_API", $data["controller_API"]);
                /*
                 * model
                 */

                $log["model"] = $this->add_model($table . "_model", $data["model"]);
            } else {
                $log = $data;
            }
        } else {
            $log["error"] = "error can't create CRUD";
            $log["isarray"] = is_array($data);
            $log["notempty"] = !empty($data);
            $log["size"] = $size === 8;
            $log["isset"] = isset($data["selectAllAdmin"]);
        }

        return array("status" => $boolean, "log" => $log);
    }

    public function deleteCms() {
        foreach ($this->dirs as $url) {
            $this->delete($url);
        }
        foreach ($this->folders as $url) {
            $this->remove_dir($url);
        }
    }

    public function backup($table = NULL, $method = NULL) {
        $names["show"] = "show_" . $table;
        $names["delete"] = "delete_" . $table;
        $names["add"] = "add_" . $table;
        $names["edit"] = "edit_" . $table;
        $names["show_view"] = "show_" . $table;
        $names["find"] = "find_" . $table;
        $names["controller"] = "" . $table;
        $names["model"] = $table . "_model";

        if ($method == "backup") {
            $bk_path = $this->path() . "backups/" . date("y_m_d h_i_s");
            if (file_exists($this->path() . "site/application/controllers/" . $names["controller"] . ".php")) {

                if (!file_exists($bk_path)) {
                    if (mkdir($bk_path . "/controllers/", 0777, TRUE)) {
                        $data = $this->move($this->path() . "site/application/controllers/" . $names["controller"] . ".php", $bk_path . "/controllers/" . $names["controller"] . ".php");
                        if (mkdir($bk_path . "/models/", 0777, FALSE)) {
                            $this->move($this->path() . "site/application/models/" . $names["model"] . ".php", $bk_path . "/models/" . $names["model"] . ".php");
                        }
                        if (mkdir($bk_path . "/views/", 0777, FALSE))
                            ;
                        if (mkdir($bk_path . "/views/users/" . $table . "/", 0777, TRUE)) {
                            $this->move($this->path() . "site/application/views/users/" . $table . "/" . $names["show_view"] . ".php", $bk_path . "/views/users/" . $table . "/" . $names["show_view"] . ".php");
                            $this->move($this->path() . "site/application/views/users/" . $table . "/" . $names["find"] . ".php", $bk_path . "/views/users/" . $table . "/" . $names["find"] . ".php");
                        }
                        if (mkdir($bk_path . "/views/admin/" . $table . "/", 0777, TRUE)) {
                            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["show"] . ".php", $bk_path . "/views/admin/" . $table . "/" . $names["show"] . ".php");
                            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["delete"] . ".php", $bk_path . "/views/admin/" . $table . "/" . $names["delete"] . ".php");
                            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["add"] . ".php", $bk_path . "/views/admin/" . $table . "/" . $names["add"] . ".php");
                            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["edit"] . ".php", $bk_path . "/views/admin/" . $table . "/" . $names["edit"] . ".php");
                        }
                    }
                }
            }
        }
        if ($method == "self") {
            $this->move($this->path() . "site/application/models/" . $names["model"] . ".php", $this->path() . "site/application/models/" . $names["model"] . ".bkp");
            $this->move($this->path() . "site/application/controllers/" . $names["controller"] . ".php", $this->path() . "site/application/controllers/" . $names["controller"] . ".bkp");
            $this->move($this->path() . "site/application/views/users/" . $table . "/" . $names["show_view"] . ".php", $this->path() . "site/application/views/users/" . $table . "/" . $names["show_view"] . ".bkp");
            $this->move($this->path() . "site/application/views/users/" . $table . "/" . $names["find"] . ".php", $this->path() . "site/application/views/users/" . $table . "/" . $names["find"] . ".bkp");
            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["show"] . ".php", $this->path() . "site/application/views/admin/" . $table . "/" . $names["show"] . ".bkp");
            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["delete"] . ".php", $this->path() . "site/application/views/admin/" . $table . "/" . $names["delete"] . ".bkp");
            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["add"] . ".php", $this->path() . "site/application/views/admin/" . $table . "/" . $names["add"] . ".bkp");
            $this->move($this->path() . "site/application/views/admin/" . $table . "/" . $names["edit"] . ".php", $this->path() . "site/application/views/admin/" . $table . "/" . $names["edit"] . ".bkp");
        }
        if ($method == "none") {
            $data = "NULL";
        }
        // return $data;
    }

    private function add_file($path = NULL, $fileName = NULL, $data = NULL, $ext = ".php") {
        $dir = $this->dumpPath . $path;
        $url = $dir . $fileName . $ext;
        $this->dirs[] = $url;

        if (file_exists($dir)) {
            $this->write($url, $data);
            return "se creo el archivo " . $url;
        } else {
            return "error directorio no encontrado";
        }
    }

    public function add_view($fileName = NULL, $data = NULL) {
        return $this->add_file("views/users/" . $this->table . "/", $fileName, $data);
    }

    public function add_controller($fileName = NULL, $data = NULL) {
        return $this->add_file("controllers/", $fileName, $data);
    }

    public function add_model($fileName = NULL, $data = NULL) {
        return $this->add_file("models/", $fileName, $data);
    }

    public function add_admin($fileName = NULL, $data = NULL) {
        return $this->add_file("views/admin/" . $this->table . "/", $fileName, $data);
    }

    public function add_helper($fileName = NULL, $data = NULL) {
        return $this->add_file("helpers/", $fileName, $data);
    }

    public function add_config($fileName = NULL, $data = NULL, $ext = ".php") {
        return $this->add_file("config/", $fileName, $data, $ext);
    }

    public function add_dir($name = NULL, $chmod = 0777) {
        $dir = $name . "/";
        $this->folders[] = $dir;
        if (!file_exists($dir)) {
            if (mkdir($dir, $chmod)) {
                return "the dir :" . $dir;
            } else {
                return "cant create the directory please check your permison :" . $dir;
            }
        } else {
            return "the directory exists";
        }
    }

    public function dirs() {
        return $this->dirs;
    }

}