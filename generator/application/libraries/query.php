<?php
class query{
	private $database;
	private $servidor_bd,$usuario_bd,$pass_bd;
	function __construct(){
	require_once("../conf/datos_conexion.php");
	$this->database=$base_datos;
	$this->servidor_bd=$servidor_bd;
	$this->usuario_bd=$usuario_bd;
	$this->pass_bd=$pass_bd;
		}
	
	private function conexion()
	{
		
		try
		{
			
			$conexion = mysql_connect( $this->servidor_bd, $this->usuario_bd, $this->pass_bd);
			if( $conexion )
				if( mysql_select_db($this->database) )
					return $conexion;
				else
					throw new Exception("Error al seleccionar la Base de datos"); 
			else
				throw new Exception("Error al conectar con el servidor de la Base de datos"); 	 
		}
		catch(Exception $e)
		{
			echo ("Message : " . $e->getMessage() );
		}
		
		
	}
	public	function Query($q)
	{
			$c = $this->conexion();
			mysql_query("SET NAMES 'UTF8'");
			$r = mysql_query($q, $c);
			
			mysql_close($c);
			if( gettype($r) == "resource" ) // Es tabla de resultados
				return $r;
			else
			if( gettype($r) == "boolean" )	// Es un boolean 
				return $r;
			else return "Error ".mysql_error($c);			
	}
	/*
	*funcion que obtiene los datos de un query y los regresa en forma de un arreglo bidimencional
	*ex: $data[0]["id_usuario"].
	*/
	public	function dataQuery($query){
		$data="";
		$c=$this->conexion();
		mysql_query("SET NAMES 'UTF8'");
		$result = mysql_query($query);
		if($result){
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$data[]=$row;
			}
		}
		return($data);
		mysql_close($c);
		}
	/*obtiene los nombres de un las tablas de un query
	importante hay que agregar los datos de la tabla ya que si no se encuentra ningun dato nos arrojara un error
	*/
	public function getQueryNames($query)
	{
		$c = $this->conexion();
		mysql_query("SET NAMES 'UTF8'");
		$result = mysql_query($query);
		
		
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		$keys=array_keys($row);
		
		return $keys;
		mysql_close($c);
	}
	
	public	function  QueryGetId($query)
	{
		$c = $this->conexion();
		mysql_query("SET NAMES 'UTF8'");
		$r = mysql_query($query, $c);
		if( gettype($r) == "boolean" )
			$id = mysql_insert_id($c);
		else
			$id=false;
		mysql_close($c);
		return $id;
	}
	
	public	function getdatabase()
	{
		return ($this->database); 	
	}
	
	public function setDatabase($db=null)
	{
		$this->database=$db;
	}
        public function setServer($server=null)
        {
                $this->servidor_bd=$server;
        }
        public function getServer()
        {
            return $this->servidor_bd;
        }
        public function setUser($user=null)
        {
                $this->usuario_bd=$user;
        }
        public function getUser()
        {
            return $this->usuario_bd;
        }
        public function setPassword($password=null)
        {
                $this->pass_bd=$password;
        }
        public function getPassword()
        {
            return $this->pass_bd;
        }
}
?>