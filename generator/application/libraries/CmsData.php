<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CmsData {

    private $dbTable;
    private $primaryKey;
    private $columns;
    public $data;
    private $foraignKeys;
    private $fileKeys;
    private $adapter;

    public function init($Table = null, $database, $columns = null, $primary = null, $foreigns = array(), $adapter = CRUD_adapter) {
        include_once("form.php");
        $this->dbTable = $Table;
        $this->primaryKey = $primary;
        $this->foraignKeys = $foreigns;
        $this->data = array();
        $this->columns = $columns;
        $this->adapter = $adapter;
        $this->selectAllAdmin();
        $this->selectAllView();
        $this->selectOneView();
        $this->deleteFormAdmin();
        $this->insertForm();
        $this->updateFormAdmin();
        $this->createController();
        $this->createModel();
        $this->createAPI();
        return "ready";
    }

    /* beta */

    public function insertForm() {
        $insertForm = '
		<?php
		if(isset(##_SESSION) AND !empty(##_SESSION))
		{	
		?>
		';
        $myform = new form;

        $myform->createForm("<?php ##segments = array('" . $this->dbTable . "', 'create'); echo site_url(##segments); ?>", "post", $this->dbTable);

        /*
         * 
         */
        if ($this->exists_columns()) {
            foreach ($this->columns as $key) {
                if ($key == $this->primaryKey) {
                    
                } else {
                    if ($this->adapter["type"][$key] == "text") {
                        $myform->inputTextForm("", $key);
                    }
                    if ($this->adapter["type"][$key] == "hidden") {
                        $myform->inputHiddenForm("", $key);
                    }
                    if ($this->adapter["type"][$key] == "checkBox") {
                        $myform->inputCheckBox("", $key);
                    }
                    if ($this->adapter["type"][$key] == "textBox") {
                        $myform->textBox("", $key, 55, 5);
                    }
                    if ($this->adapter["type"][$key] == "password") {
                        $myform->inputPasswordForm("", $key);
                    }
                    if ($this->adapter["type"][$key] == "file") {
                        $myform->inputFileForm($key);
                        $this->fileKeys[] = $key;
                    }
                    if ($this->adapter["type"][$key] == "datepicker") {
                        $myform->datepickerForm($key);
                        $this->fileKeys[] = $key;
                    }
                    if ($this->adapter["type"][$key] == "null") {
                        
                    }
                }
            }/*             * *** */
        }
        /*
         * INSERTS THE DROPDOWN LIST WITH THE FORAIGN TABLES
         */
        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys AS $fkey) {

                $myform->dropdown_list($fkey["primary_key"], $fkey["table"], $fkey["show"], $fkey["foraign_key"]);
            }
        }
        /*
         * 
         */
        $myform->endForm("enviar");

        $insertForm.=$myform->getForm();
        $insertForm.='<?php
		}
		else
		{ redirect("/login/","refresh");}
		echo ##client;
		?>
		';
        $insertForm = str_replace("##", '$', $insertForm);
        $this->data[$this->dbTable]["insertForm"] = $insertForm;
    }

    public function selectAllView() {

        $selectAllView = '';
        $selectAllView.='<article class="resultados">
		<?php
		
		if(!empty(##' . $this->dbTable . ') and isset(##' . $this->dbTable . ')){
		foreach (##' . $this->dbTable . ' as ##dato)
		{?><section id="resultado">
			';
        /*
         * 
         * 
         */
        if ($this->exists_primary()) {
            $selectAllView.='
                    <br/><a href="<?php ##segments = array("' . $this->dbTable . '", "find",##dato["' . $this->primaryKey . '"]); echo site_url(##segments); ?>">ver</a> ';
        }

        if ($this->exists_columns()) {


            foreach ($this->columns as $key) {



                $selectAllView.='<?php echo ##dato["' . $key . '"];?> 
                            ';
            }
        }

        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $selectAllView.='<?php echo ##dato["' . $fkey["show"] . '"];?>';
            }
        }
        $selectAllView.='
		</section>
		<?php
		}
		}
		?>
		</article>
		';
        $selectAllView = str_replace("%%", '"', $selectAllView);
        $selectAllView = str_replace("##", '$', $selectAllView);
        $this->data[$this->dbTable]["selectAllView"] = $selectAllView;
    }

    public function selectOneView() {
        $selectOneView = '<?php
		
		if(!empty(##' . $this->dbTable . ') and isset(##' . $this->dbTable . '))
		{
		?>';
        $selectOneView.='<article class="resultados">
		<section id="resultado">
			';
        /*
         * 
         * 
         */
        if ($this->exists_columns()) {
            foreach ($this->columns as $key) {
                if ($key == $this->primaryKey) {
                    
                } else {
                    $selectOneView.='<?php echo ##' . $this->dbTable . '[0]["' . $key . '"];?> 
                            ';
                }
            }
        }

        if ($this->exists_foreigns()) {
            //$myform->dropdown_list($fkey["primary_key"],$fkey["table"],$fkey["show"],$fkey["foraign_key"]);
            foreach ($this->foraignKeys as $fkey) {
                $selectOneView.='<?php echo ##' . $this->dbTable . '[0]["' . $fkey["show"] . '"];?> ';
            }
        }
        $selectOneView.='
		</section>
		</article>
		';
        $selectOneView.='
		<?php
		}
		?>';
        $selectOneView = str_replace("%%", '"', $selectOneView);
        $selectOneView = str_replace("##", '$', $selectOneView);
        $this->data[$this->dbTable]["selectOneView"] = $selectOneView;
    }

    public function selectAllAdmin() {
        $selectAllAdmin = '<?php
		
		?>';
        $selectAllAdmin.='<article class="nuevo">
		<a href="<?php ##segments = array("' . $this->dbTable . '", "create"); echo site_url(##segments); ?>">agregar</a>
		</article>
		';
        $selectAllAdmin.='<article class="resultados">
		<?php
		if(!empty(##_SESSION) and isset(##_SESSION))
		{
		if(!empty(##' . $this->dbTable . ') and isset(##' . $this->dbTable . ')){';
        
          if ($this->exists_columns()) {
        $selectAllAdmin.='?> <section class="table-responsive" id="resultado">
                <table class="table table-hover" id="pagination">
                    <thead>'; 
           $selectAllAdmin.='<th></th>'; 
           foreach ($this->columns as $key) {

                $selectAllAdmin.='<th>'. $key . '</th> 
                            ';
            }
            
            
              if ($this->exists_foreigns()) {
                foreach ($this->foraignKeys as $fkey) {
                    $selectAllAdmin.='<th>' . $fkey["show"] . '</th>';
                }
            }
        
            
          
            /**
             *              */
               $selectAllAdmin.=' </thead>
                    <tbody> ';
        /*
         * 
         * 
         */
           }
        
	  $selectAllAdmin.=	'<?php  foreach (##' . $this->dbTable . ' as ##dato)
		{?><tr>
			';
         
        if ($this->exists_primary()) {
            $selectAllAdmin.='<td>
                            <br/>
                            <a href="<?php ##segments = array("' . $this->dbTable . '", "update",md5(##dato["' . $this->primaryKey . '"])); echo site_url(##segments); ?>">actualizar</a> 
                            <a href="<?php ##segments = array("' . $this->dbTable . '", "delete",md5(##dato["' . $this->primaryKey . '"])); echo site_url(##segments); ?>">Eliminar</a> </td>';
        }
        if ($this->exists_columns()) {
            
            /*
             * cambios que permiten trabajar con data tables.js 
             * 
             *  */
              
         
            foreach ($this->columns as $key) {

                $selectAllAdmin.='<td><?php echo ##dato["' . $key . '"];?></td> 
                            ';
            }
            if ($this->exists_foreigns()) {
                foreach ($this->foraignKeys as $fkey) {
                    $selectAllAdmin.='<td><?php echo ##dato["' . $fkey["show"] . '"];?></td>';
                }
            }
        }
        $selectAllAdmin.='</tr>
        
		
		<?php
		}
		}
                ?>
     </tbody>
        </table>
    </section>                
<?php
		}
		else
		{
		redirect("/login/","refresh");
		}
		?>
		</article>
		';
        $selectAllAdmin = str_replace("%%", '"', $selectAllAdmin);
        $selectAllAdmin = str_replace("##", '$', $selectAllAdmin);
        $this->data[$this->dbTable]["selectAllAdmin"] = $selectAllAdmin;
    }

    /* regresa despues de crear un login de default '.$this->dbTable.' */

    public function updateFormAdmin() {
        $updateFormAdmin = '<?php
		
		if(isset(##_SESSION) AND !empty(##_SESSION))
		{	
			if(!empty(##' . $this->dbTable . ') and isset(##' . $this->dbTable . ')){
		?>
		';
        $myform = new form;
        $myform->clearForm();
        $myform->createForm("<?php ##segments = array('" . $this->dbTable . "', 'update',##id); echo site_url(##segments); ?>", "post", $this->dbTable);
        /*
         * 
         * 
         */
        if ($this->exists_columns()) {
            foreach ($this->columns as $key) {
                $postkey = "<&&php echo  ##" . $this->dbTable . "[0]['" . $key . "']; &&>";
                if ($key == $this->primaryKey) {
                    $myform->inputHiddenForm($postkey, $key);
                } else {
                    if ($this->adapter["type"][$key] == "text") {
                        $myform->inputTextForm($postkey, $key);
                    }
                    if ($this->adapter["type"][$key] == "hidden") {
                        $myform->inputHiddenForm($postkey, $key);
                    }
                    if ($this->adapter["type"][$key] == "checkBox") {
                        $myform->inputCheckBox($postkey, $key);
                    }
                    if ($this->adapter["type"][$key] == "textBox") {
                        $myform->textBox($postkey, $key, 55, 5);
                    }
                    if ($this->adapter["type"][$key] == "password") {
                        $myform->inputPasswordForm($postkey, $key);
                    }
                    if ($this->adapter["type"][$key] == "file") {
                        $myform->inputFileForm($key);
                    }
                    if ($this->adapter["type"][$key] == "datepicker") {
                        $myform->datepickerForm($key, $postkey);
                        $this->fileKeys[] = $key;
                    }
                    if ($this->adapter["type"][$key] == "null") {
                        
                    }
                }
            }
        }

        /*
         * INSERTS THE DROPDOWN LIST WITH THE FORAIGN TABLES
         */
        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys AS $fkey) {
/*hay que acomodar la tabla en el criterio del dropdown list*/
                $myform->dropdown_list($fkey["primary_key"], $fkey["table"], $fkey["show"], $fkey["foraign_key"],' <?php if( ##value["'.$fkey["show"].'"]==##'.$this->dbTable.'[0]["'.$fkey["show"].'"]) echo " selected";  ?>');
            }
        }
        /*
         * 
         */



        $myform->endForm("enviar");
        $FormAdmin = $myform->getForm();
        $updateFormAdmin.=$FormAdmin;
        $updateFormAdmin.='
		<?php
			}
		}else{redirect("/login/","refresh");}
		echo ##client;
		?>
		';
        $updateFormAdmin = str_replace("##", '$', $updateFormAdmin);
        $updateFormAdmin = str_replace("&&", '?', $updateFormAdmin);

        $this->data[$this->dbTable]["updateFormAdmin"] = $updateFormAdmin;
    }

    public function deleteFormAdmin() {
        $myForm = new form;
        $opciones = array("FALSE", "TRUE");
        $deleteForm = '
		<?php
		
		if(isset(##_SESSION) AND !empty(##_SESSION))
		{
		?>
		';
        $myForm->clearForm();
        $myForm->createForm("<?php ##segments = array('" . $this->dbTable . "', 'delete',##id); echo site_url(##segments); ?>", "post", $this->dbTable);
        $myForm->select($opciones, "confirmar");
        $myForm->endForm("eliminar");
        $deleteForm.=$myForm->getForm();
        $deleteForm.='<?php
		}
		else
		{
		redirect("/login/","refresh");
		}
		?>';
        $deleteForm = str_replace(">TRUE<", '>si<', $deleteForm);
        $deleteForm = str_replace(">FALSE<", '>no<', $deleteForm);
        $deleteForm = str_replace("##", '$', $deleteForm);
        $deleteForm = str_replace("&&", '?', $deleteForm);

        $this->data[$this->dbTable]["deleteForm"] = $deleteForm;
    }
/*
 *  FUNCTION Controller
 * 
 *  
 */
    public function createController() {
        $controller = "";
        $controller = '<?php // created ' . date("d-m-y h:i:s") . '
if (!defined("BASEPATH")) die();
class ' . ucfirst($this->dbTable) . ' extends CI_Controller {

   public function index()
	{
      ##this->load->view("templates/header");
	  ##this->load->view("templates/aside");
      ##this->load->view("templates/footer");
	}
   public function view()
   {
   ';
        $controller.='
	##this->load->model("' . $this->dbTable . '_model");
	##this->config->load("cms");
    /**/##data["item"]=##this->config->item("user_nav");
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->get();
	//##data["keys"]=array_keys(##data["' . $this->dbTable . '"][0]);
	##this->load->view("templates/header");
	##this->load->view("templates/menubar",##data);
	##this->load->view("users/' . $this->dbTable . '/show_' . $this->dbTable . '",##data);
    ##this->load->view("templates/footer");
   }
    public function admin()
   {
   ';
        $controller.='
	##this->load->model("' . $this->dbTable . '_model");
	/**/##this->config->load("cms");
    /**/##data["item"]=$this->config->item("admin_nav");
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->get();
	//##data["keys"]=array_keys(##data["' . $this->dbTable . '"][0]);
	##this->load->view("templates/header");
	/**/##this->load->view("templates/adminmenu",$data);
	##this->load->view("admin/' . $this->dbTable . '/show_' . $this->dbTable . '",##data);
    ##this->load->view("templates/footer");
   }
   public function find()
   {
   ';
        $controller.='
	##this->load->model("' . $this->dbTable . '_model");
	##this->config->load("cms");
    /**/##data["item"]=##this->config->item("user_nav");
	##data["id"]=##this->uri->segment(3);
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->getOne(##data["id"]);
    ##this->load->view("templates/header");
	##this->load->view("templates/menubar",##data);
	##this->load->view("users/' . $this->dbTable . '/find_' . $this->dbTable . '",##data);
    ##this->load->view("templates/footer");
   }
   public function create()
   {
   ##this->load->model("' . $this->dbTable . '_model");
   ';

        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $controller.='
    $data["' . $fkey["table"] . '"]=$this->' . $this->dbTable . '_model->get_' . $fkey["table"] . '_' . $fkey["show"] . '();
    ';
            }
        }


        $controller.='
    ##data["client"]=##this->client();
	/**/##this->config->load("cms");
    /**/##data["item"]=$this->config->item("admin_nav");
    
	if(##this->server() AND (isset(##_POST) AND !empty(##_POST)))
	{
	##this->' . $this->dbTable . '_model->add();
        redirect("/' . $this->dbTable . '/admin/", "refresh");
	}
    ##this->load->view("templates/header");
	/**/##this->load->view("templates/adminmenu",##data);
	##this->load->view("admin/' . $this->dbTable . '/add_' . $this->dbTable . '",##data);
    ##this->load->view("templates/footer");
   }
      public function update()
   {
   ##this->load->model("' . $this->dbTable . '_model");
   ';

        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $controller.='
    $data["' . $fkey["table"] . '"]=$this->' . $this->dbTable . '_model->get_' . $fkey["table"] . '_' . $fkey["show"] . '();
    ';
            }
        }



        $controller.='
    ##data["client"]=##this->client();
    ##data["id"]=##this->uri->segment(3);
	/**/##this->config->load("cms");
    /**/##data["item"]=$this->config->item("admin_nav");
	if(##this->server() AND (isset(##data["id"]) and ##data["id"]!="") and (isset($_POST) and !empty(##_POST))){
	##this->' . $this->dbTable . '_model->update(##this->uri->rsegment(3));
            redirect("/' . $this->dbTable . '/admin/", "refresh");
	}
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->getOne(##data["id"]);
    ##this->load->view("templates/header");
	/**/##this->load->view("templates/adminmenu",##data);
	##this->load->view("admin/' . $this->dbTable . '/edit_' . $this->dbTable . '",##data);
    ##this->load->view("templates/footer");
   }
   public function delete()
   {
	##data["id"]=##this->uri->segment(3);
	##this->load->model("' . $this->dbTable . '_model");
	/**/##this->config->load("cms");
    /**/##data["item"]=$this->config->item("admin_nav");
    if((isset(##data["id"]) and ##data["id"] != "") and !empty(##_POST)){
        if(##_POST["confirmar"]==="TRUE"){
	##this->' . $this->dbTable . '_model->delete(##data["id"]);
        redirect("/' . $this->dbTable . '/admin/TRUE/", "refresh");
	}
	else
	{
	redirect("/' . $this->dbTable . '/admin/FALSE/", "refresh");
	}
      }
	
    ##this->load->view("templates/header");
	/**/##this->load->view("templates/adminmenu",##data);
	##this->load->view("admin/' . $this->dbTable . '/delete_' . $this->dbTable . '",##data);
    ##this->load->view("templates/footer");
   }
	public function server()
	{
	if(isset(##_POST) and !empty(##_POST))
	{
	$this->load->library("form_validation");
	';
        /*
         * 
         *  valida del lado del servidor
         * 
         */
        if ($this->exists_columns()) {
            foreach ($this->columns as $key) {

                if ($key == $this->primaryKey) {                    
                } else {
                    // $idName=explode("_validate", $key);
                    $idName[0] = $key;
                    switch ($this->adapter["validation"][$key]) {
                        case "null":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|xss_clean");
                                ';
                            break;
                        case "notNull":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean");
					';
                            break;
                        case "email":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|valid_email");
					';
                            break;
                        case "celphone":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|numeric|exact_length[10]");
					';
                            break;
                        case "telephone":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|numeric|exact_length[8]");
					';
                            break;
                        case "date":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|exact_length[10]");
					';
                            break;
                        case "postalCode":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|exact_length[5]");
					';
                            break;
                        case "ipv4":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|valid_ip");
					';
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        /*
         * this create the foreign key validation we need validate this because it can make error
         * 
         */
         if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $controller.='##this->form_validation->set_rules("' . $fkey["foraign_key"] . '","' . $fkey["foraign_key"] . '","trim|required|xss_clean|integer");';
                $controller.='
    //$data["' . $fkey["table"] . '"]=$this->' . $this->dbTable . '_model->get_' . $fkey["table"] . '_' . $fkey["show"] . '();
    ';
            }
        }
        
        /*
         * 
         *          
         */
        //##this->validate->inputEmpty("nombre");
        $controller.='
	return ##this->form_validation->run();
	}
        else
        {
        return FALSE;
        }
	}
	public function client()
	{
	##this->load->library("validate");
	##this->validate->setId("' . $this->dbTable . '");
	';
        /*
         * 
         * 
         * 
         */
        if ($this->exists_columns()) {
            foreach ($this->columns as $key) {
                if ($key == $this->primaryKey) {
                    
                } else {
                    //$idName=explode("_validate", $key);
                    $idName[0] = $key;
                    switch ($this->adapter["validation"][$key]) {
                        case "null":
                            break;
                        case "notNull":
                            $controller.='##this->validate->clientInputEmpty("' . $idName[0] . '");
				';
                            break;
                        case "email":
                            $controller.='##this->validate->clientMail("' . $idName[0] . '");
				';
                            break;
                        case "celphone":
                            $controller.='##this->validate->clientCelphone("' . $idName[0] . '");
				';
                            break;
                        case "telephone":
                            $controller.='##this->validate->clientTelephone("' . $idName[0] . '");
				';
                            break;
                        case "date":
                            $controller.='##this->validate->clientDateFormat("' . $idName[0] . '");
				';
                            break;
                        case "postalCode":
                            $controller.='##this->validate->clientPostalCode("' . $idName[0] . '");
				';
                            break;
                        case "ipv4":
                            $controller.='##this->validate->clientIpv4("' . $idName[0] . '");
				';
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        $controller.='
	return ##this->validate->getJscript();
	}';
        $controller.='
}
';
        $controller = str_replace("%%", '"', $controller);
        $controller = str_replace("##", '$', $controller);
        $this->data[$this->dbTable]["controller"] = $controller;
    }
    /*
 * 
 * 
 *       Create API
 * 
 *  
 */
    public function createAPI() {
        $controller = "";
        $controller = '<?php // created ' . date("d-m-y h:i:s") . '
            if (!defined("BASEPATH")) die();
include_once APPPATH . "libraries/REST_Controller.php";
class ' . ucfirst($this->dbTable) . '_API extends REST_Controller {

   public function show_all_get()
   {
   ';
        $controller.='
	##this->load->model("' . $this->dbTable . '_model");	
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->get();
        ##this->response($data, 200);
   }
    
   public function find_by_id_get()
   {
   ';
        $controller.='
	##this->load->model("' . $this->dbTable . '_model");
	
	##data["id"]=##this->get("id", TRUE);
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->getOne(##data["id"]);
        ##this->response($data, 200);
   
   }
   public function create_post()
   {
   ##this->load->model("' . $this->dbTable . '_model");
       
	if(##this->server() AND (isset(##_POST) AND !empty(##_POST)))
	{
	##this->' . $this->dbTable . '_model->add();
    	}else{
   ';
/*
 *  En caso de usar como formulario enviara la informacion de las llaves foraneas para contruir datos de seleccion 
 *  y si se reenvia la informacion con datos los recibira y agregara a la BD
 */
        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $controller.='
    $data["' . $fkey["table"] . '"]=$this->' . $this->dbTable . '_model->get_' . $fkey["table"] . '_' . $fkey["show"] . '();
    ';
            }
        }


        $controller.='
    	}
    ##this->response($data, 200);
   }
      public function edit_post()
   {
   ##this->load->model("' . $this->dbTable . '_model");
   ';

        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $controller.='
    $data["' . $fkey["table"] . '"]=$this->' . $this->dbTable . '_model->get_' . $fkey["table"] . '_' . $fkey["show"] . '();
    ';
            }
        }



        $controller.='    
    ##data["id"]=##this->post("id",TRUE);
	
	if(##this->server() AND (isset(##_data["id"]) and ##_data["id"]>0) and (isset($_POST) and !empty(##_POST))){
	##this->' . $this->dbTable . '_model->update(##data["id"]);
        
	}
	##data["' . $this->dbTable . '"]=##this->' . $this->dbTable . '_model->getOne(##data["id"]);
    ##this->response($data, 200);
   }
   public function delete_post()
   {
	##data["id"]=##this->post("id");
	##this->load->model("' . $this->dbTable . '_model");
	##data["validation"]=(isset(##data["id"]) and ##data["id"]>0) and !empty(##_POST);
    if(##data["validation"]){
        if(##_POST["confirmar"]==="TRUE"){
	##this->' . $this->dbTable . '_model->delete(##data["id"]);        
	}
        ##this->response($data,200);
      }	
   }
	public function server()
	{
	if(isset(##_POST) and !empty(##_POST))
	{
	$this->load->library("form_validation");
	';
        /*
         * 
         *  valida del lado del servidor
         * 
         */
        if ($this->exists_columns()) {
            foreach ($this->columns as $key) {

                if ($key == $this->primaryKey) {                    
                } else {
                    // $idName=explode("_validate", $key);
                    $idName[0] = $key;
                    switch ($this->adapter["validation"][$key]) {
                        case "null":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|xss_clean");
                                ';
                            break;
                        case "notNull":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean");
					';
                            break;
                        case "email":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|valid_email");
					';
                            break;
                        case "celphone":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|numeric|exact_length[10]");
					';
                            break;
                        case "telephone":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|numeric|exact_length[8]");
					';
                            break;
                        case "date":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|exact_length[10]");
					';
                            break;
                        case "postalCode":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|exact_length[5]");
					';
                            break;
                        case "ipv4":
                            $controller.='##this->form_validation->set_rules("' . $idName[0] . '","' . $idName[0] . '","trim|required|xss_clean|valid_ip");
					';
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        //##this->validate->inputEmpty("nombre");
        /*
         * foreign key validation 
         * 
         */
         if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                $controller.='##this->form_validation->set_rules("' . $fkey["foraign_key"] . '","' . $fkey["foraign_key"] . '","trim|required|xss_clean|integer");';
    //            $controller.='
    //$data["' . $fkey["table"] . '"]=$this->' . $this->dbTable . '_model->get_' . $fkey["table"] . '_' . $fkey["show"] . '();
    //';
            }
        }
        /*
         * 
         * 
         */
        $controller.='
	return ##this->form_validation->run();
	}else
        {
        return FALSE;
        }
	}
	';
        $controller.='
}
';
        $controller = str_replace("%%", '"', $controller);
        $controller = str_replace("##", '$', $controller);
        $this->data[$this->dbTable]["controller_API"] = $controller;
    }

        
        
        
        
  /*
   * 
   * 
   * end REST API code
   *    
   */      


    /*
     * 
     * createModel
     *      
     */

    public function createModel() {
        $model = '';
        $model.='
<?php if (!defined("BASEPATH")) die();
class ' . ucfirst($this->dbTable) . '_model extends CI_Model {

   public function __construct()
	{
	  parent::__construct();
      ##this->load->database();
	}
   public function get()
   {';
        $model.=$this->create_select_items();
        if ($this->exists_foreigns()) {
            //$myform->dropdown_list($fkey["primary_key"],$fkey["table"],$fkey["show"],$fkey["foraign_key"]);
            foreach ($this->foraignKeys as $fkey) {

                $model.='
                $this->db->join("' . $fkey["table"] . '","' . $fkey["table"] . '.' . $fkey["primary_key"] . '=' . $this->dbTable . '.' . $fkey["foraign_key"] . '","INNER");';
            }
        }
        $model.='
	 ##query = ##this->db->get("' . $this->dbTable . '");
	 return ##query->result_array();
   }
   public function getOne(##id)
   {
   ';
        $model.=$this->create_select_items();
        //$myform->dropdown_list($fkey["primary_key"],$fkey["table"],$fkey["show"],$fkey["foraign_key"]);
        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {

                $model.='
                $this->db->join("' . $fkey["table"] . '","' . $fkey["table"] . '.' . $fkey["primary_key"] . '=' . $this->dbTable . '.' . $fkey["foraign_key"] . '","INNER");';
            }
        }
        $model.='
	 ##this->db->where("md5(' . $this->dbTable . '.' . $this->primaryKey . ')",##id);
	 ##this->db->limit(1);
	 ##query = ##this->db->get("' . $this->dbTable . '");
	 return ##query->result_array();
   }
   function add()
   {
   /*we change this if you don´t need work with webservice please uncoment this lines*/
	//if(isset(##_POST) and !empty(##_POST))
	//	{
			##data = ##_POST;
			##query =##this->db->insert("' . $this->dbTable . '",##data);
			//redirect("/' . $this->dbTable . '/admin/", "refresh");
	//	}
   }
   function update(##id)
   {
   //if((isset(##id) and ##id>0) and (isset($_POST) and !empty(##_POST)))
    //{
	##data =##_POST;
	##this->db->where( "md5(' . $this->primaryKey . ')",##id);
	##query =##this->db->update("' . $this->dbTable . '",##data);
	//redirect("/' . $this->dbTable . '/admin/", "refresh");
	//}
   }
   
   public function delete(##id)
   {
   //if(isset(##id) and ##id>0 and !empty(##_POST))
    //{
	//if(##_POST["confirmar"]==="TRUE"){
	##this->db->where("md5(' . $this->primaryKey . ')",##id);
	##query =##this->db->delete("' . $this->dbTable . '");
	//redirect("/' . $this->dbTable . '/admin/TRUE/", "refresh");
	//}
	//else
	//{
	//redirect("/' . $this->dbTable . '/admin/FALSE/", "refresh");
	//}
	//}
   }
   ';

        if ($this->exists_foreigns()) {
//$myform->dropdown_list($fkey["primary_key"],$fkey["table"],$fkey["show"],$fkey["foraign_key"]);
            foreach ($this->foraignKeys as $fkey) {
                $model.=
                        '
        public function get_' . $fkey["table"] . '_' . $fkey["show"] . '()';
                $model.='
       {
             ##this->db->select("' . $fkey["show"] . ',' . $fkey["primary_key"] . '");
             ##query = $this->db->get("' . $fkey["table"] . '");
                  return $query->result_array();
       }';
            }
        }
        
        /*
         * get by methods
         * 
         */
        if ($this->exists_columns()) {

            foreach ($this->columns as $col) {
                $model.='
     public function get_by_' . $col . '(##' . $col . ' = NULL)
     {
         $this->db->like("' . $col . '",##' . $col . ');
         $query = $this->db->get("' . $this->dbTable . '");
         return $query->result_array();
     }';
            }            
            /*
             * get by all methods
             * 
             *              
             */
               $model.='
     public function get_by_' . $this->dbTable . '_fields(##' . $this->dbTable . ' = NULL)
     {';
            
            foreach ($this->columns as $col) {
        $model.='     
         $this->db->or_like("' . $col . '",##' . $this->dbTable . ');
         ';
            }
      $model.='$query = $this->db->get("' . $this->dbTable . '");
         return $query->result_array();
     }';
        }
        /*
         * 
         * 
         */
        
        
        $model.=
                '}
';

        $model = str_replace("%%", '"', $model);
        $model = str_replace("##", '$', $model);
        $this->data[$this->dbTable]["model"] = $model;
    }
/*
 * 
 *End createModel 
 *  
 */
    private function exists_primary() {
        $isset = (isset($this->primaryKey) && $this->primaryKey != "");
        return $isset;
    }

    private function exists_columns() {
        $isset = isset($this->columns);
        if ($isset) {

            return $is_array = (is_array($this->columns) && !empty($this->columns));
        } else {
            return FALSE;
        }
    }

    private function exists_foreigns() {
        $isset = isset($this->foraignKeys);
        if ($isset) {
            return $is_array = ( is_array($this->foraignKeys) && !empty($this->foraignKeys) );
        } else {
            return FALSE;
        }
    }

    //$myform->dropdown_list($fkey["primary_key"],$fkey["table"],$fkey["show"],$fkey["foraign_key"]);
    private function create_select_items() {
        $select = '##this->db->select("*';
        if ($this->exists_primary()) {
            $select.=',' . $this->dbTable . '.' . $this->primaryKey;
        }
        if ($this->exists_columns()) {
            foreach ($this->columns as $col) {
                $select.="," . $this->dbTable . "." . $col;
            }
        }
        if ($this->exists_foreigns()) {
            foreach ($this->foraignKeys as $fkey) {
                //$select.=",".$fkey["table"].'.'.$fkey["primary_key"].' as ';
                $select.="," . $fkey["table"] . '.' . $fkey["show"];
            }
        }
        $select.='");';
        return str_replace("*,", "", $select);
    }

    /**/
    /*
     * por implementar
     */
    /* setters */

    public function setKeys($array) {
        $this->columns = $array;
    }

    /* getters */

    public function getKeys() {
        return $this->columns;
    }

    public function getDbTable() {
        return $this->dbTable;
    }

    public function getData() {
        return $this->data;
    }

}
