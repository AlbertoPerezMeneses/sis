<?php
class Form
{
	private $form;
	private $js;
	function __costruct()
	{
		$this->form="";
		$this->js="";
	}
	public function createForm($action,$method,$id)
	{
		$this->form.='<article class="'.$id.' col-8 col-sm-12 col-lg-4 form-group">
		<section>';
		$this->form.='<form accept-charset="utf-8"  action="'.$action.'" method="'.$method.'" id="'.$id.'" class="form-horizontal">
		';
	}	
	public function inputTextForm($value,$name)
	{
            $this->form.='<section class="form-name">';
            $this->form.='<label>'.$name.'</label>';
            $this->form.='</section>';
            $this->form.='<section class="form-item">';
            $this->form.='<input type="text" value="'.$value.'" name="'.$name.'" id="'.$name.'" class="form-control"/>';
            $this->form.='</section>';
		
	}	
	public function inputCheckBox($value,$name)
	{   
		$this->form.='<input type="checkbox" value="'.$value.'" name="'.$name.'" id="'.$name.'" class="form-control" />
		';	
		
	}
	public function inputHiddenForm($value,$name)
	{
		$this->form.='<input type="hidden" name="'.$name.'" value="'.$value.'" id="'.$name.'"  class="form-control"/>
		';
	}
	public function textBox($value,$name,$cols,$rows)
	{
            $this->form.='<section class="form-name">';
            $this->form.='<label>'.$name.'</label>';
            $this->form.='</section>';
            $this->form.='<section class="form-item">';
            $this->form.='<textarea name="'.$name.'" cols="'.$cols.'" rows="'.$rows.'" id="'.$name.'" class="form-control">'.$value.'</textarea>
		';	
            $this->form.='</section>';
		
		
		
	}
	public function select($arrayValue,$name)
	{
            
            $this->form.='<section class="form-name">';
            $this->form.='<label>'.$name.'</label>';
            $this->form.='</section>';
            $this->form.='<section class="form-item">';
		$this->form.='<select name="'.$name.'" id="'.$name.' " class="form-control">
	';
		foreach ($arrayValue as $value)
		{
			$this->form.='<option value="'.$value.'">'.$value.'</option>
			';
		}
	$this->form.='</select>
	';
        $this->form.='</section>';
	
	}
        
        public function dropdown_list($pk,$table,$show,$fk,$selectedCritearia = "")
        {
            $this->form.='<section class="form-name">';
            $this->form.='<label>'.$table.'</label>';
            $this->form.='</section>';
            $this->form.='<section class="form-item">';
		$this->form.='<select name="'.$fk.'" id="'.$fk.' " class="form-control">
	';      
                  $this->form.='
                  <?php
                    foreach (##'.$table.' as ##value)
                    { ?>';
                            $this->form.='<option value="<?php echo ##value["'.$pk.'"]; ?>"  '.$selectedCritearia.'><?php echo ##value["'.$show.'"];?> </option>
                            <?php 
                            ';
                       
                    $this->form.='} ?>';
                
	$this->form.='</select>
	';
        $this->form.='</section>';    
        }


        public function inputFileForm($name)
	{
            $this->form.='<section class="form-name">';
            $this->form.='</section>';
            $this->form.='<section class="form-item">';
            $this->form.='</section>';
	$this->form=str_replace("<form  action",'<form  enctype="multipart/form-data" accept-charset="utf-8" action',$this->form);
	$this->form.='<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
   <input name="'.$name.'" type="file" id="'.$name.'"  class="form-control" />
	';
	}
        public function multiCheckBox($name = null,$values = array())
        {
            foreach ($values as $value){
            $this->form.='<section class="form-name">';
            $this->form.='<label>'.$value.'</label>';
            $this->form.='</section>';
            $this->form.='<section class="checkbox">';
            $this->form.='<input type="checkbox" name="'.$name.'" id="'.$name.'"  value="'.$value.'"> ';
            $this->form.='</section>';
            }
            
        }
	public function datepickerForm($name = null,$value= null)
	{
            $this->form.='<section class="form-name">';
            $this->form.='</section>';
            
	$this->form.='<label>'.$name.'</label>';
        $this->form.='<section class="form-item">';
		$this->form.='<input type="text" value="'.$value.'" name="'.$name.'" id="'.$name.'" class="form-control" style="cursor:auto" readonly/>
		';
        $this->form.='</section>';
	$this->js.='$(function() {$( "#'.$name.'" ).datepicker();});';
	}
	public function endForm($submitValue)
	{
		$this->form.='<input type="submit" value="'.$submitValue.'" class="form-control" />
		';
		$this->form.='<br/>
		';
		$this->form.='</form>
			</section>
		</article>';
	}
	public function getForm()
	{
	if(!empty($this->js) and isset($this->js))
	{
		$this->form.="<script>".$this->js."</script>";
	}
		return($this->form);
	}
	public function clearForm()
	{
		$this->form="";
	}
}

?>
