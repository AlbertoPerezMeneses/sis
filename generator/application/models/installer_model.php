<?php
if (!defined('BASEPATH')) die();
class installer_model extends CI_Model{
    
    var $set_database;
    var $status_db;
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
        $this->load->dbutil();
        $this->set_database="";
        $this->status_db=FALSE;
    }
    public function set_database($database = NULL)
    {        
        $this->status_db=TRUE;
        $this->set_database=$database;
        $db['database'] = $database;
        $this->load->database($db);        
    }
    public function create_login()
    {
        $SQL='CREATE TABLE IF NOT EXISTS `'.$this->set_database.'`.`robotoit_user` (
	`id_user` INT(9) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(45) NOT NULL,
	`user_email` VARCHAR(45) NOT NULL,
	`user_password` VARCHAR(256) NOT NULL,
	PRIMARY KEY `id_user` (`id_user`)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;';
        
        
        if($this->dbutil->database_exists($this->set_database))
            {
                if(!$this->table_exists("robotoit_user"))
                { 
                $this->db->query($SQL);
                $this-> insert_admin_user();
                //return $this->db->last_query();
                return $this->table_exists("robotoit_user");
                }
            }
            else
            {
                return FALSE;
            }
    }
    public function drop_login($boolean = FALSE)
    {
       // $SQL_truncate="TRUNCATE ";
        $SQL='DROP TABLE IF EXISTS `'.$this->set_database.'`.`robotoit_user`';
        if($this->dbutil->database_exists($this->set_database))
        {    
            if($boolean)
            {
                    $this->db->query($SQL);
                    return $this->db->last_query();
            }
        }
    }
    private function insert_admin_user()
    {
        $clear_SQL="DELETE FROM `".$this->set_database."`.`robotoit_user` WHERE id_user='1';";
        $SQL="INSERT INTO `".$this->set_database."`.`robotoit_user` 
            (`id_user`, `user_name`, `user_email`, `user_password`)
            VALUES 
            ('1', 'admin', 'admin@localhost', '21232f297a57a5a743894a0e4a801fc3')";
        
            $this->db->trans_start();
            $this->db->query($clear_SQL);
            $this->db->query($SQL);
            $this->db->trans_complete();
            return $this->db->last_query();
    }
    private function table_exists($table)
    {
        $query = $this->db->query("SHOW TABLES FROM ".$this->set_database);
        $data=$query->result_array();
        $exists_table=  array_search($table, $data[0]);
        if($exists_table == FALSE)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
   
}
