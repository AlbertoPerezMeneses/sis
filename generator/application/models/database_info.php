<?php
if (!defined('BASEPATH')) die();

class database_info extends CI_Model
{
    var $table= "";
    var $database="";
    var $primary="";
    var $foreign="";

    function __construct()
        {
            parent::__construct();
                    $this->load->database();
            }
    public function set_database($database = NULL)
    {
    $this->database=$database;	
    }

    public function set_table($table = NULL)
    {
    $this->table = $table;
    }

    public function get_databases()
    {
    $databases = $this->db->query("SHOW DATABASES");
    foreach ($databases->result_array() as $database)
        {
        if($database["Database"] == "information_schema" || $database["Database"] == "performance_schema" || $database["Database"] == "mysql")
        {
        }
        else
        {
            $data[]=$database["Database"];
        }
        
        }
    return $data;
    }
/*
 * query
 * 
 * **/
    public function get_tables()
    {
    $tables = $this->db->query("SHOW TABLES FROM ".$this->database);
    foreach ( $tables->result_array()as $table)
        {
        
            if( $table["Tables_in_".$this->database]== "robotoit_connection" ||  $table["Tables_in_".$this->database] == "robotoit_log_user" || $table["Tables_in_".$this->database] == "robotoit_user")
                {
                
                }
                else
                {
                    $data[]=$table["Tables_in_".$this->database];
                }    
        
        }
        return $data;
    }

    public function get_columns($is_foraign=FALSE)
    {
        $this->foreign["field"]="";
        $this->foreign["relation"]="";
        /**
         * 
         */
        
            $query = $this->db->query("DESCRIBE ".$this->database.".".$this->table);
            $columns = $query->result_array();
        
            foreach ($columns as $key) 
                {
                /* GET THE DEFINED PRIMARY AND FOREIGN KEY PLEASE CHECK MYSQL DEFINE QUERY*/
                if( ( $key["Key"] == "PRI") && ( $key["Extra"] == "auto_increment" ) )
                    {
                        $this->primary=$key["Field"];    
                    }
                if( ( $key["Key"] == "PRI") && ( $key["Extra"] == "" ) || ( $key["Key"] == "MUL" ) )
                    {
                     
                     $this->foreign["field"][]=$key["Field"];
                     $this->foreign["relation"][]=$this->get_relational_column($key["Field"],$is_foraign);
                     
                    }

                    $cols[]=$key["Field"];
                }
            return $cols;
    }
    public function get_primary_key()
    {
        return $this->primary;
    }

    public function get_foreign_keys()
    {
        return $this->foreign;
    }
    /*
     *  se agrego la opcion de elegir el patron de busqueda  
     * @important
     * el patron de busqueda debe ser '_id'
     */
    private function get_relational_column($column,$is_foraign_map=TRUE)
    {           
        if($is_foraign_map){
        $info = explode("_", $column);
        
        }else
        {
            $info = explode("_id", $column);
            $info[1]="id".$info[1];
        }
           return $info;
    }
    
    /*
     * GET THE MAP OF THE TABLE INCLUDING THE TABLES RELATIONATED
     * $database = the database.
     * $table = the table that wish maping
     */
    public function maping($database,$table)
    {
        $map=NULL;
        $this->clear_map();
        $this->set_database($database);
        $this->set_table($table);
        $temp_table=$table;
       $map["tables_from_".$table] = $this->get_columns();
       $map["primary"]=$this->get_primary_key(); 
       $map["foreigns"]=array();
       $map["foreigns"]["field"]=array();
       $map["foreigns"]=$this->get_foreign_keys();
       
       if(is_string($map["primary"]) )
       {
            $search = $map["primary"];
            $columns=$this->remove_item( $this->get_columns() ,$search );
       }
       
       if(is_array($map["foreigns"]["field"])&& !empty($map["foreigns"]["field"]))
       {
         
           foreach ($map["foreigns"]["field"] as $fKey)
               {
               $columns=$this->remove_item($columns, $fKey);
               }
           
           /*
            * aqui hay que obtener la informacion de las tablas relacionadas por llave foranea
            * checar como separas las lleves foraneas ya que la aplicacion junta todas las llaves foraneas
            * de el mapeo
            */
       
           foreach ($map["foreigns"]["relation"] as $relation)
               {
                $this->clear_map();
                $this->set_database($database);
                $this->table=$relation[0];
          
               
                $map["tables_from_".$this->table] = $this->get_columns(FALSE);
                $map["primary_from_".$this->table]=$this->get_primary_key(); 
                $map["foreigns_from_".$this->table]=$this->get_foreign_keys();
                
                /*
                 * quita la llave primaria de las llaves foraneas
                */ 
                
      
                if(is_string($map["primary_from_".$this->table]) )
                {
                $search = $map["primary_from_".$this->table];
                $map["tables_from_".$this->table]=$this->remove_item( $this->get_columns() ,$search );
                }
               
                
               }
              
       }
        $map["tables_from_".$temp_table] =$columns;
       //$map["debug"]=$columns;
       return $map;
    }

    

     private function remove_item($array,$search)
            {           
            $data="";
                foreach ($array as $item)
                {
                            if($item != $search)
                            {
                                    $data[]=$item;
                            }
                }
                return $data;
            }
            
    private function clear_map()
            {
                $this->database="";
                $this->table="";
                $this->primary="";
                $this->foreign="";
            }
}