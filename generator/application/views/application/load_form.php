<?php
$attributes = array('class' => 'form form-group');
$options= array("class"=>"form-control","id"=>"");

$label["column"] = array('class' => 'text-muted');
$label["foreign"] = array('class' => 'text-warning');
echo form_open("crud_factory/crud/".$database);

foreach ($tables as $table)
{
    ?>
<article class="col-sm-12 col-lg-4">
<h1 class ="text-primary text-center"><?php echo $table; ?></h1>  
<!-- aqui inserto la llave primaria -->
<h2 class="text-success"><?php
if(isset($map[$table]["primary"]) && $map[$table]["primary"]!="")
    { 
    echo $map[$table]["primary"];
    echo form_hidden($table."[primary]",$map[$table]["primary"]);
    }
    else
    {
        /*en caso de no existir llave primaria*/
        //echo form_hidden($table."[primary]","NULL");
    }
    //var_dump($map[$table]);
    
?>
</h2>
<!--aqui termina la llave primaria-->
<!-- aqui inserto los campos -->
    <?php
    if( is_array($map[$table]["tables_from_".$table] ) && !empty($map[$table]["tables_from_".$table]))
    {
        foreach ($map[$table]["tables_from_".$table] as $column)
        {
        ?>

<section>
    <?php 
        echo form_label( $column,$column ,$label["column"]);            
        echo select($table."[columns][".$column."][type]",$input); 
        echo select($table."[columns][".$column."][validation]",$validation);                
    ?>
</section>

    <?php
        }
    }
    else
    {
        /*
         * tomando en cuenta que no exista ninguna columna de datos
         */
     //   echo form_hidden($table."[columns][type]","NULL"); 
     //   echo form_hidden($table."[columns][validation]","NULL");                
    }
    /*
     * falta implementar las llaves foraneas 
     * que te muestre en un select la tabla con la que 
     * se realizara el join y mostrar la llave primaria
     */
   // var_dump($map[$table]);  
?>
<!--aqui terminan los campos-->
<!--aqui inician las llaves foraneas-->
<?php
if(is_array($map[$table]["foreigns"]["field"])&& !empty($map[$table]["foreigns"]["field"]))
       {
           $i=0;
           foreach ($map[$table]["foreigns"]["field"] as $fKey)
               {
               
               ?>
 <?php 
 
 echo form_label( $fKey ,$fKey, $label["foreign"]);
 /*
  * $table."[".$column."]
  * revisar porque no me esta dando las tablas correspondientes a la tabla de la  llave foranea
  */
 echo select($table."[relation][".$fKey."][show]",$map[$table]["tables_from_".$map[$table]["foreigns"]["relation"][$i][0]]);  
 echo form_hidden($table."[relation][".$fKey."][table]",$map[$table]["foreigns"]["relation"][$i][0]);
 echo form_hidden($table."[relation][".$fKey."][primary_key]",$map[$table]["foreigns"]["relation"][$i][1]);
 echo form_hidden($table."[relation][".$fKey."][foraign_key]",$fKey);
 //
 //var_dump($map[$table]["foreigns"]["relation"]);
 /*
  * 
  * cheacar ya que esta repitiendo los parametros de las llaves foraneas
  */
 $i++;
 ?>
                 
<?php
           }
       }

?>
<!--aqui terminan las llaves foraneas-->

</article>  

    
<?php
}
?>
<article class="form-inline col-12">
<?php
/*
echo form_label("debug","debug");
echo select("debug", array("false"=>"FALSE","true"=>"TRUE"));
echo form_label("delete","delete");
echo select("delete", array("false"=>"FALSE","true"=>"TRUE"));
 */
echo form_submit(array("class"=>"form-control","value"=>"enviar"));
echo form_close();
//var_dump($map);
?>

</article>
<article class="col-12">
    
    <?php
//var_dump($data,$tables);
    ?>
</article>
         