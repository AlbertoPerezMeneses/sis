<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="mi primer pagina echa con codeignaiter">
   <meta name="keywords" content="">
   <meta name="author" content="alberto perez meneses ">

   <title>CodeIgniter Bootstrap</title>

   <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
   <link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
   <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
   <link href="<?php echo base_url('assets/css/main.css') ?>" rel="stylesheet">
   <link href="<?php echo base_url('assets/css/jquery-ui-1.10.3.custom.min.css') ?>" rel="stylesheet">
   
   <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
   <script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
   <script src="<?php echo base_url('assets/js/jquery-1.7.2.js') ?>"></script>
   <script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.custom.min.js') ?>"></script>
   <script src="<?php echo base_url('assets/js/uiconfig.js') ?>"></script>
   <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
</head>
<body>
<article class="container">
