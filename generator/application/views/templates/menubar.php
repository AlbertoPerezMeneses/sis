<nav class="subnav" style="margin-bottom: 10px;">
   <ul class="nav nav-pills">
      <li class="active" ><a href="<?php echo site_url(); ?>">Inicio</a></li>
      <li class="dropdown">
         <a class="dropdown-toggle" data-toggle="dropdown">Ver<b class="caret"></b></a>
         <ul class="dropdown-menu">
				<?php if(isset($item) and !empty($item))
				{
					foreach ($item as $i){
				?>
               <li><a href="<?php $segments= array($i,"view"); echo site_url($segments)?>"><?php echo $i?></a></li>
			   
			   <?php }
			   }
			   ?>
         </ul>
      </li> 
      <ul class="nav nav-pills pull-right">
	  <?php 
	  session_start();
	  if(isset($_SESSION) and !empty($_SESSION)){ ?>
		 <li><a href="<?php $segments = array("login","close"); echo site_url($segments);?>">Salir</a></li>
		 <?php }else{ ?>
         <li><a href="<?php $segments = array("login"); echo site_url($segments);?>">login</a></li>
		 <?php }?>
      </ul>
   </ul>
</nav>