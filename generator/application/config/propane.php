
<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
/*
 * this option define the driver 
 */
$settings["driver"]="codeigniter";
/*
 * this option define the version of the framework
 */
$settings["version"]="2.1.4";
/*
 * this option create all cruds when is true and 
 * var_dump last table when is false
 */
$settings["create"]=TRUE;
/*
 * this option delete all cruds selected in the form settings
 */
$settings["delete"]=FALSE;
/*
 * this option set the backup mode
 */
$settings["backup"]="none";
/*
 * this load all configurations
 */
$config["propane"]=$settings;
        