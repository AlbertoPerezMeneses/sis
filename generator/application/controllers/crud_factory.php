<?php

if (!defined('BASEPATH'))
    die();

class Crud_factory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("database_info");
        $this->load->config("propane");
    }

    public function index() {
        $data["data"] = $this->database_info->get_databases();
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/databases", $data);
        $this->load->view("templates/footer");
    }

    public function get_db() {
        $data["database"] = $this->uri->segment(3);
        $this->database_info->set_database($data["database"]);
        $data["tables"] = $this->database_info->get_tables();
        $this->load->helper('form');
        //
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/tables", $data);
        $this->load->view("templates/footer");
    }

    public function form_settings() {


        $data["input"] = array("text", "textBox", "hidden", "datepicker", "checkBox", "password", "null", "select", "file");
        $data["validation"] = array("null", "notNull", "email", "celphone", "telephone", "date", "postalCode", "ipv4");
        $data["database"] = $this->uri->segment(3);
        $data["tables"] = $_POST["tables"];

        $this->load->helper('form');
        if (is_array($_POST) && !empty($_POST)) {
            foreach ($data["tables"] as $table) {
                $data["map"][$table] = $this->database_info->maping($data["database"], $table);
            }
        } else {
            
        }

        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/forms", $data);
        $this->load->view("templates/footer");
    }

    public function crud() {
        $database = $this->uri->segment(3);
        $setting = $this->config->item("propane");
        $this->load->library("cmsdata");
        $this->load->library("cms");
        $this->load->library("crud_adapter");
        $item["data"] = $setting;

        if (is_array($_POST) && !empty($_POST)) {
            foreach (array_keys($_POST) as $table) {
                $data[$table] = $this->crud_adapter->init($_POST, $table);
                $this->cmsdata->init($table, $database, $data[$table]["columns"], $data[$table]["primary"], $data[$table]["foreigns"], $data[$table]);
                $data = $this->cmsdata->getData();
                $item["data"] = $this->cms->process($data[$table], $table, $setting["create"],$setting["backup"]);
                if ($setting["delete"])
                    $this->cms->deleteCms();
            }          
            $crud_config=$_POST;
            $crud_config["database"]=$database;
            
            
            $this->cms->add_config("config" . date("y_m_d_h_i_s"), json_encode($crud_config, JSON_PRETTY_PRINT), ".json");
        }
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/index", $item);
        $this->load->view("templates/footer");
    }

    public function config() {

        $this->load->helper('form');
        $this->load->library('propane');
        $setting = $this->config->item("propane");
        $item["propane"] = $setting;

        $item["framework"][] = "yii";
        $item["framework"][] = "codeigniter";
        $item["framework"][] = "zend";


        $item["backup"][] = "self";
        $item["backup"][] = "backup";
        $item["backup"][] = "none";

        /*
         * 
         */

        if (is_array($_POST) && !empty($_POST)) {
            $this->propane->add_Settings($_POST);
            redirect("crud_factory/config/");
        }
        /*
         * 
         */
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/dashboard", $item);
        $this->load->view("templates/footer");
    }
    
    public function load() {
        
        $this->load->helper('form');
        $this->load->helper("file");
                

        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        
        if(is_array($_FILES) && !empty($_FILES))
            {
            
             $data["data"]=  json_decode(read_file($_FILES["schema"]["tmp_name"]),TRUE);   
            if($data["data"]===NULL)
                {
                $data["data"]="can't load eschema";
                
                }
            
            
            $this->load->view("application/index",$data);
            }

        
        $this->load->view("application/create_crud");
        $this->load->view("templates/footer");
        
    }

    
     public function load_form() {
        $data["input"] = array("text", "textBox", "hidden", "datepicker", "checkBox", "password", "null", "select", "file");
        $data["validation"] = array("null", "notNull", "email", "celphone", "telephone", "date", "postalCode", "ipv4");
        $data["database"] = $this->uri->segment(3);
        

        $this->load->helper('form');
        $this->load->helper("file");
        
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
       
        
        if(isset($_FILES) && !empty($_FILES))
            {
            //$data["tables"] = $_POST["tables"];
            $data["schema"]=  json_decode(read_file($_FILES["schema"]["tmp_name"]),TRUE); 
            $data["database"]=$data["schema"]["database"];
            array_pop($data["schema"]);
            $data["tables"]=  array_keys($data["schema"]);
            $info["data"]=$data;
                                    
            foreach ($data["tables"] as $table) {
            $data["map"][$table] = $this->database_info->maping($data["database"], $table);
            
            }
            $this->load->view("application/load_form", $data);
            }
            
        $this->load->view("application/index", $info);
        $this->load->view("templates/footer");
    }
    
}

