<?php
if (!defined('BASEPATH')) die();
class Installer extends CI_Controller{

    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        $this->load->model("database_info");
        $data["data"]=$this->database_info->get_databases();
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("installer/databases",$data);
        $this->load->view("templates/footer");
    }
    
    public function install()
    {
        $database=$this->uri->segment(3); 
        if($database!="")
        {
        $this->load->model("installer_model");      
        $this->installer_model->set_database($database);
        $info["data"]["exists"]=$this->installer_model->create_login();                
        }
        else
        {
            $info["data"]["error"]="no se ha elgido una base de datos";
        }
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/index",$info);
        $this->load->view("templates/footer");
        
    }
    public function uninstall()
    {
        $database=$this->uri->segment(3);
        $status=$this->uri->segment(4);
        $this->load->model("installer_model");
        if($status=="true")
        {
            $this->installer_model->set_database($database);
            $info["data"]=$this->installer_model->drop_login(TRUE);
        }
        $this->load->view("templates/header");
        $this->load->view("templates/main_navigator");
        $this->load->view("application/index",$info);
        $this->load->view("templates/footer");
        
    }
}

