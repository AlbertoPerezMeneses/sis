'use strict';
/* Directives */
var mydirective=angular.module('myApp.directives', []);
        
mydirective.directive('sidebar', function() {
    return {
      restrict:"E",
      templateUrl:base_url+"assets/directives/sidebar.html"
    };
  });
  
    

mydirective.directive("navigator", function() {
    return {
      restrict:"E",
      templateUrl:base_url+"assets/directives/nav.html"
    };
  });
  
  mydirective.directive("users", function() {
    return {
      restrict:"E",
      templateUrl:base_url+"assets/directives/users.html"
    };
  });
  
  
  mydirective.directive("dashboard", function() {
    return {
      restrict:"E",
      templateUrl:base_url+"assets/directives/dashboard.html",
      transclude: true,
      scope:{activity:'@',priority:"@",dificult:"@",list:"="},
    };
  });
  
  mydirective.directive("activities", function() {
    return {
      restrict:"E",
      templateUrl:base_url+"assets/directives/activities.html"
    };
  });