'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var myModule = angular.module('myApp.services', []);

myModule.value('version', '0.1');

myModule.factory("get_all", ["$http", function($http) {
        return function() {
            var Items = [];
            $http.get("http://localhost/mastercodeigniter/index.php/log_activities/show_all/format/json")
                    .succes(function(data) {
                        items = data;
                    })
                    .error(function(data) {
                        console.log("error " + data);
                    });
            return items;
        }
    }]);